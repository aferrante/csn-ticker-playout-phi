unit ScheduleEntryTimeEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls;

type
  TScheduleEntryEditDlg = class(TForm)
    Panel1: TPanel;
    Label15: TLabel;
    Label23: TLabel;
    Label2: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label37: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    DateTimePicker1: TDateTimePicker;
    ComboBox9: TComboBox;
    ComboBox8: TComboBox;
    DateTimePicker2: TDateTimePicker;
    ComboBox10: TComboBox;
    ComboBox11: TComboBox;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ScheduleEntryEditDlg: TScheduleEntryEditDlg;

implementation

{$R *.DFM}

end.
