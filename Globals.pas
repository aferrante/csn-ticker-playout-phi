unit Globals;

interface

uses
  StBase, StColl, CheckTickerSchedule, Classes;

{$H+}
type
  TickerRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    GameID: String[20];
    SponsorLogo_Name: String[50];
    SponsorLogo_Dwell: SmallInt;
    StatStoredProcedure: String[50];
    StatType: SmallInt;
    StatTeam: String[50];
    StatLeague: String[20];
    StatRecordNumber: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  BreakingNewsRec = Record
    Event_Index: Smallint;
    Event_GUID: TGUID;
    Enabled: Boolean;
    League: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    UserData: Array[1..25] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  BreakingNewsSettingsRec = record
    BreakingNewsEnable: Boolean;
    BreakingNewsMode: SmallInt;
    Iterations: SmallInt;
  end;

  ColorRec = record
    Description: String;
    DBIndexValue: SmallInt;
  end;

  OverlayRec = Record
    OverlayName: String[20];
    OverlayType: SmallInt;
    OverlayText: String[10];
    OverlayFilename: String[50];
  end;

  TeamRec = record
    League: String[25];
    OldSTTeamCode: String[8];
    NewSTTeamCode: String[15];
    StatsIncID: Double;
    LongName: String[100];
    ShortName: String[100];
    BaseName: String[100];
    DisplayName1: String[50];
    DisplayName2: String[50];
  end;

  TeamDisplayInfoRec = record
    //Version 2.0.14 lengthened to 25 characters
    //DisplayMnemonic: String[10];
    DisplayMnemonic: String[25];
    Displayname: String[50];
    Division: String[50];
    Conference: String[50];
  end;

  LeagueRec = record
    LeagueNameShort: String[20];
    LeagueNameLong: String[50];
    LeagueGraphicFilename: String[50];
    LeagueType: SmallInt;
  end;

  StatRec = Record
    StatLeague: String[10];
    StatType: SmallInt;
    StatDescription: String[50];
    StatHeader: String[50];
    StatAbbreviation: String[10];
    StatStoredProcedure: String[50];
    StatDataField: String[20];
    UseStatQualifier: Boolean;
    StatQualifier: String[100];
    StatQualifierValue: SmallInt;
  end;

  SelectedStatRec = record
    StatType: SmallInt;
    StatStoredProcedure: String;
    StatLeague: String;
    StatTeam: String;
  end;

  SponsorLogoRec = Record
    SponsorLogoIndex: SmallInt;
    SponsorLogoName: String[50];
    SponsorLogoFilename: String[50];
  end;

  CurrentSponsorLogoRec = Record
    CurrentSponsorLogoName: String[50];
    CurrentSponsorLogoDwell: SmallInt;
    CurrentSponsorTemplate: SmallInt;
    LastDisplayedSponsorLogoName: String[50];
  end;

  PromoLogoRec = Record
    PromoLogoIndex: SmallInt;
    PromoLogoName: String[20];
    PromoLogoFilename: String[20];
  end;

  GamePhaseRec = record
    League: String[10];
    ST_Phase_Code: SmallInt;
    SI_Phase_Code: SmallInt;
    Label_Period: String[20];
    Display_Period: String[10];
    End_Is_Half: Boolean;
    Display_Half: String[20];
    Display_Final: String[20];
    Display_Extended1: String[10];
    Display_Extended2: String[10];
    Game_OT: Boolean;
  end;

  LineupText = Record
    UseLineup: Boolean;
    TextFields: Array[1..6] of String;
  end;

  EngineRec = Record
    Enabled: Boolean;
    IPAddress: String;
    Port: String;
  end;

  TemplateDefsRec = record
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
    Template_Has_Children: Boolean;
    Template_Is_Child: Boolean;
    Record_Type: SmallInt;
    SceneDirectorID: SmallInt;
    Default_Dwell: Word;
    ManualLeague: Boolean;
    UsesGameData: Boolean;
    RequiredGameState: SmallInt;
    TemplateSponsorType: SmallInt;
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    IsFantasyTemplate: Boolean; //OptionalFlag1
    IsLeadersTemplate: Boolean; //OptionalFlag2
  end;

  TemplateFieldsRec = record
    Template_ID: SmallInt;
    Field_ID: SmallInt;
    Field_Type: SmallInt;
    Field_Is_Manual: Boolean;
    Field_Label: String[50];
    Action_Controller_ID: SmallInt;
    Scene_Field_Name_A: String[200];
    Scene_Field_Name_B: String[200];
    Field_Contents: String[200];
    Field_Format_Prefix: String[50];
    Field_Format_Suffix: String[50];
    Field_Max_Length: SmallInt;
    LastValueCheckString: String[255];
    ValueHasChanged: Boolean;
  end;

  CategoryRec = record
    Category_Type: SmallInt;
    Category_ID: SmallInt;
    Category_Name: String[50];
    Category_Label: String[10];
    Category_Description: String[100];
    Category_Is_Sport: Boolean;
    Category_Sport: String[10];
  end;

  CategoryTemplatesRec = record
    Category_ID: SmallInt;
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
  end;

  SportRec = record
    Sport_Mnemonic: String[10];
    Sport_Description: String[50];
    Sport_Games_Table_Name: String[100];
    Sport_Stats_Query: String[250];
  end;

  GameRec = record
    League: String[10];
    GameID: String[15];
    DataFound: Boolean;
    SubLeague: String[50];
    DoubleHeader: Boolean;
    DoubleHeaderGameNumber: SmallInt;
    VStatsIncID: LongInt;
    HStatsIncID: LongInt;
    HName: String[50];
    VName: String[50];
    //Version 2.0.14 Lengthened to 25 characters
    //HMnemonic: String[10];
    //VMnemonic: String[10];
    HMnemonic: String[25];
    VMnemonic: String[25];
    HRecord: String[15];
    VRecord: String[15];
    HStreak: String[15];
    VStreak: String[15];
    HRank: String[4];
    VRank: String[4];
    HScore: SmallInt;
    VScore: SmallInt;
    VOpeningOdds: Double;
    HOpeningOdds: Double;
    OpeningTotal: Double;
    VCurrentOdds: Double;
    HCurrentOdds: Double;
    CurrentTotal: Double;
    Phase: SmallInt;
    TimeMin: SmallInt;
    TimeSec: SmallInt;
    StateForInProgressCheck: SmallInt;
    State: SmallInt;
    Count: String[5];
    Situation: String[50];
    Batter: String[25];
    Baserunners: Array[1..3] of String[25];
    Description: String[100];
    Date: TDateTime;
    StartTime: TDateTime;
    Visitor_Param1, Visitor_Param2: SmallInt;
    Home_Param1, Home_Param2: SmallInt;
  end;

  RecordTypeRec = record
    Playlist_Type: SmallInt;
    Record_Type: SmallInt;
    Record_Description: String[50];
  end;

  StyleChipRec = record
    StyleChip_Index: SmallInt;
    StyleChip_Code: String[4];
    StyleChip_Description: String[50];
    StyleChip_Type: SmallInt;
    StyleChip_String: String[20];
    StyleChip_FontCode: SmallInt;
    StyleChip_CharacterCode: SmallInt;
  end;

  LeagueCodeRec = record
    League_Mnemonic_Standard: String[10];
    Is_Subleague: Boolean;
  end;

  AutomatedLeagueRec = record
    SI_LeagueCode: String[10];
    ST_LeagueCode: String[10];
    Display_Mnemonic: String[10];
  end;

  PlaylistInfoRec = record
    Playlist_Description: String[100];
    Start_Enable_Time: TDateTime;
    End_Enable_Time: TDateTime;
    Playlist_ID: Double;
    Station_ID: SmallInt;
    Ticker_Mode: SmallInt;
  end;

  FootballSituationRec = record
    VisitorHasPossession: Boolean;
    HomeHasPossession: Boolean;
    YardsFromGoal: SmallInt;
    Down: SmallInt;
    Distance: SmallInt;
  end;

  //New record types for Stats Inc. Phase 2 implementation
  WeatherIconRec = record
    Icon_Description: String[50];
    Icon_Name: String[50];
  end;

  WeatherForecastRec = record
    CurrentLeague: String[10];
    CurrentHomeStatsID: LongInt;
    Weather_Stadium_Name: String[50];
    Weather_Current_Temperature: String[5];
    Weather_High_Temperature: String[5];
    Weather_Low_Temperature: String[5];
    Weather_Current_Conditions: String[50];
    Weather_Forecast_Icon_Day: String[50];
    Weather_Forecast_Icon_Night: String[50];
  end;

  //For team records
  TeamRecordRec = record
    Param: Array[1..4] of String;
  end;

  //For starting pitchers
  StartingPitcherStatsRec = record
    CurrentGameID: String[15];
    Home_Pitcher_Name_Last: String[20];
    Home_Pitcher_Name_First: String[20];
    Home_Pitcher_Wins: String[5];
    Home_Pitcher_Losses: String[5];
    Visitor_Pitcher_Name_Last: String[20];
    Visitor_Pitcher_Name_First: String[20];
    Visitor_Pitcher_Wins: String[5];
    Visitor_Pitcher_Losses: String[5];
  end;

  //For game final pitcher stats
  FinalPitcherStatsRec = record
    CurrentGameID: String[15];
    NeedToAddSavingPitcher: Boolean;
    Winning_Pitcher_Name_Last: String[20];
    Winning_Pitcher_Name_First: String[20];
    Winning_Pitcher_Wins: String[5];
    Winning_Pitcher_Losses: String[5];
    Winning_Pitcher_IP: String[5];
    Winning_Pitcher_K: String[5];
    Winning_Pitcher_BB: String[5];
    Winning_Pitcher_Hits: String[5];
    Winning_Pitcher_Runs: String[5];
    Winning_Pitcher_EarnedRuns: String[5];
    Winning_Pitcher_HomeRuns: String[5];
    Losing_Pitcher_Name_Last: String[20];
    Losing_Pitcher_Name_First: String[20];
    Losing_Pitcher_Wins: String[5];
    Losing_Pitcher_Losses: String[5];
    Losing_Pitcher_IP: String[5];
    Losing_Pitcher_K: String[5];
    Losing_Pitcher_BB: String[5];
    Losing_Pitcher_Hits: String[5];
    Losing_Pitcher_Runs: String[5];
    Losing_Pitcher_EarnedRuns: String[5];
    Losing_Pitcher_HomeRuns: String[5];
    Saving_Pitcher_Name_Last: String[20];
    Saving_Pitcher_Name_First: String[20];
    Saving_Pitcher_Saves: String[5];
  end;

  FinalBatterStatsRec = record
    Name_Last: String[20];
    Name_First: String[20];
    Atbats: String[5];
    Hits: String[5];
    Runs: String[5];
    Doubles: String[5];
    Triples: String[5];
    HomeRuns: String[5];
    RBI: String[5];
    StolenBases: String[5];
  end;

  //Basketball stats function records
  FinalBBallStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Points: String;
    Rebounds: String;
    Assists: String;
  end;

  //Hockey stats function records
  //Skater stats
  FinalSkaterStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Goals: String;
    Assists: String;
    SOG: String;
  end;

  //Goalie stats
  FinalGoalieStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Shots: String;
    Saves: String;
    GoalsAgainst: String;
  end;

  //Football stats function records
  FinalFBStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VPassingName_Last: String[20];
    VPassingName_First: String[20];
    VPassingAttempts: String;
    VPassingCompletions: String;
    VPassingYards: String;
    VPassingTouchdowns: String;
    VPassingInterceptions: String;
    HPassingName_Last: String[20];
    HPassingName_First: String[20];
    HPassingAttempts: String;
    HPassingCompletions: String;
    HPassingYards: String;
    HPassingTouchdowns: String;
    HPassingInterceptions: String;
    VRushingName_Last: String[20];
    VRushingName_First: String[20];
    VRushingAttempts: String;
    VRushingYards: String;
    VRushingTouchdowns: String;
    HRushingName_Last: String[20];
    HRushingName_First: String[20];
    HRushingAttempts: String;
    HRushingYards: String;
    HRushingTouchdowns: String;
    VReceivingName_Last: String[20];
    VReceivingName_First: String[20];
    VReceivingReceptions: String;
    VReceivingYards: String;
    VReceivingTouchdowns: String;
    HReceivingName_Last: String[20];
    HReceivingName_First: String[20];
    HReceivingReceptions: String;
    HReceivingYards: String;
    HReceivingTouchdowns: String;
  end;

  //General stats function records
  BoxscoreRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VNumPeriods: SmallInt;
    VScore: String[3];
    VPeriod1: String[3];
    VPeriod2: String[3];
    VPeriod3: String[3];
    VPeriod4: String[3];
    VPeriod5: String[3];
    VPeriod6: String[3];
    VPeriod7: String[3];
    HNumPeriods: SmallInt;
    HScore: String[3];
    HPeriod1: String[3];
    HPeriod2: String[3];
    HPeriod3: String[3];
    HPeriod4: String[3];
    HPeriod5: String[3];
    HPeriod6: String[3];
    HPeriod7: String[3];
  end;

  //////////////////////////////////////////////////////////////////////////////
  //Begin Fantasy record types
  //////////////////////////////////////////////////////////////////////////////
  //Generic
  FantasyGeneralStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    League: String[10];
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //NFL
  FantasyFootballPassingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Attempts: Array[1..10] of String;
    Completions: Array[1..10] of String;
    PassingYards: Array[1..10] of String;
    PassingTDs: Array[1..10] of String;
  end;

  FantasyFootballRushingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Rushes: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  FantasyFootballReceivingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Receptions: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  //MLB
  FantasyBaseballPitchingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Hits: Array[1..10] of String;
    Walks: Array[1..10] of String;
    Wins: Array[1..10] of String;
    Losses: Array[1..10] of String;
    Saves: Array[1..10] of String;
    Strikeouts: Array[1..10] of String;
    EarnedRuns: Array[1..10] of String;
  end;

  FantasyBaseballBattingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    AtBats: Array[1..10] of String;
    Hits: Array[1..10] of String;
    RBI: Array[1..10] of String;
    Doubles: Array[1..10] of String;
    Triples: Array[1..10] of String;
    HomeRuns: Array[1..10] of String;
    Walks: Array[1..10] of String;
    StolenBases: Array[1..10] of String;
  end;

  //NBA
  FantasyBasketballStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
    Rebounds: Array[1..10] of String;
    Assists: Array[1..10] of String;
    ThreePtFieldGoalsMade: Array[1..10] of String;
    Blocks: Array[1..10] of String;
    Steals: Array[1..10] of String;
    FieldGoalsMade: Array[1..10] of String;
    FieldGoalAttempts: Array[1..10] of String;
  end;

  //NHL
  FantasyHockeyPointsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
  end;

  FantasyHockeyGoalsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Goals: Array[1..10] of String;
  end;

  FantasyHockeyAssistsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Assists: Array[1..10] of String;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //End Fantasy record types
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //Start Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////
  //General leader type
  SeasonLeadersGeneralStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    League: String[10];
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //End Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////

  DefaultTemplatesRec = record
    Category_ID: Smallint;
    Category_Name: String[50];
    Template_Index: Integer;
    Template_ID: Integer;
    Template_Description: String[255];
    Enabled: Boolean;
    Selected: Boolean;
  end;

  //Aded flag to indicate data value is valid; used for symbolic names
  SymbolValueRec = record
    SymbolValue: String;
    OKToDisplay: Boolean;
  end;

  //Added for XPression support
  CommandRec = record
    Dwell: SmallInt;
    Command: ANSIString;
  end;

  CommandTypeRec = record
    CommandType: SmallInt;
    CommandDescription: String[100];
    CommandPrefix: String[10];
  end;

  //Record types used for commands
  ActionTypeRec = record
		ActionType: SmallInt;
    ActionDescription: String[100];
  end;

  TemplateCommandRec = record
    TemplateID: Double;
    ActionID: LongInt;
    CommandID: LongInt;
    ActionType: SmallInt;
    ActionDescription: String[100];
    CommandDescription: String[100];
    CommandType: SmallInt;
    SceneDirectorID: SmallInt;
    SceneDirectorName: String[100];
    ActionControllerID: SmallInt;
    AlwaysFireActionController: Boolean;
    ActionController_A: String[100];
    ActionController_B: String[100];
    ActionIsCrawl: Boolean;
    Delay: SmallInt;
    CommandEnabled: Boolean;
  end;

  SceneDirectorRec = record
    SceneDirectorID: SmallInt;
    SceneDirectorName: String[100];
    SceneDirectorState: SmallInt;
  end;

  ActionControllerRec = record
    SceneID: SmallInt;
    ActionControllerID: SmallInt;
    SceneDirectorName: String[100];
    ActionControllerName_A: String[100];
    ActionControllerName_B: String[100];
    ActionControllerState: SmallInt;
    ActionIsCrawl: Boolean;
  end;

  ScoreChipRec = record
    ChipDirectorName: String[20];
    CurrentChipStatus: Boolean;
    NewChipStatus: Boolean;
  end;

  //Added for score alerts
  ScoreAlertGameRec = record
    League: String[10];
    GameID: String[20];
    GameState: SmallInt;
    LastUpdateTime: TDateTime;
  end;
var
  PrefsFile: TextFile;
  Operator: String;
  Description: String;

  User_Collection: TStCollection; //Collection used for user logins
  Ticker_Collection: TStCollection; //Collection used for zipper entries
  BreakingNews_Collection: TStCollection; //Collection used for breaking news entries
  Team_Collection: TStCollection; //Collection used for NCAA teams
  League_Collection: TStCollection; //Collection used for leagues
  Ticker_Playout_Collection: TStCollection; //Collection used for zipper playout
  Overlay_Collection: TStCollection; //Collection used for overlays
  SponsorLogo_Collection: TStCollection; //Collection used for Sponsor logos
  PromoLogo_Collection: TStCollection; //Collection used for Promo logos
  Stat_Collection: TStCollection; //Collection used for stats types
  Temp_Stat_Collection: TStCollection; //Collection used for stats types
  Game_Phase_Collection: TStCollection; //Collection used for stats types
  Template_Defs_Collection: TStCollection; //Collection used for Template definitions
  Template_Fields_Collection: TStCollection; //Collection for template fields
  Temporary_Fields_Collection: TStCollection; //Collection for template fields
  Categories_Collection: TStCollection; //Collection for ticker categories
  Category_Templates_Collection: TStCollection; //Collection for templates that apply to ticker categories
  RecordType_Collection: TStCollection; //Collection for record types
  StyleChip_Collection: TStCollection; //Collection for style chips
  LeagueCode_Collection: TStCollection; //Collection for league codes
  Automated_League_Collection: TStCollection; //Collection used for leagues
  ScoreAlert_Games_Collection: TStCollection; //Collection used for monitoring games for score alerts

  DBConnectionString, DBConnectionString2: String;
  SearchText: String;
  SpellCheckerDictionaryDir: String;
  LoginComplete: Boolean;
  LastPageIndex: SmallInt;
  LoopTicker: Boolean;
  CurrentTickerEntryIndex: SmallInt;
  CurrentBreakingNewsEntryIndex: SmallInt;
  LastOverlay: String;
  NewOverlayPending: Boolean;
  ZipperColors: Array[0..8] of ColorRec;
  GameEntryMode: SmallInt; //1 = NCAA, 2 = NFL
  MaxCharsMatchupNotes, MaxCharsGameNotes: SmallInt;
  CurrentLeague: String;
  UseXMLGTServer: Boolean;
  SocketConnected: Boolean;
  EngineParameters: EngineRec;
  EngineControlMode: SmallInt;
  SponsorLogoDwell: SmallInt;
  SponsorLogoDwellCounter: SmallInt;
  ErrorLogFile: TextFile; {File for logging hardware interface errors}
  AsRunLogFile: TextFile; {File for logging sponsors & GPIs}
  AsRunLogFileDirectoryPath: String;
  Error_Condition: Boolean; {Flag to indicate an error occurred}
  ErrorLoggingEnabled: Boolean; {Flag to indicate error logging is enabled}
  CommandLoggingEnabled: Boolean; {Flag to indicate command logging is enabled}
  CommandLogFile: TextFile; {File for logging commands}
  CommandLogFileDirectoryPath: String;
  PacketEnable: Boolean;
  TickerDisplayMode: SmallInt;
//  CurrentSponsorLogoName: String;
//  CurrentSponsorLogoDwell: SmallInt;
//  LastDisplayedSponsorLogoName: String;
  TickerAbortFlag: Boolean;
  RunningTicker: Boolean;
  ElapsedTimeThisWheel: LongInt;
  Debug: Boolean;
  RunningThread: Boolean;
  LastDayOfWeek: SmallInt;
  Startup: Boolean;
  GPIsEnabled: Boolean;
  GPICOMPort: SmallInt;
  GPIBaudRate: SmallInt;
  GPIsOKToFire: Boolean;
  RefreshingGrid: Boolean;
  LogFilePath: String;
  LogFileName: String;
  ScheduleMonitoringEnabled: Boolean;
  CurrentTickerPlaylistID: Double;
  CurrentTickerPlaylistStartTime: TDateTime;
  CurrentBreakingNewsPlaylistID: Double;
  CurrentBreakingNewsPlaylistStartTime: TDateTime;
  CurrentBreakingNewsPlaylistEndTime: TDateTime;
  TickerInLiveEventMode: Boolean;
  AsRunLoggingEnabled: Boolean;
  //For logo/clock
  LogoClockEnabled: Boolean;
  LogoClockMode: SmallInt;
  CurrentTimeZone: SmallInt;
  LoadingPlaylist: Boolean;
  CheckTickerScheduleThread: TCheckTickerSchedule;
  UpdatingTickerPlaylist: Boolean;
  UpdatingBreakingNewsPlaylist: Boolean;
  ScheduledPlaylistInfo: Array[1..3] of PlaylistInfoRec;
  ScoreLogoOnlyEnable: Boolean;
  BreakingNewsSettings: BreakingNewsSettingsRec;
  MasterSegmentIndex: SmallInt;
  BreakingNewsIterationsCompleted: SmallInt;
  CurrentGameData: GameRec;
  ForceUpperCase: Boolean;
  ForceUpperCaseGameInfo: Boolean;
  USEDATAPACKET: Boolean;
  LastSegmentHeading: String;
  GameStartTimeOffset: Double;
  TimeZoneSuffix: String;
  ScheduledBreakingNewsPlaylistID: Double;
  EnableTemplateFieldFormatting: Boolean;
  GPILockout: SmallInt;

  //For  station ID
  StationID: SmallInt;
  StationDescription: String;

  //For sponsor logos
  CurrentSponsorInfo: CurrentSponsorLogoRec;
  EnablePersistentSponsorLogo: Boolean;

  //For expansion into multiple pages
  FantasyExpansionPageCount, LeadersExpansionPageCount: SmallInt;

  //Used for Phase 2 stats data to prevent multiple stored proc calls
  CurrentWeatherForecastRec: WeatherForecastRec;
  CurrentStartingPitcherStatsRec: StartingPitcherStatsRec;
  CurrentFinalPitcherStatsRec: FinalPitcherStatsRec;
  CurrentBoxscoreRec: BoxscoreRec;
  CurrentBBallStatsRec: FinalBBallStatsRec;
  CurrentFBStatsRec: FinalFBStatsRec;
  //For fantasy stats
  CurrentFantasyFootballPassingStatsRec: FantasyFootballPassingStatsRec;
  CurrentFantasyFootballRushingStatsRec: FantasyFootballRushingStatsRec;
  CurrentFantasyFootballReceivingStatsRec: FantasyFootballReceivingStatsRec;
  CurrentFantasyBaseballPitchingStatsRec: FantasyBaseballPitchingStatsRec;
  CurrentFantasyBaseballBattingStatsRec: FantasyBaseballBattingStatsRec;
  CurrentFantasyBasketballPointsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballReboundsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballAssistsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyHockeyPointsStatsRec: FantasyHockeyPointsStatsRec;
  CurrentFantasyHockeyGoalsStatsRec: FantasyHockeyGoalsStatsRec;
  CurrentFantasyHockeyAssistsStatsRec: FantasyHockeyAssistsStatsRec;

  //Version 3.1 - New Fantasy Record Types
  CurrentFantasyBaseballHitsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballHomeRunsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRBIsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRunsScoredStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStolenBasesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballWinsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballSavesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStrikeoutsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballInningsPitchedStatsRec: FantasyGeneralStatsRec;

  //Version 3.1 - Start season leaders record types
  Current_NFL_QB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_QB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_Field_Goal_Leaders_FGM: SeasonLeadersGeneralStatsRec;

  Current_MLB_AL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_ERA: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_ERA: SeasonLeadersGeneralStatsRec;

  Current_NHL_Leaders_GPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_GAA: SeasonLeadersGeneralStatsRec;

  Current_NBA_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_RPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_SPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_BPG: SeasonLeadersGeneralStatsRec;
  //End season leaders record types
  
  //To enable showing time and day in schedule for games not scheduled for the current day
  ShowTimeAndDay: Boolean;

  //Abbreviations
  ABBV_Rushes: String;
  ABBV_Receptions: String;
  ABBV_Yards: String;
  ABBV_TDs: String;
  ABBV_INTs: String;
  ABBV_Points: String;
  ABBV_Rebounds: String;
  ABBV_Assists: String;
  ABBV_Steals: String;
  ABBV_Blocks: String;
  ABBV_PPG: String;
  ABBV_RPG: String;
  ABBV_APG: String;
  ABBV_SPG: String;
  ABBV_BPG: String;
  ABBV_Goals: String;
  ABBV_Saves: String;

  //For disabling automated rankings
  UseManualRankings_NCAAB, UseManualRankings_NCAAF, UseManualRankings_NCAAW: Boolean;

  //Added for NCAAB Tournament
  UseSeedingForNCAABGames: Boolean;

  //Added for NCAAF BCS Rankings
  UseBCSRankings_NCAAF: Boolean;

  //Added for XPression support
  //For actions/commands
  QueuedCommandCollection: TStringList;

  ActionType_Collection: TStCollection;
  CommandType_Collection: TStCollection;
  TemplateCommand_Collection: TStCollection;
  TempTemplateCommand_Collection: TStCollection;
  ActionController_Collection: TStCollection;
  SceneDirector_Collection: TStCollection;
  LastTemplateID: SmallInt;
  LastSceneDirectorID: SmallInt;
  LastLineupData: LineUpText;
  SceneName: String;
  TickerBedInDelay: SmallInt;
  LineupInDelay: SmallInt;
  LineupOutDelay: SmallInt;
  LineupToFirstPageDelay: SmallInt;
  LastCommandSentWasLineup: Boolean;
  InitialDataTemplateFired: Boolean;
  ScoreChipStatus: Array[1..6] of ScoreChipRec;
  LastGameIDDisplayed: String;

  //For headers
  FantasyStartTemplate: SmallInt;
  FantasyEndTemplate: SmallInt;
  LeadersStartTemplate: SmallInt;
  LeadersEndTemplate: SmallInt;

  CrawlIsEnabled: Boolean;
  RunAnimationOnTickerOut: Boolean;

  LOG_QUEUE_COMMANDS: Boolean;
  LOG_OUTGOING_COMMANDS: Boolean;
  LOG_DELAYS: Boolean;
  LOG_ENGINE_RESPONSES: Boolean;
  LOG_TIMER_EVENTS: Boolean;

  EnableTeamRecords_MLB, EnableTeamRecords_NBA, EnableTeamRecords_NFL, EnableTeamRecords_NHL: Boolean;
  EnableTeamRecords_NCAAB, EnableTeamRecords_NCAAF, EnableTeamRecords_NCAAW, EnableTeamRecords_MLS: Boolean;

  Score_Alert_Time_Window: LongInt;
  LastScoreAlertCollectionPlaylistID: String;
  TickerInAnimationDelay: SmallInt;

const
  //Default delay for pages
  DEFAULTDELAY = 5000;

  //For playlist modes
  TICKER = 1;
  BREAKINGNEWS = 2;

  //Constant for cases where no entry index is specified
  NOENTRYINDEX = -1;

  //Breaking news modes
  IMMEDIATE = 0;
  SEGMENT = 1;

  //For football field position
  FIELDARROWRIGHT = #180;
  FIELDARROWLEFT = #181;
  FOOTBALLPOSSESSION = #200;
  LEADERINDICATOR = #200;

  //Game States for Stats Inc.
  PREGAME = 1;
  INPROGRESS = 2;
  FINAL = 3;
  POSTPONED = 4;
  DELAYED = 5;
  SUSPENDED = 6;
  CANCELLED = 7;
  UNDEFINED = -1;

  //Sponsor templates
  SPONSOR_TEMPLATE_ID = 1;
  SPONSOR_OUT = 2;

  //For XPression support
  //For command types
  COMMAND_TYPE_LOAD_PROJECT = 1;
  COMMAND_TYPE_OPEN_SCENE = 2;
  COMMAND_TYPE_UPDATE_TEMPLATE_FIELDS = 3;
  COMMAND_TYPE_FIRE_SCENE_DIRECTOR = 4;
  COMMAND_TYPE_FIRE_ACTION_CONTROLLER = 5;

  //For action types
  ACTION_TEMPLATE_IN_INITIAL = 1;
  ACTION_TEMPLATE_UPDATE = 2;
  ACTION_TEMPLATE_OUT = 3;
  ACTION_TEMPLATE_RESET = 4;

  SCENE_DIRECTOR_OUT = 0;
  SCENE_DIRECTOR_IN_A = 1;
  SCENE_DIRECTOR_IN_B = 2;

  ACTION_CONTROLLER_OUT = 0;
  ACTION_CONTROLLER_IN_A = 1;
  ACTION_CONTROLLER_IN_B = 2;

  ACTION_NOT_CRAWL = FALSE;
  ACTION_IS_CRAWL = TRUE;

  DONT_RESET_ALL = FALSE;
  RESET_ALL = TRUE;

  TEMPLATE_SPONOSR_TYPE_IN = 1;
  TEMPLATE_SPONOSR_TYPE_OUT = 2;

  TEMPLATE_TYPE_NORMAL = 1;
  TEMPLATE_TYPE_BREAKING_NEWS = 2;
  TEMPLATE_TYPE_PROGRAM_ALERT = 3;
  TEMPLATE_TYPE_GENERAL_NEWS = 4;

  SCENE_DIRECTOR_ID_FANTASY = 4;
  SCENE_DIRECTOR_ID_GAME_STATS = 7;
  SCENE_DIRECTOR_ID_NCAA_GAME_STATS = 13;
  SCENE_DIRECTOR_ID_MATCHUPS_IN = 11;
  SCENE_DIRECTOR_ID_R_MATCHUPS_IN = 12;
  SCENE_DIRECTOR_ID_BASEBALL_LIVE = 17;
  SCENE_DIRECTOR_ID_FOOTBALL_LIVE = 18;

  ANIMATION_CONTROLLER_ID_GAME_DATA = 11;
  ANIMATION_CONTROLLER_ID_GAME_STATS_DATA = 12;
  ANIMATION_CONTROLLER_ID_GAME_SLUG = 13;
  ANIMATION_CONTROLLER_ID_GAME_SLUG_CENTER = 14;
  ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA = 15;
  ANIMATION_CONTROLLER_ID_NCAA_GAME_STATS_DATA = 16;
  ANIMATION_CONTROLLER_ID_NCAA_GAME_SLUG = 17;
  ANIMATION_CONTROLLER_ID_GAME_TEAM2 = 21;
  ANIMATION_CONTROLLER_ID_GAME_SCORE2 = 22;

  ANIMATION_CONTROLLER_ID_MATCHUP_IN = 29;
  ANIMATION_CONTROLLER_ID_START_TIME_IN = 30;
  ANIMATION_CONTROLLER_ID_MATCHUP_NOTE_IN = 31;

  ANIMATION_CONTROLLER_ID_R_MATCHUP_IN = 32;
  ANIMATION_CONTROLLER_ID_R_MATCHUP_START_TIME_IN = 33;
  ANIMATION_CONTROLLER_ID_R_MATCHUP_NOTE_IN = 34;

  ANIMATION_CONTROLLER_ID_GAME_NOTE_CRAWL = 55;

  ONE_MINUTE = 1/(24*60);

implementation

end.
