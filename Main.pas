////////////////////////////////////////////////////////////////////////////////
// Playout application for Comcast Ticker
// V1.0  12/27/07  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Changed naming conventions for logfiles from SNY to Comcast
// V1.0.1  01/05/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed issues with looping modes when using manually selected playlist
// V1.0.3  02/04/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed issue with 1X loop mode when disabled entries were at the end of the
// list
// V1.0.5  03/06/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support INI-based offset for game start times to support
// regional sports nets
// V1.0.6  03/31/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support INI-based timezone suffix (e.g. ET, PT)
// V1.0.7  03/31/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support football field position indicator template
// V1.1.0  07/23/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support monitoring of playlists by Playout Station ID #.
// Used to support common Authoring for Bay Area & Sacramento
// V2.0.0.0  03/26/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support INI setting to switch template field prefix and suffix
// strings on and off.
// V2.0.0.2  04/17/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support Game Final Glyph and fix issue with Game Phase display
// when game is postponed, rain-delay, etc.
// V2.0.0.3  05/09/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to include new formatting functions for timezone, etc.
// V2.0.0.4  05/17/08  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support new behavior for blanking line-ups while displaying
// sponsor pages.
// V2.0.0.5  05/21/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support "compound" field definitions required for support of
// 2-line ticker mode in new engine graphics look.
// V2.1.0.0  06/01/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed bug where playlist index was not getting reset to 0 on a manual load.
// V2.1.1.0  10/09/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Implemented support for sponsor page at tail of playlist - required for
// CSN Chicago; changed socket component to prevent hang when disconnected
// from engine.
// V2.1.2.0  11/30/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Implemented support for AP Top 25 college rankings for NCAAB & NCAAF
// V2.1.4.0  12/08/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Implemented support for full-page sponsor logos and templates with rankings.
// V2.2.0.0  12/12/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Implemented support for new sponsor logo templates related to lower-right
// "persistent" logos
// V2.2.1.0  12/17/09  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support Stats Inc. data for basic game scores & schedules.
// Also removed most Sportsticker related functions.
// V2.3.0.0  03/19/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix bug with game start times always showing the day of the week.
// Also removed most Sportsticker related functions.
// V2.3.0.1  04/01/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added symbolic field description for game final highlighted in gold.
// V2.3.0.2  04/05/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for additional fields used for baseball line-scores (hits,
// errors).
// V2.3.0.3  04/09/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for new baseball in-game templates with baserunner situation.
// V2.4.0.0  05/25/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Misc. minor modifications.
// V2.4.1.0  05/27/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified for Phase 2 Stats Inc. (stats data) functionality.
// V3.0.0.0  10/10/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to correct issues with text positioning when showing Day of Week +
// Time for game start times.
// V3.0.1.0  11/12/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix bug with NFL post-game stats (RB stats used QB data).
// V3.0.2.0  11/29/10  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to NOT clear sponsor information if refreshing the current ticker
// playlist while the ticker is halted (CheckTickerSchedule thread).
// V3.0.3.0  01/13/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support SNY specific formatting and features; fixed Alerts
// and manual playlist loading functions.
// V3.1.0.0  03/22/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix bug with winning pitcher hero notes where losing pitcher
// named showed up instead; fixed issue with Rain Delays
// V3.1.1.0  04/04/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed behavior for rain delays in MLB and other special cases.
// V3.1.2.0  04/08/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed issues with MLB games where score did not show up until the bottom of
// the 1st inning; also issue with inning indicator for inter-league games.
// V3.1.3.0  06/21/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for BCS CFB Poll Rankings. Added support for selecting
// specific entry in playlist to start with when triggering ticker. Added
// template dwell time editor. Fixed issue where sponsor logo was not being
// cleared when loading a playlist manually. Fixed formatting for game start
// times when games are in the future.
// Modified to use separate query/data source for Playlist Selector dialog to
// prevent issue where currently selected data record in list would move due
// to other use of Ticker_Groups data source. Changed schedule check thread
// timer interval from 7.5S to 5.0S. Modified Schedule Check thread to prevent
// Canvas Draw error (was trying to set grid position from within thread).
// V3.2.0.0  01/25/12  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support Ross XPression and new SNY Ticker look for 2015.
// V4.0.0  04/02/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to prevent overlapping notes
// V4.0.1  04/06/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix issues with Program Alerts and Breaking News
// V4.0.2  04/07/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to fix issues with sponsors not persisting and line-ups not coming
// back in if the topics have not changed.
// V4.0.3  04/07/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support various automated stats templates (hero notes, fantasy,
// (league leaders, etc.).
// V4.0.4  04/08/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for special game conditions (e.g. Delay) for MLB
// V4.0.5  04/10/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for crawls
// V4.0.6  04/13/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Bug fixes for for crawls. Added support for disabling team records/points.
// V4.0.7  04/21/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for season leaders.
// V4.0.8  04/27/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for Score Alerts.
// V4.0.9  04/30/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support playlists with a single entry for Breaking News, Program
// Alert or simple news.
// V4.0.10  05/04/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added support for adjustable delay following trigger for ticker in
// animation when first bringing ticker in.
// V4.0.11  05/05/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to use adjust fonts to allow for longer college team mnemonics.
// V4.0.12  09/15/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to use mixed case for college team mnemonics (ranked names);
// extended length of team mnemonics from 10 to 25 characters.
// V4.0.14  09/16/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to use mixed case for base college team mnemonics.
// V4.0.15  09/30/15  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
//Ticker Record types

//Playlist Types
// 0 Ticker
// 1 Breaking News

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  TSImageList, Grids_ts, TSGrid, TSDBGrid, DBCtrls, StColl, MPlayer, OleCtrls,
  OleServer, Grids, PBJustOne, ClipBrd, TSMask, INIFiles, DBGrids, {OoMisc,} {AdStatLt,}
  Globals, AdPacket, AdPort, {sStatusBar,} OoMisc, AdStatLt;

{H+} //ANSI strings
//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Prefs1: TMenuItem;
    SetPrefs1: TMenuItem;
    N1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    tsImageList: TtsImageList;
    tsImageList1: TtsImageList;
    tsImageList2: TtsImageList;
    Login1: TMenuItem;
    LogintoSystem1: TMenuItem;
    N2: TMenuItem;
    LogoutfromSystem1: TMenuItem;
    PBJustOne1: TPBJustOne;
    Database1: TMenuItem;
    RepopulateTeamsCollection1: TMenuItem;
    N3: TMenuItem;
    PurgeScheduledEvents1DayOld1: TMenuItem;
    tsMaskDefs1: TtsMaskDefs;
    Panel9: TPanel;
    Label69: TLabel;
    NumEntries: TLabel;
    TickerPlaylistGrid: TtsGrid;
    Panel8: TPanel;
    Image2: TImage;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    LastCommandLabel: TLabel;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText38: TStaticText;
    Label1: TLabel;
    TickerStartTimeLabel: TLabel;
    TickerEndTimeLabel: TLabel;
    StaticText6: TStaticText;
    StaticText8: TStaticText;
    Panel3: TPanel;
    Label15: TLabel;
    Label5: TLabel;
    ApdStatusLight1: TApdStatusLight;
    ApdStatusLight2: TApdStatusLight;
    StaticText13: TStaticText;
    StaticText15: TStaticText;
    StaticText16: TStaticText;
    StaticText27: TStaticText;
    StaticText12: TStaticText;
    Panel2: TPanel;
    StaticText17: TStaticText;
    AbortPlaylistButton: TBitBtn;
    TriggerTickerButton: TBitBtn;
    Panel4: TPanel;
    TickerStatusLabel: TLabel;
    ResetTickerButton: TBitBtn;
    BitBtn4: TBitBtn;
    ScheduleMonitoringEnabledCheckBox: TCheckBox;
    Panel1: TPanel;
    NumBreakingNewsEntries: TLabel;
    Label12: TLabel;
    BreakingNewsPlaylistGrid: TtsGrid;
    Panel7: TPanel;
    Label22: TLabel;
    Panel11: TPanel;
    Label23: TLabel;
    Label4: TLabel;
    TimeOfDayTimer: TTimer;
    ErrorFlashTimer: TTimer;
    GPIResetTimer: TTimer;
    GPIInitTimer: TTimer;
    N4: TMenuItem;
    DisableErrorChecking1: TMenuItem;
    EnableErrorChecking1: TMenuItem;
    ResetError1: TMenuItem;
    N5: TMenuItem;
    Label6: TLabel;
    SetGraphicsEngineConnectionPreferences1: TMenuItem;
    N6: TMenuItem;
    ReconnecttoGraphicsEngine1: TMenuItem;
    PlaylistRefreshTimer: TTimer;
    TickerPlaylistName: TLabel;
    ApdComPort1: TApdComPort;
    ApdDataPacket1: TApdDataPacket;
    ApdStatusLight3: TApdStatusLight;
    StaticText5: TStaticText;
    StaticText7: TStaticText;
    ApdStatusLight4: TApdStatusLight;
    StaticText1: TStaticText;
    DwellValue: TLabel;
    ApdStatusLight5: TApdStatusLight;
    StaticText4: TStaticText;
    OneTimeLoopModeLabel: TPanel;
    Label7: TLabel;
    N7: TMenuItem;
    EnableCommandLogging1: TMenuItem;
    DisableCommandLogging1: TMenuItem;
    TickerModePanel: TPanel;
    TickerMode: TLabel;
    Panel6: TPanel;
    StaticText9: TStaticText;
    StationIDLabel: TLabel;
    StationIDNumLabel: TLabel;
    ReloadINIfile1: TMenuItem;
    Options1: TMenuItem;
    UseRankingsforNCAAB: TMenuItem;
    UseSeedforNCAAB: TMenuItem;
    CommandOutList: TMemo;
    N8: TMenuItem;
    S1: TMenuItem;
    H1: TMenuItem;
    N9: TMenuItem;
    C1: TMenuItem;
    ClearCommandListBtn: TBitBtn;
    StatusBar: TStatusBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Label8: TLabel;
    Label9: TLabel;
    //General program functions
    procedure FormActivate(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //Dialog functions
    procedure About1Click(Sender: TObject);
    procedure SetPrefs1Click(Sender: TObject);
    procedure StorePrefs1Click(Sender: TObject);
    procedure LoadPrefs;
    //Playlist operations
    procedure RefreshPlaylistGrid (PlaylistType: SMallInt);
    procedure DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
    //Utility functions
    procedure ReloadDataFromDatabase;
    function GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
    function ScrubText (InText: String) : String;
    procedure LoadPlaylistCollection(PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
    procedure RepopulateTeamsCollection1Click(Sender: TObject);
    function GetLeagueName(League_ID: SmallInt): String;
    function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function GetGameTimeString(GTimeStr: String): String;
    function GetStatInfo (StoredProcedureName: String): StatRec;
    function GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
    function GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
    function GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
    procedure TriggerTickerButtonClick(Sender: TObject);
    procedure TriggerCurrentTicker;
    procedure TimeOfDayTimerTimer(Sender: TObject);
    procedure ErrorFlashTimerTimer(Sender: TObject);
    procedure GPIResetTimerTimer(Sender: TObject);
    procedure GPIInitTimerTimer(Sender: TObject);
    procedure AbortCurrentEvent;
    procedure SearchForNextScheduledPlaylist(LoopingTicker: Boolean);
    procedure AbortPlaylistButtonClick(Sender: TObject);
    procedure ResetTickerButtonClick(Sender: TObject);
    procedure EnableErrorChecking1Click(Sender: TObject);
    procedure DisableErrorChecking1Click(Sender: TObject);
    procedure ResetError1Click(Sender: TObject);
    procedure DatabaseConnectError(ExceptionString: String);
    procedure DatabaseEngineError(ExceptionString: String);
    procedure FormCreate(Sender: TObject);
    procedure StorePrefs;
    procedure SetGraphicsEngineConnectionPreferences1Click(Sender: TObject);
    procedure ReconnecttoGraphicsEngine1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure PlaylistRefreshTimerTimer(Sender: TObject);
    procedure ScheduleMonitoringEnabledCheckBoxClick(Sender: TObject);
    function GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
    procedure ResetTickerToTop;
    function GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
    function GetAutomatedLeagueDisplayMnemonic (League: String): String;
    procedure ApdDataPacket1StringPacket(Sender: TObject; Data: String);
    procedure EnableCommandLogging1Click(Sender: TObject);
    procedure DisableCommandLogging1Click(Sender: TObject);
    procedure ReloadINIfile1Click(Sender: TObject);
    procedure UseRankingsforNCAABClick(Sender: TObject);
    procedure UseSeedforNCAABClick(Sender: TObject);
    procedure TickerPlaylistGridClick(Sender: TObject);
    procedure S1Click(Sender: TObject);
    procedure H1Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure ClearCommandListBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule,      //Data module
     SearchDataEntry, //Dialog used to enter text to search for
     GeneralFunctions,//General purpose functions
     PlaylistSelect,  //Dialog for selecting playlist to load
     PlaylistGraphicsViewer,  //Viewer for Zipper playlists
     ZipperEntryEditor,  //Editor dialog for playlist entries
     ScheduleEntryTimeEditor, //Editor for schedule entry times
     NoteEntryEditor, //Dialog for game note entry & editing
     NCAAManualGameEntry, //Dialog for manual NCAA entries
     ZipperEntry, //Dialog for basic data entry
     EngineIntf, //Ticker engine interface module
     EngineConnectionPreferences, //Ticker engine connnection prefs dialog
     CheckTickerSchedule, //Dialog for basic playlist element entry
     GameDataFunctions;

{$R *.DFM}
{$H+}
//Procedures to delete data pointers in each collection node
procedure Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure BreakingNews_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Temp_Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Temp_Sponsor_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Team_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TeamRec));
end;
procedure Ticker_Playout_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure SponsorLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SponsorLogoRec));
end;
procedure PromoLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PromoLogoRec));
end;
procedure Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Temp_Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Game_Phase_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GamePhaseRec));
end;
procedure Template_Defs_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateDefsRec));
end;
procedure Temporary_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Template_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Categories_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryRec));
end;
procedure Category_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryTemplatesRec));
end;
procedure Sports_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SportRec));
end;
procedure RecordType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(RecordTypeRec));
end;
procedure StyleChip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StyleChipRec));
end;
procedure LeagueCode_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LeagueCodeRec));
end;
procedure Automated_League_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(AutomatedLeagueRec));
end;
//Added for Expression support
procedure ActionType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ActionTypeRec));
end;
procedure CommandType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CommandTypeRec));
end;
procedure TemplateCommand_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateCommandRec));
end;
procedure TempTemplateCommand_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateCommandRec));
end;
procedure SceneDirector_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SceneDirectorRec));
end;
procedure ActionController_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ActionControllerRec));
end;
procedure ScoreAlert_Games_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ScoreAlertGameRec));
end;
////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for main form creation
procedure TMainForm.FormCreate(Sender: TObject);
begin
  {Init for continuous loop}
  LoopTicker := TRUE;

  {Init error condition flag}
  Error_Condition := FALSE;

  //oolean to indciate error logging is enabled; default = false; will be
  //enabled after one second timer expires
  ErrorLoggingEnabled := True;

  //Init StartUp flag for display of splash-screen
  StartUp := True;

  //Make label to indicate error checking disabled invisible
  StaticText27.Visible := False;
end;

//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
var
  i: SmallInt;
begin
  //Set debug flag
  Debug := FALSE;

  //Init flags & handles
  RunningThread := FALSE;
  LoadingPlaylist := FALSE;
  LogoClockEnabled := TRUE;
  CurrentBreakingNewsPlaylistID := -1;
  LastSegmentHeading := '';

  //Set defaults
  LastPageIndex := 0;
  DBConnectionString := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Data Source=TSTN';
  //Init for 2-line ticker, looping
  TickerDisplayMode := 3;

  //Set data dictionary directory
  SpellCheckerDictionaryDir := 'C:\Program Files\VDS\TSTN\Dictionaries';

  //Set day of week (used for roll-over of week number)
  LastDayOfWeek := DayOfWeek(Now);

  if (Startup = True) then
  begin
    //Clear flag
    Startup := False;

    LastPageIndex := 0;
    EngineControlMode := 0; //Run locally
    SocketConnected := FALSE;

    //Set default engine prefs
    with EngineParameters do
    begin
      Enabled := FALSE;
      IPAddress := '127.0.0.1';
      Port := '7795';
    end;

    //Disable GPIs to fire
    GPIsEnabled := FALSE;
    GPIsOKToFire := FALSE;

    //Reset flag
    RefreshingGrid := FALSE;

    LogFilePath := 'C:\Logfiles\';
    LogFileName := LogFilePath + 'NFL';

    //Load pereferences file
    LoadPrefs;

    //Set menu options for seeding
    if (UseSeedingForNCAABGames) then
    begin
      UseRankingsforNCAAB.Checked := FALSE;
      UseSeedforNCAAB.Checked := TRUE;
    end
    else begin
      UseRankingsforNCAAB.Checked := TRUE;
      UseSeedforNCAAB.Checked := FALSE;
    end;

    //Setup GPIs
    //Connect COM port if GPIs enabled
    if (GPIsEnabled) then
    begin
      ApdComPort1.AutoOpen := FALSE;
      ApdComPort1.Open := FALSE;
      ApdComPort1.ComNumber := GPICOMPort;
      ApdComPort1.Baud := GPIBaudRate;
      ApdComPort1.Open := TRUE;
      ApdComPort1.AutoOpen := TRUE;
    end
    else begin
      ApdComPort1.AutoOpen := FALSE;
      ApdComPort1.Open := FALSE;
    end;

    //Create collections
    Ticker_Collection := TStCollection.Create(1500);
    BreakingNews_Collection := TStCollection.Create(500);
    Team_Collection := TStCollection.Create(5000);
    Ticker_Playout_Collection := TStCollection.Create(1500);
    Overlay_Collection := TStCollection.Create(250);
    SponsorLogo_Collection := TStCollection.Create(100);
    PromoLogo_Collection := TStCollection.Create(100);
    Stat_Collection := TStCollection.Create(250);
    Temp_Stat_Collection := TStCollection.Create(250);
    Game_Phase_Collection := TStCollection.Create(500);
    Template_Defs_Collection := TStCollection.Create(500);
    Template_Fields_Collection := TStCollection.Create(5000);
    Temporary_Fields_Collection := TStCollection.Create(100);
    Categories_Collection := TStCollection.Create(50);
    Category_Templates_Collection := TStCollection.Create(500);
    RecordType_Collection := TStCollection.Create(250);
    StyleChip_Collection := TStCollection.Create(100);
    LeagueCode_Collection := TStCollection.Create(1000);
    Automated_League_Collection := TStCollection.Create(50);
    //Added for XPression support
    ActionType_Collection := TStCollection.Create(50);
    CommandType_Collection := TStCollection.Create(50);
    TemplateCommand_Collection := TStCollection.Create(2000);
    TempTemplateCommand_Collection := TStCollection.Create(100);
    ActionController_Collection := TStCollection.Create(100);
    SceneDirector_Collection := TStCollection.Create(25);
    ScoreAlert_Games_Collection := TStCollection.Create(1000);

    Ticker_Collection.DisposeData := Ticker_Collection_DisposeData;
    BreakingNews_Collection.DisposeData := BreakingNews_Collection_DisposeData;
    Team_Collection.DisposeData := Team_Collection_DisposeData;
    Ticker_Playout_Collection.DisposeData := Ticker_Playout_Collection_DisposeData;
    SponsorLogo_Collection.DisposeData := SponsorLogo_Collection_DisposeData;
    PromoLogo_Collection.DisposeData := PromoLogo_Collection_DisposeData;
    Stat_Collection.DisposeData := Stat_Collection_DisposeData;
    Temp_Stat_Collection.DisposeData := Temp_Stat_Collection_DisposeData;
    Game_Phase_Collection.DisposeData := Game_Phase_Collection_DisposeData;
    Template_Defs_Collection.DisposeData := Template_Defs_Collection_DisposeData;
    Template_Fields_Collection.DisposeData := Template_Fields_Collection_DisposeData;
    Temporary_Fields_Collection.DisposeData := Temporary_Fields_Collection_DisposeData;
    Categories_Collection.DisposeData := Categories_Collection_DisposeData;
    Category_Templates_Collection.DisposeData := Category_Templates_Collection_DisposeData;
    RecordType_Collection.DisposeData := RecordType_Collection_DisposeData;
    StyleChip_Collection.DisposeData := StyleChip_Collection_DisposeData;
    LeagueCode_Collection.DisposeData := LeagueCode_Collection_DisposeData;
    Automated_League_Collection.DisposeData := Automated_League_Collection_DisposeData;
    //Added for XPression support
    ActionType_Collection.DisposeData := ActionType_Collection_DisposeData;
    CommandType_Collection.DisposeData := CommandType_Collection_DisposeData;
    TemplateCommand_Collection.DisposeData := TemplateCommand_Collection_DisposeData;
    TempTemplateCommand_Collection.DisposeData := TempTemplateCommand_Collection_DisposeData;
    ActionController_Collection.DisposeData := ActionController_Collection_DisposeData;
    SceneDirector_Collection.DisposeData := SceneDirector_Collection_DisposeData;
    ScoreAlert_Games_Collection.DisposeData := ScoreAlert_Games_Collection_DisposeData;

    //Added for XPression support
    //Setup command queue
    QueuedCommandCollection := TStringList.Create;

    //Activate database & tables
    With dmMain do
    begin
      dbSNYTicker.Connected := FALSE;
      dbSNYTicker.ConnectionString := DBConnectionString;
      dbSNYTicker.Connected := TRUE;
      tblTicker_Groups.Active := TRUE;
      tblTicker_Elements.Active := TRUE;
      tblScheduled_Ticker_Groups.Active := TRUE;
      tblBreakingNews_Groups.Active := TRUE;
      tblBreakingNews_Elements.Active := TRUE;
      //tblScheduled_BreakingNews_Groups.Active := TRUE;
      tblSponsor_Logos.Active := TRUE;
      dbSportbase.Connected := FALSE;
      dbSportbase.ConnectionString := DBConnectionString2;
      dbSportbase.Connected := TRUE;
    end;

    //Call procedure to load data from database
    ReloadDataFromDatabase;

    //Connect to the graphics engine if it's enabled
    try
      EngineParameters.Enabled := TRUE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
      end;
    end;
    //Init the playlists IDs so new ones are found
    CurrentTickerPlaylistID := -1;
    CurrentBreakingNewsPlaylistID := -1;
  end;

  //Spawn thread to check ticker playlist schedule
//  CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
//  CheckTickerScheduleThread.Priority := tpLowest;
//  CheckTickerScheduleThread.FreeOnTerminate := TRUE;
//  //Execute the thread
//  try
//    CheckTickerScheduleThread.Resume;
//  except
//    CheckTickerScheduleThread.Free;
//    if (ErrorLoggingEnabled = True) then
//      EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
//  end;

    PlaylistRefreshTimer.Enabled := TRUE;

    //Set state of schedule monitoring
    if (ScheduleMonitoringEnabled = TRUE) then
    begin
      ScheduleMonitoringEnabledCheckBox.Checked := TRUE;
      BitBtn4.Visible := FALSE;
      Label6.Visible := FALSE;
      //Enable timer to check for scheduled playlist
      //PlaylistRefreshTimer.Enabled := TRUE;
    end
    //In manual mode
    else begin
      //Clear flag; disble controls; show label
      ScheduleMonitoringEnabledCheckBox.Checked := FALSE;
      BitBtn4.Visible := TRUE;
      Label6.Visible := TRUE;
      //Disable timer to check for scheduled playlist
      //PlaylistRefreshTimer.Enabled := FALSE;
    end;

    //Delay
    for i := 1 to 10 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Init score chips
    ScoreChipStatus[1].ChipDirectorName := 'WIN_CHIP_SINGLE_L';
    ScoreChipStatus[2].ChipDirectorName := 'WIN_CHIP_DOUBLE_L';
    ScoreChipStatus[3].ChipDirectorName := 'WIN_CHIP_TRIPLE_L';
    ScoreChipStatus[4].ChipDirectorName := 'WIN_CHIP_SINGLE_R';
    ScoreChipStatus[5].ChipDirectorName := 'WIN_CHIP_DOUBLE_R';
    ScoreChipStatus[6].ChipDirectorName := 'WIN_CHIP_TRIPLE_R';
    for i := 1 to 6 do
    begin
      ScoreChipStatus[i].CurrentChipStatus := FALSE;
      ScoreChipStatus[i].NewChipStatus := FALSE;
    end;

  //Call function to run startup commands
  EngineInterface.RunStartupCommands;
end;

//Handler for program exit from main menu
procedure TMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: SmallInt;
begin
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the CSN Ticker Playout application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Clear & disable the queue
    EngineInterface.CommandDwellTimer.Enabled := FALSE;
    QueuedCommandCollection.Clear;

    //Clear the zipper if it's running
    AbortCurrentEvent;
    EngineInterface.CommandDwellTimer.Enabled := TRUE;

    //Delay
    for i := 1 to 10 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Set action for main form
    Action := caFree;
    //Disable timers
    TimeOfDayTimer.Enabled := FALSE;
    GPIResetTimer.Enabled := FALSE;
    PlaylistRefreshTimer.Enabled := FALSE;
    //Make sure packet timeout timer is disabled
    EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;

    //Terminate the thread
    CheckTickerScheduleThread.Terminate;
  end
  else
     Action := caNone;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs1Click(Sender: TObject);
begin
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING VARIOUS PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.About1Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.SetPrefs1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.Edit1.Text := AsRunLogFileDirectoryPath;
    Modal.Edit2.Text := DBConnectionString;
    Modal.Edit3.Text := DBConnectionString2;
    if (GPIsEnabled) then Modal.RadioGroup1.ItemIndex := 1
    else Modal.RadioGroup1.ItemIndex := 0;
    Modal.RadioGroup2.ItemIndex := GPIComPort-1;
    Case GPIBaudRate of
      9600: Modal.RadioGroup3.ItemIndex := 0;
     19200: Modal.RadioGroup3.ItemIndex := 1;
    end;
    if (ScheduleMonitoringEnabled) then Modal.RadioGroup4.ItemIndex := 1
    else Modal.RadioGroup4.ItemIndex := 0;
    Modal.StationIDNum.Value := StationID;
    Modal.StationIDDesc.Text := StationDescription;

    //Set check boxes for enable/disable team records
    if (EnableTeamRecords_MLB) then
      Modal.EnableTeamRecords_Checkbox_MLB.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_MLB.Checked := FALSE;
    if (EnableTeamRecords_NBA) then
      Modal.EnableTeamRecords_Checkbox_NBA.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NBA.Checked := FALSE;
    if (EnableTeamRecords_NFL) then
      Modal.EnableTeamRecords_Checkbox_NFL.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NFL.Checked := FALSE;
    if (EnableTeamRecords_NHL) then
      Modal.EnableTeamRecords_Checkbox_NHL.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NHL.Checked := FALSE;
    if (EnableTeamRecords_NCAAB) then
      Modal.EnableTeamRecords_Checkbox_NCAAB.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NCAAB.Checked := FALSE;
    if (EnableTeamRecords_NCAAF) then
      Modal.EnableTeamRecords_Checkbox_NCAAF.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NCAAF.Checked := FALSE;
    if (EnableTeamRecords_NCAAW) then
      Modal.EnableTeamRecords_Checkbox_NCAAW.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_NCAAW.Checked := FALSE;
    if (EnableTeamRecords_MLS) then
      Modal.EnableTeamRecords_Checkbox_MLS.Checked := TRUE
    else
      Modal.EnableTeamRecords_Checkbox_MLS.Checked := FALSE;

    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      AsRunLogFileDirectoryPath := Modal.Edit1.Text;
      DBConnectionString := Modal.Edit2.Text;
      DBConnectionString2 := Modal.Edit3.Text;
      if (Modal.RadioGroup1.ItemIndex = 1) then GPIsEnabled := TRUE
      else GPIsEnabled := FALSE;
      GPIComPort := Modal.RadioGroup2.ItemIndex+1;
      if (Modal.RadioGroup3.ItemIndex = 0) then GPIBaudRate := 9600
      else GPIBaudRate := 19200;
      if (Modal.RadioGroup4.ItemIndex = 1) then ScheduleMonitoringEnabled := TRUE
      else ScheduleMonitoringEnabled := FALSE;

      //Set check boxes for enable/disable team records
      EnableTeamRecords_MLB := Modal.EnableTeamRecords_Checkbox_MLB.Checked;
      EnableTeamRecords_NBA := Modal.EnableTeamRecords_Checkbox_NBA.Checked;
      EnableTeamRecords_NFL := Modal.EnableTeamRecords_Checkbox_NFL.Checked;
      EnableTeamRecords_NHL := Modal.EnableTeamRecords_Checkbox_NHL.Checked;
      EnableTeamRecords_NCAAB := Modal.EnableTeamRecords_Checkbox_NCAAB.Checked;
      EnableTeamRecords_NCAAF := Modal.EnableTeamRecords_Checkbox_NCAAF.Checked;
      EnableTeamRecords_NCAAW := Modal.EnableTeamRecords_Checkbox_NCAAW.Checked;
      EnableTeamRecords_MLS := Modal.EnableTeamRecords_Checkbox_MLS.Checked;

      //Setup GPIs
      //Connect COM port if GPIs enabled
      if (GPIsEnabled) then
      begin
        ApdComPort1.AutoOpen := FALSE;
        ApdComPort1.Open := FALSE;
        ApdComPort1.ComNumber := GPICOMPort;
        ApdComPort1.Baud := GPIBaudRate;
        ApdComPort1.Open := TRUE;
        ApdComPort1.AutoOpen := TRUE;
      end
      else begin
        ApdComPort1.AutoOpen := FALSE;
        ApdComPort1.Open := FALSE;
      end;

      //Set controls for schedule monitoring
      if (Modal.RadioGroup4.ItemIndex = 1) then
      begin
        ScheduleMonitoringEnabledCheckBox.Checked := TRUE;
        ScheduleMonitoringEnabled := TRUE;
        BitBtn4.Visible := FALSE;
        Label6.Visible := FALSE;
        //Enable timer to check for scheduled playlist
        //PlaylistRefreshTimer.Enabled := TRUE;
      end
      else begin
        ScheduleMonitoringEnabledCheckBox.Checked := FALSE;
        ScheduleMonitoringEnabled := FALSE;
        BitBtn4.Visible := TRUE;
        Label6.Visible := TRUE;
        //Disable timer to check for scheduled playlist
        //PlaylistRefreshTimer.Enabled := FALSE;
      end;

      //Set station ID info
      StationID := Modal.StationIDNum.Value;
      StationDescription := Modal.StationIDDesc.Text;
      //Set label
      StationIDNumLabel.Caption := IntToStr(StationID);
      StationIDLabel.Caption := StationDescription;

      //Activate database & tables
      With dmMain do
      begin
        dbSNYTicker.Connected := FALSE;
        dbSNYTicker.ConnectionString := DBConnectionString;
        dbSNYTicker.Connected := TRUE;
        tblTicker_Elements.Active := TRUE;
        tblTicker_Groups.Active := TRUE;
        tblScheduled_Ticker_Groups.Active := TRUE;
        tblBreakingNews_Elements.Active := TRUE;
        tblBreakingNews_Groups.Active := TRUE;
        //tblScheduled_BreakingNews_Groups.Active := TRUE;
        tblSponsor_Logos.Active := TRUE;
        dbSportbase.Connected := FALSE;
        dbSportbase.ConnectionString := DBConnectionString2;
        dbSportbase.Connected := TRUE;
      end;
      //Store the preferences
      StorePrefs;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
// Various functions used for collection lookup, data processing, etc.
////////////////////////////////////////////////////////////////////////////////
//Handler for menu entry to re-load INI file
procedure TMainForm.ReloadINIfile1Click(Sender: TObject);
begin
  LoadPrefs;
end;

//Handler to reload collections
procedure TMainForm.RepopulateTeamsCollection1Click(Sender: TObject);
begin
  ReloadDataFromDatabase;
end;
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  TeamPtr: ^TeamRec; //Pointer to team collection object
  SponsorLogoPtr: ^SponsorLogoRec; //Pointer to sponsor logo object
  StatPtr: ^StatRec; //Pointer to stat object
  GamePhasePtr: ^GamePhaseRec;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CategoryRecPtr: ^CategoryRec;
  CategoryTemplatesRecPtr: ^CategoryTemplatesRec;
  SportRecPtr: ^SportRec;
  RecordTypeRecPtr: ^RecordTypeRec;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec;
  ActionTypeRecPtr: ^ActionTypeRec;
  CommandTypeRecPtr: ^CommandTypeRec;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  ActionControllerRecPtr: ^ActionControllerRec;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  try
    //Load collections for users and block subcategories
    Team_Collection.Clear;
    Team_Collection.Pack;
    //Load in teams information
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Teams');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to teams collection
        GetMem (TeamPtr, SizeOf(TeamRec));
        With TeamPtr^ do
        begin
          League := dmMain.Query1.FieldByName('League').AsString;
          //OldSTTeamCode := dmMain.Query1.FieldByName('OldSTTeamCode').AsString;
          NewSTTeamCode := dmMain.Query1.FieldByName('NewSTTeamCode').AsString;
          StatsIncID := dmMain.Query1.FieldByName('StatsIncID').AsFloat;
          LongName := dmMain.Query1.FieldByName('LongName').AsString;
          ShortName := dmMain.Query1.FieldByName('ShortName').AsString;
          BaseName := dmMain.Query1.FieldByName('BaseName').AsString;
          DisplayName1 := dmMain.Query1.FieldByName('DisplayName1').AsString;
          DisplayName2 := dmMain.Query1.FieldByName('DisplayName2').AsString;
          If (Team_Collection.Count <= 5000) then
          begin
            Team_Collection.Insert(TeamPtr);
            Team_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end
    else begin
      MessageDlg('No data found in Teams database table!', mtError, [mbOk], 0);
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the game phase codes table; iterate for all records in table
    Game_Phase_Collection.Clear;
    Game_Phase_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Game_Phase_Codes');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (GamePhasePtr, SizeOf(GamePhaseRec));
        With GamePhasePtr^ do
        begin
          League := dmMain.Query1.FieldByName('League').AsString;
          ST_Phase_Code := dmMain.Query1.FieldByName('ST_Phase_Code').AsInteger;
          SI_Phase_Code := dmMain.Query1.FieldByName('SI_Phase_Code').AsInteger;
          Display_Period := dmMain.Query1.FieldByName('Display_Period').AsString;
          End_Is_Half := dmMain.Query1.FieldByName('End_Is_Half').AsBoolean;
          Display_Half := dmMain.Query1.FieldByName('Display_Half').AsString;
          Display_Final := dmMain.Query1.FieldByName('Display_Final').AsString;
          Display_Extended1 := dmMain.Query1.FieldByName('Display_Extended1').AsString;
          Display_Extended2 := dmMain.Query1.FieldByName('Display_Extended2').AsString;
          If (Game_Phase_Collection.Count <= 500) then
          begin
            //Add to collection
            Game_Phase_Collection.Insert(GamePhasePtr);
            Game_Phase_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end
    else begin
      MessageDlg('No data found in Game Phase database table!', mtError, [mbOk], 0);
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the stats info table; iterate for all records in table
    Stat_Collection.Clear;
    Stat_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Stat_Definitions');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (StatPtr, SizeOf(StatRec));
        With StatPtr^ do
        begin
          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
          StatDescription := dmMain.Query1.FieldByName('StatDescription').AsString;
          StatHeader := dmMain.Query1.FieldByName('StatHeader').AsString;
          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
          StatDataField := dmMain.Query1.FieldByName('StatDataField').AsString;
          StatAbbreviation := dmMain.Query1.FieldByName('StatAbbreviation').AsString;
          UseStatQualifier := dmMain.Query1.FieldByName('UseStatQualifier').AsBoolean;
          StatQualifier := dmMain.Query1.FieldByName('StatQualifier').AsString;
          StatQualifierValue := dmMain.Query1.FieldByName('StatQualifierValue').AsInteger;
          If (Stat_Collection.Count <= 100) then
          begin
            //Add to collection
            Stat_Collection.Insert(StatPtr);
            Stat_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the sponsor logos table; iterate for all records in table
    SponsorLogo_Collection.Clear;
    SponsorLogo_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Sponsor_Logos');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (SponsorLogoPtr, SizeOf(SponsorLogoRec));
        With SponsorLogoPtr^ do
        begin
          SponsorLogoIndex := dmMain.Query1.FieldByName('LogoIndex').AsInteger;
          SponsorLogoName := dmMain.Query1.FieldByName('LogoName').AsString;
          SponsorLogoFilename := dmMain.Query1.FieldByName('LogoFilename').AsString;
          If (SponsorLogo_Collection.Count <= 100) then
          begin
            //Add to collection
            SponsorLogo_Collection.Insert(SponsorLogoPtr);
            SponsorLogo_Collection.Pack;
            //Add to comboboxes
            //ComboBox4.Items.Add(SponsorLogoName);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template definitions table; iterate for all records in table
    Template_Defs_Collection.Clear;
    Template_Defs_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Defs');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateDefRecPtr, SizeOf(TemplateDefsRec));
        With TemplateDefRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
          Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
          Template_Has_Children := dmMain.Query1.FieldByName('Template_Has_Children').AsBoolean;
          Template_Is_Child := dmMain.Query1.FieldByName('Template_Is_Child').AsBoolean;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          SceneDirectorID := dmMain.Query1.FieldByName('SceneDirectorID').AsInteger;
          TemplateSponsorType := dmMain.Query1.FieldByName('TemplateSponsorType').AsInteger;
          Default_Dwell := dmMain.Query1.FieldByName('Default_Dwell').AsInteger;
          ManualLeague := dmMain.Query1.FieldByName('ManualLeague').AsBoolean;
          UsesGameData := dmMain.Query1.FieldByName('UsesGameData').AsBoolean;
          RequiredGameState := dmMain.Query1.FieldByName('RequiredGameState').AsInteger;
          TemplateSponsorType := dmMain.Query1.FieldByName('TemplateSponsorType').AsInteger;
          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
          IsFantasyTemplate := dmMain.Query1.FieldByName('OptionalFlag1').AsBoolean;
          IsLeadersTemplate := dmMain.Query1.FieldByName('OptionalFlag2').AsBoolean;
          If (Template_Defs_Collection.Count <= 500) then
          begin
            //Add to collection
            Template_Defs_Collection.Insert(TemplateDefRecPtr);
            Template_Defs_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template fieldss table; iterate for all records in table
    Template_Fields_Collection.Clear;
    Template_Fields_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Fields');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
        With TemplateFieldsRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
          Field_Type := dmMain.Query1.FieldByName('Field_Type').AsInteger;
          Field_Is_Manual := dmMain.Query1.FieldByName('Field_Is_Manual').AsBoolean;
          Field_Label := dmMain.Query1.FieldByName('Field_Label').AsString;
          Field_Contents := dmMain.Query1.FieldByName('Field_Contents').AsString;
          Action_Controller_ID := dmMain.Query1.FieldByName('Action_Controller_ID').AsInteger;
          Scene_Field_Name_A := dmMain.Query1.FieldByName('Scene_Field_Name_A').AsString;
          Scene_Field_Name_B := dmMain.Query1.FieldByName('Scene_Field_Name_B').AsString;
          Field_Format_Prefix := dmMain.Query1.FieldByName('Field_Format_Prefix').AsString;
          Field_Format_Suffix := dmMain.Query1.FieldByName('Field_Format_Suffix').AsString;
          Field_Max_Length := dmMain.Query1.FieldByName('Field_Max_Length').AsInteger;
          LastValueCheckString := '';
          ValueHasChanged := FALSE;
          If (Template_Fields_Collection.Count <= 5000) then
          begin
            //Add to collection
            Template_Fields_Collection.Insert(TemplateFieldsRecPtr);
            Template_Fields_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the record types table; iterate for all records in table
    RecordType_Collection.Clear;
    RecordType_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Record_Types');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (RecordTypeRecPtr, SizeOf(RecordTypeRec));
        With RecordTypeRecPtr^ do
        begin
          Playlist_Type := dmMain.Query1.FieldByName('Playlist_Type').AsInteger;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          Record_Description := dmMain.Query1.FieldByName('Record_Description').AsString;
          If (RecordType_Collection.Count <= 250) then
          begin
            //Add to collection
            RecordType_Collection.Insert(RecordTypeRecPtr);
            RecordType_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the style chips table; iterate for all records in table
    StyleChip_Collection.Clear;
    StyleChip_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Style_Chips');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to style chip collection
        GetMem (StyleChipRecPtr, SizeOf(StyleChipRec));
        With StyleChipRecPtr^ do
        begin
          StyleChip_Index := dmMain.Query1.FieldByName('StyleChip_Index').AsInteger;
          StyleChip_Description := dmMain.Query1.FieldByName('StyleChip_Description').AsString;
          StyleChip_Code := dmMain.Query1.FieldByName('StyleChip_Code').AsString;
          StyleChip_Type := dmMain.Query1.FieldByName('StyleChip_Type').AsInteger;
          StyleChip_String := dmMain.Query1.FieldByName('StyleChip_String').AsString;
          StyleChip_FontCode := dmMain.Query1.FieldByName('StyleChip_FontCode').AsInteger;
          StyleChip_CharacterCode := dmMain.Query1.FieldByName('StyleChip_CharacterCode').AsInteger;
          If (StyleChip_Collection.Count <= 100) then
          begin
            //Add to collection
            StyleChip_Collection.Insert(StyleChipRecPtr);
            StyleChip_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the automated leagues table; iterate for all records in table
    Automated_League_Collection.Clear;
    Automated_League_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Automated_Leagues');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (AutomatedLeagueRecPtr, SizeOf(AutomatedLeagueRec));
        With AutomatedLeagueRecPtr^ do
        begin
          SI_LeagueCode := dmMain.Query1.FieldByName('SI_LeagueCode').AsString;
          ST_LeagueCode := dmMain.Query1.FieldByName('ST_LeagueCode').AsString;
          Display_Mnemonic := dmMain.Query1.FieldByName('Display_Mnemonic').AsString;
          If (Automated_League_Collection.Count <= 50) then
          begin
            //Add to collection
            Automated_League_Collection.Insert(AutomatedLeagueRecPtr);
            Automated_League_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Added for XPression support
    ActionType_Collection.Clear;
    ActionType_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Action_Types');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (ActionTypeRecPtr, SizeOf(ActionTypeRec));
        With ActionTypeRecPtr^ do
        begin
      		ActionType := dmMain.Query1.FieldByName('ActionType').AsInteger;
          ActionDescription := dmMain.Query1.FieldByName('ActionDescription').AsString;
          If (ActionType_Collection.Count <= 100) then
          begin
            //Add to collection
            ActionType_Collection.Insert(ActionTypeRecPtr);
            ActionType_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    CommandType_Collection.Clear;
    CommandType_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Command_Types');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (CommandTypeRecPtr, SizeOf(CommandTypeRec));
        With CommandTypeRecPtr^ do
        begin
      		CommandType := dmMain.Query1.FieldByName('CommandType').AsInteger;
          CommandDescription := dmMain.Query1.FieldByName('CommandDescription').AsString;
          CommandPreFix := dmMain.Query1.FieldByName('CommandPrefix').AsString;
          If (CommandType_Collection.Count <= 100) then
          begin
            //Add to collection
            CommandType_Collection.Insert(CommandTypeRecPtr);
            CommandType_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    TemplateCommand_Collection.Clear;
    TemplateCommand_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Commands');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (TemplateCommandRecPtr, SizeOf(TemplateCommandRec));
        With TemplateCommandRecPtr^ do
        begin
      		TemplateID := dmMain.Query1.FieldByName('TemplateID').AsFloat;
      		ActionID := dmMain.Query1.FieldByName('ActionID').AsInteger;
      		CommandID := dmMain.Query1.FieldByName('CommandID').AsInteger;
      		ActionType := dmMain.Query1.FieldByName('ActionType').AsInteger;
          ActionDescription := dmMain.Query1.FieldByName('ActionDescription').AsString;
          CommandDescription := dmMain.Query1.FieldByName('CommandDescription').AsString;
      		CommandType := dmMain.Query1.FieldByName('CommandType').AsInteger;
          SceneDirectorID := dmMain.Query1.FieldByName('SceneDirectorID').AsInteger;
          SceneDirectorName := dmMain.Query1.FieldByName('SceneDirectorName').AsString;
          ActionControllerID := dmMain.Query1.FieldByName('ActionControllerID').AsInteger;
          AlwaysFireActionController := dmMain.Query1.FieldByName('AlwaysFireActionController').AsBoolean;
          ActionController_A := dmMain.Query1.FieldByName('ActionController_A').AsString;
          ActionController_B := dmMain.Query1.FieldByName('ActionController_B').AsString;
          ActionIsCrawl := dmMain.Query1.FieldByName('ActionIsCrawl').AsBoolean;
      		Delay := dmMain.Query1.FieldByName('Delay').AsInteger;
          CommandEnabled := TRUE;
          If (TemplateCommand_Collection.Count <= 2000) then
          begin
            //Add to collection
            TemplateCommand_Collection.Insert(TemplateCommandRecPtr);
            TemplateCommand_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    SceneDirector_Collection.Clear;
    SceneDirector_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM SceneDirectors');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        GetMem (SceneDirectorRecPtr, SizeOf(SceneDirectorRec));
        With SceneDirectorRecPtr^ do
        begin
      		SceneDirectorID := dmMain.Query1.FieldByName('SceneDirectorID').AsInteger;
          SceneDirectorName := dmMain.Query1.FieldByName('SceneDirectorName').AsString;
          SceneDirectorState := 0;
          if (SceneDirector_Collection.Count <= 25) then
          begin
            //Add to collection
            SceneDirector_Collection.Insert(SceneDirectorRecPtr);
            SceneDirector_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    ActionController_Collection.Clear;
    ActionController_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM SceneActionControllers');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        GetMem (ActionControllerRecPtr, SizeOf(ActionControllerRec));
        With ActionControllerRecPtr^ do
        begin
          SceneID := dmMain.Query1.FieldByName('SceneID').AsInteger;
      		ActionControllerID := dmMain.Query1.FieldByName('ActionControllerID').AsInteger;
          SceneDirectorName := dmMain.Query1.FieldByName('SceneDirectorName').AsString;
          ActionControllerName_A := dmMain.Query1.FieldByName('ActionControllerName_A').AsString;
          ActionControllerName_B := dmMain.Query1.FieldByName('ActionControllerName_B').AsString;
          ActionIsCrawl := dmMain.Query1.FieldByName('ActionIsCrawl').AsBoolean;
          ActionControllerState := 0;
          if (ActionController_Collection.Count <= 100) then
          begin
            //Add to collection
            ActionController_Collection.Insert(ActionControllerRecPtr);
            ActionController_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query                      
    dmMain.Query1.Active := FALSE;

  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to reload data from database');
  end;
end;

//Function to scrub a string of text to remove control characters
function TMainForm.ScrubText (InText: ANSIString) : ANSIString;
var
  i: SmallInt;
  OutStr: ANSIString;
begin
  OutStr := '';
  //Remove all except printable ASCII characters
  for i := 1 to Length(InText) do
    if (Ord(InText[i]) >= 32) AND (Ord(InText[i]) <= 126) then OutStr := OutStr + InText[i];
  ScrubText := Trim(OutStr);
end;

//Function to return number of zipper playlists in the database corresponding to
//the specified Cart ID
function TMainForm.GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
var
  OutCount: SmallInt;
begin
  try
    dmMain.Query1.Close;
    dmMain.Query1.SQL.Clear;
    Case PlaylistType of
     0: dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     1: dmMain.Query1.SQL.Add('SELECT * FROM Bug_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     2: dmMain.Query1.SQL.Add('SELECT * FROM ExtraLine_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
    end;
    dmMain.Query1.Open;
    //Get the count
    OutCount := dmMain.Query1.RecordCount;
    //Close the query and return
    dmMain.Query1.Close;
  except
    if (ErrorLoggingEnabled = True) then
      EngineInterface.WriteToErrorLog('Error occurred while trying to get playlist count');
  end;
  GetPlaylistCount := OutCount;
end;

//Function to take league ID and return league name
function TMainForm.GetLeagueName(League_ID: SmallInt): String;
var
  LeagueRecPtr: ^LeagueRec;
  OutStr: String;
begin
  OutStr := ' ';
  if (League_Collection.Count >= League_ID) then
  begin
    LeagueRecPtr := League_Collection.At(League_ID-1);
    OutStr := LeagueRecPtr^.LeagueNameShort;
  end;
  GetLeagueName := OutStr;
end;

//Function to take a template ID & return its description
function TMainForm.GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      with OutRec do
      begin
        Template_ID := TemplateDefsRecPtr^.Template_ID;
        Template_Type := TemplateDefsRecPtr^.Template_Type;
        Template_Description := TemplateDefsRecPtr^.Template_Description;
        Record_Type := TemplateDefsRecPtr^.Record_Type;
        Default_Dwell := TemplateDefsRecPtr^.Default_Dwell;
        UsesGameData := TemplateDefsRecPtr^.UsesGameData;
        RequiredGameState := TemplateDefsRecPtr^.RequiredGameState;
        Template_Has_Children := TemplateDefsRecPtr^.Template_Has_Children;
        Template_Is_Child := TemplateDefsRecPtr^.Template_Is_Child;
      end;
    end
  end;
  GetTemplateInformation := OutRec;
end;

//Function to take a league and a game phase code, and return a game phase
//record
function TMainForm.GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Label_Period := ' ';
  OutRec.Display_Period := ' ';
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := League;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Display_Period := GamePhaseRecPtr^.Display_Period;
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Final := GamePhaseRecPtr^.Display_Final;
        OutRec.Display_Extended1 := GamePhaseRecPtr^.Display_Extended1;
        OutRec.Display_Extended2 := GamePhaseRecPtr^.Display_Extended2;
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

//Function to get game time string from GT Server time value
function TMainForm.GetGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(GTimeStr, -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take the stat stored procedure name and return the stat information record
function TMainForm.GetStatInfo (StoredProcedureName: String): StatRec;
var
  i: SmallInt;
  StatRecPtr: ^StatRec; //Pointer to stat record type
  OutRec: StatRec;
begin
  OutRec.StatLeague := ' ';
  OutRec.StatType := 0;
  OutRec.StatDescription := ' ';
  OutRec.StatHeader := ' ';
  OutRec.StatStoredProcedure := ' ';
  OutRec.StatDataField := ' ';
  OutRec.StatAbbreviation := ' ';
  OutRec.UseStatQualifier := FALSE;
  OutRec.StatQualifier := ' ';
  OutRec.StatQualifierValue := 0;
  if (Stat_Collection.Count > 0) then
  begin
    for i := 0 to Stat_Collection.Count-1 do
    begin
      StatRecPtr := Stat_Collection.At(i);
      if (Trim(StatRecPtr^.StatStoredProcedure) = Trim(StoredProcedureName)) then
      begin
        OutRec.StatLeague := StatRecPtr^.StatLeague;
        OutRec.StatType := StatRecPtr^.StatType;
        OutRec.StatDescription := StatRecPtr^.StatDescription;
        OutRec.StatHeader := StatRecPtr^.StatHeader;
        OutRec.StatStoredProcedure := StatRecPtr^.StatStoredProcedure;
        OutRec.StatDataField := StatRecPtr^.StatDataField;
        OutRec.StatAbbreviation := StatRecPtr^.StatAbbreviation;
        OutRec.UseStatQualifier := StatRecPtr^.UseStatQualifier;
        OutRec.StatQualifier := StatRecPtr^.StatQualifier;
        OutRec.StatQualifierValue := StatRecPtr^.StatQualifierValue;
      end;
    end;
  end;
  GetStatInfo := OutRec;
end;

//Function to get game state
function TMainForm.GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (GTIME = '9999') OR (GPHASE = 0) then
    //Game not started
    Result := 0
  else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
    //Game is final
    Result := 3
  else if (GTIME = 'END-') then
    //End of period/quarter
    Result := 2
  else if (GPHASE <> 0) AND (StrToIntDef(GTIME, -1) <> -1) then
    //Game in progress
    Result := 1
  else
    //All other conditions
    Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
end;

//Function to get the number of fields in a template
function TMainForm.GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  If (Template_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = Template_ID) then Inc(OutVal);
    end;
  end;
  GetTemplateFieldsCount := OutVal;
end;

//Function to get the record type based on the template ID
function TMainForm.GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = Template_ID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeFromTemplateID := OutVal;
end;

//Function to return a record type description base on playlist type and record type code
function TMainForm.GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
var
  i: SmallInt;
  RecordTypeRecPtr: ^RecordTypeRec;
  OutStr: String;
begin
  OutStr := '';
  if (RecordType_Collection.Count > 1) then
  begin
    for i := 0 to RecordType_Collection.Count-1 do
    begin
      RecordTypeRecPtr := RecordType_Collection.At(i);
      if (RecordTypeRecPtr^.Playlist_Type = Playlist_Type) AND (RecordTypeRecPtr^.Record_Type = Record_Type) then
        OutStr := RecordTypeRecPtr^.Record_Description;
    end;
  end;
  GetRecordTypeDescription := OutStr;
end;

function TMainForm.GetAutomatedLeagueDisplayMnemonic (League: String): String;
var
  i: SmallInt;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec; //Pointer to team record type
  OutStr: String;
  FoundRecord: Boolean;
begin
  //Init to passed in variable; will return if no substitution found
  OutStr := League;
  if (Automated_League_Collection.Count > 0) then
  begin
    FoundRecord := FALSE;
    i := 0;
    Repeat
      AutomatedLeagueRecPtr := Automated_League_Collection.At(i);
      if (Trim(AutomatedLeagueRecPtr^.SI_LeagueCode) = League) then
      begin
        OutStr := AutomatedLeagueRecPtr^.Display_Mnemonic;
      end;
      Inc(i);
    Until (FoundRecord = TRUE) OR (i = Automated_League_Collection.Count);
  end;
  GetAutomatedLeagueDisplayMnemonic := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR GENERAL PROGRAM OPERATIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the trial playlist grid
procedure TMainForm.RefreshPlaylistGrid(PlaylistType: SmallInt);
var
  i: SmallInt;
  TickerPtr: ^TickerRec;
  BreakingNewsPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
begin
  //Init values
  Case PlaylistType of
   1: begin
        CollectionCount := Ticker_Collection.Count;
        //Clear grid values
        if (TickerPlaylistGrid.Rows > 0) then
          TickerPlaylistGrid.DeleteRows (1, TickerPlaylistGrid.Rows);
      end;
   2: begin
        CollectionCount := BreakingNews_Collection.Count;
        //Clear grid values
        if (BreakingNewsPlaylistGrid.Rows > 0) then
          BreakingNewsPlaylistGrid.DeleteRows (1, BreakingNewsPlaylistGrid.Rows);
      end;
  end;
  if (CollectionCount > 0) then
  begin
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      Case PlaylistType of
          //Ticker
       1: begin
            TickerPlaylistGrid.StoreData := TRUE;
            TickerPlaylistGrid.Cols := 6;
            TickerPlaylistGrid.Rows := CollectionCount;
            TickerPtr := Ticker_Collection.At(i);
            TickerPlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            TickerPlaylistGrid.Cell[2,i+1] := TickerPtr^.League; //League name
            TickerPlaylistGrid.Cell[3,i+1] := TickerPtr^.Enabled; //Record enable
            TickerPlaylistGrid.Cell[4,i+1] :=
              GetRecordTypeDescription(1, TickerPtr^.Record_Type);
            //Set Additional Info field
            if (TickerPtr^.Record_Type = 1) then
              TickerPlaylistGrid.Cell[5,i+1] := TickerPtr^.SponsorLogo_Name
            else if (TickerPtr^.Record_Type = 6) or (TickerPtr^.Record_Type = 7) then
              TickerPlaylistGrid.Cell[5,i+1] := TickerPtr^.UserData[1]
            else if (GetTemplateInformation(TickerPtr^.Template_ID).UsesGameData = TRUE) then
              TickerPlaylistGrid.Cell[5,i+1] := TickerPtr^.Description
            else
              TickerPlaylistGrid.Cell[5,i+1] := TickerPtr^.UserData[1];
            //Set commends
            TickerPlaylistGrid.Cell[6,i+1] := TickerPtr^.Comments; //Record enable
            //By default, show item over white background except if mini-playlist
            if (TickerPtr^.Is_Child = TRUE) then
              TickerPlaylistGrid.RowColor[i+1] := clGray
            else
              TickerPlaylistGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(Ticker_Collection.Count);
          end;
          //Bug
       2: begin
            BreakingNewsPtr := BreakingNews_Collection.At(i);
            BreakingNewsPlaylistGrid.StoreData := TRUE;
            BreakingNewsPlaylistGrid.Cols := 6;
            BreakingNewsPlaylistGrid.Rows := CollectionCount;
            BreakingNewsPlaylistGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            BreakingNewsPlaylistGrid.Cell[2,i+1] := BreakingNewsPtr^.League; //League name
            BreakingNewsPlaylistGrid.Cell[3,i+1] := BreakingNewsPtr^.Enabled; //Record enable
            BreakingNewsPlaylistGrid.Cell[4,i+1] :=
              GetRecordTypeDescription(2, BreakingNewsPtr^.Record_Type);
            BreakingNewsPlaylistGrid.Cell[5,i+1] := BreakingNewsPtr^.UserData[1];
            BreakingNewsPlaylistGrid.Cell[6,i+1] := BreakingNewsPtr^.Comments; //Record enable
            BreakingNewsPlaylistGrid.RowColor[i+1] := clWindow;
            //Update number of entries label
            NumBreakingNewsEntries.Caption := IntToStr(BreakingNews_Collection.Count);
            BreakingNewsPlaylistGrid.RowColor[i+1] := clWindow;
          end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST STORAGE,, LOADING & DELETION
////////////////////////////////////////////////////////////////////////////////
//Procedure to delete a specified zipper playlist from the database
procedure TMainForm.DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
begin
  Case PlaylistType of
      //Ticker
   0: begin
        try
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            dmMain.tblTicker_Groups.First;
            While(dmMain.tblTicker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Groups.Delete;
              end
              else dmMain.tblTicker_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblTicker_Elements.RecordCount > 0) then
          begin
            dmMain.tblTicker_Elements.First;
            While(dmMain.tblTicker_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Elements.Delete;
              end
              else dmMain.tblTicker_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'ticker playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Breaking News
   1: begin
        try
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Groups.First;
            While(dmMain.tblBreakingNews_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Groups.Delete;
              end
              else dmMain.tblBreakingNews_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Elements.First;
            While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Elements.Delete;
              end
              else dmMain.tblBreakingNews_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'Breaking News playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
  end;
end;

//Procedure to load default zipper collection from database
procedure TMainForm.LoadPlaylistCollection (PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
var
  i, j: SmallInt;
  InsertPoint: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  SelectedPlaylistID: Double;
  ExpansionIndex: SmallInt;
begin
  Case AppendMode of
      //Clear and load new collection
   0: begin
        Case PlaylistType of
            //Ticker
         1: begin
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              try
                //Set flag
                LoadingPlaylist := TRUE;
                dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);

                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then
                begin
                  //Clear the collection - only if there are entries in the playlist
                  Ticker_Collection.Clear;
                  Ticker_Collection.Pack;
                  //Iterate through all records in the table
                  repeat
                    //Check for Fantasy Stats; if so, auto-expand to include specified entries for each stat
                    if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) or
                       (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                    begin
                      if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) then
                      begin
                        ExpansionIndex := FantasyExpansionPageCount;
                      end
                      else if (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                      begin
                        ExpansionIndex := LeadersExpansionPageCount;
                      end;

                      for j := 1 to ExpansionIndex do
                      begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                          Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                          Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                          Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                          League := dmMain.Query1.FieldByName('League').AsString;
                          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                          GameID := dmMain.Query1.FieldByName('GameID').AsString;
                          SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                          SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                          StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                          StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;

                          //Store the index for the stat (1 - 10) in the first user data field
                          //If for SNY, use 2 per page - only need 5 pages
                          UserData[1] := IntToStr(j);

                          //Load nulls into all other user-defined text fields
                          for i := 2 to 25 do UserData[i] := '';
                          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                          DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                          Description := dmMain.Query1.FieldByName('Description').AsString;
                          Selected := FALSE;
                          Comments := dmMain.Query1.FieldByName('Comments').AsString;
                          //Insert the object
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                    end
                    //Not a fantasy or leaders stat, so just add to collection
                    else begin
                      GetMem (TickerRecPtr, SizeOf(TickerRec));
                      With TickerRecPtr^ do
                      begin
                        //Set values for collection record
                        Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                        Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                        Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                        Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                        League := dmMain.Query1.FieldByName('League').AsString;
                        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                        Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                        GameID := dmMain.Query1.FieldByName('GameID').AsString;
                        SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                        SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                        StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                        StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                        StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                        StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                        StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                        //Load the user-defined text fields
                        for i := 1 to 25 do
                          UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                        StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                        EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                        DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                        Description := dmMain.Query1.FieldByName('Description').AsString;
                        Selected := FALSE;
                        Comments := dmMain.Query1.FieldByName('Comments').AsString;
                        //Insert the object
                        Ticker_Collection.Insert(TickerRecPtr);
                        Ticker_Collection.Pack;
                      end;
                    end;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  until (dmMain.Query1.EOF = TRUE);
                  //Refresh the grid
                  RefreshPlaylistGrid(TICKER);
                end;
                //Clear flags
                LoadingPlaylist := FALSE;
              except
                if (ErrorLoggingEnabled = True) then
                  EngineInterface.WriteToErrorLog('Error occurred while trying to load main ticker playlist');
              end;

              //Version 4.0.10 - do initial population of game IDs collection for score alerts
              if (LastScoreAlertCollectionPlaylistID <> FloatToStr(PlaylistID)) then
              begin
                PopulateScoreAlertGamesCollection (PlaylistID);
                LastScoreAlertCollectionPlaylistID := FloatToStr(PlaylistID);
              end;
            end;
            //Breaking News
         2: begin
              //Query for the elements table for those that match the playlist ID
              try
                //Set flag
                LoadingPlaylist := TRUE;
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                //Load the collection from the database
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM BreakingNews_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then
                begin
                  //Clear the collection - only if there are entries in the collection
                  BreakingNews_Collection.Clear;
                  BreakingNews_Collection.Pack;
                  //Iterate through all records in the table
                  repeat
                    GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                    With BreakingNewsRecPtr^ do
                    begin
                      //Set values for collection record
                      Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                      Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                      Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                      League := dmMain.Query1.FieldByName('League').AsString;
                      Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                      Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                      //Load the user-defined text fields
                      for i := 1 to 2 do
                        UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                      StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                      EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                      DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                      Description := dmMain.Query1.FieldByName('Description').AsString;
                      Comments := dmMain.Query1.FieldByName('Comments').AsString;
                      Selected := FALSE;
                      //Insert the object
                      BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                      BreakingNews_Collection.Pack;
                      //Go to next record in query
                      dmMain.Query1.Next;
                    end;
                  until (dmMain.Query1.EOF = TRUE);
                  //Refresh the grid
                  RefreshPlaylistGrid(BREAKINGNEWS);
                end;
                //Clear flag
                LoadingPlaylist := FALSE;
              except
                if (ErrorLoggingEnabled = True) then
                  EngineInterface.WriteToErrorLog('Error occurred while trying to load Breaking News playlist');
              end;
            end;
        end;
      end;
      //Append to existing collection
   1: begin
        Case PlaylistType of
            //Ticker
         1: begin
              if (dmMain.tblTicker_Groups.RecordCount > 0) then
              begin
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                try
                  //Set flag
                  LoadingPlaylist := TRUE;
                  //Set controls to values from selected block
                  SelectedPlaylistID := PlaylistID;
                  dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);

                  dmMain.Query1.Close;
                  dmMain.Query1.SQL.Clear;
                  dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                           'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                  dmMain.Query1.Open;
                  if (dmMain.Query1.RecordCount > 0) then

                  //Iterate through all records in the table
                  repeat
                    //Check for Fantasy Stats; if so, auto-expand to include specified entries for each stat
                    if (EngineInterface.TemplateIsFantasy(dmMain.Query3.FieldByName('Template_ID').AsInteger)) or
                       (EngineInterface.TemplateIsLeaders(dmMain.Query3.FieldByName('Template_ID').AsInteger)) then
                    begin
                      //If for SNY, use 2 per page - only need 5 pages
                      if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) then
                      begin
                        ExpansionIndex := FantasyExpansionPageCount;
                      end
                      else if (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                      begin
                        ExpansionIndex := LeadersExpansionPageCount;
                      end;

                      for j := 1 to ExpansionIndex do
                      begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                          Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                          Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                          Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                          League := dmMain.Query1.FieldByName('League').AsString;
                          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                          GameID := dmMain.Query1.FieldByName('GameID').AsString;
                          SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                          SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                          StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                          StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;

                          //Store the index for the stat (1 - 10) in the first user data field
                          UserData[1] := IntToStr(j);

                          //Load nulls into all other user-defined text fields
                          for i := 2 to 50 do UserData[i] := '';
                          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                          DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                          Description := dmMain.Query1.FieldByName('Description').AsString;
                          Selected := FALSE;
                          Comments := dmMain.Query1.FieldByName('Comments').AsString;
                          //Insert the object
                          if (TickerPlaylistGrid.CurrentDataRow < 1) then TickerPlaylistGrid.CurrentDataRow := 1;
                          InsertPoint := TickerPlaylistGrid.CurrentDataRow-1;
                          if (AppendMode = 1) then
                            Ticker_Collection.Insert(TickerRecPtr)
                          else if (AppendMode = 2) then
                            Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                    end
                    //Not a fantasy or leaders stat, so just add to collection
                    else begin
                      GetMem (TickerRecPtr, SizeOf(TickerRec));
                      With TickerRecPtr^ do
                      begin
                        //Set values for collection record
                        Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                        Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                        Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                        Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                        League := dmMain.Query1.FieldByName('League').AsString;
                        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                        Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                        GameID := dmMain.Query1.FieldByName('GameID').AsString;
                        SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                        SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                        StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                        StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                        StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                        StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                        StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                        //Load the user-defined text fields
                        for i := 1 to 25 do
                          UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                        StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                        EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                        DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                        Description := dmMain.Query1.FieldByName('Description').AsString;
                        Selected := FALSE;
                        Comments := dmMain.Query1.FieldByName('Comments').AsString;
                        //Insert the object
                        if (TickerPlaylistGrid.CurrentDataRow < 1) then TickerPlaylistGrid.CurrentDataRow := 1;
                        InsertPoint := TickerPlaylistGrid.CurrentDataRow-1;
                        if (AppendMode = 1) then
                          Ticker_Collection.Insert(TickerRecPtr)
                        else if (AppendMode = 2) then
                          Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                        Ticker_Collection.Pack;
                      end;
                    end;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  until (dmMain.Query1.EOF = TRUE);
                  //Refresh the grid
                  RefreshPlaylistGrid(TICKER);
                  //Clear flag
                  LoadingPlaylist := FALSE;
               except
                  if (ErrorLoggingEnabled = True) then
                    EngineInterface.WriteToErrorLog('Error occurred while trying to load main ticker playlist');
                end;
              end;
              //Version 4.0.10 - do initial population of game IDs collection for score alerts
              PopulateScoreAlertGamesCollection (PlaylistID);
            end;
            //Breaking News
         2: begin
            end;
        end;
      end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TRIGGER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to trigger the ticker
procedure TMainForm.TriggerTickerButtonClick(Sender: TObject);
begin
  TriggerCurrentTicker;
end;

////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger Currently scheduled ticker
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.TriggerCurrentTicker;
begin
  //Make sure there is data in the zipper collection
  if (Ticker_Collection.Count > 0) AND (RunningTicker = FALSE) then
  begin
    //Set flag to indicate we're running the default zipper; also prevents search for
    //new playlist in schedule while running stored procedures
    RunningTicker := TRUE;

    //Disable reset to top button
    ResetTickerButton.Enabled := FALSE;

    //Make sure packet timeout timer is disabled
    EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;

    //Reset sponsor logo dwell counter
    SponsorLogoDwellCounter := 9999;

    //Set labels
    TickerStatusLabel.Color := clLime;
    TickerStatusLabel.Caption := 'TICKER RUNNING';
    LastCommandLabel.Caption := 'Trigger Current Ticker';

    //Clear the abort flag
    TickerAbortFlag := FALSE;
    //Refresh the zipper grid
    RefreshPlaylistGrid(1);
    //Set the data
    EngineInterface.StartTickerData;

    //If Current Entry index is 0, where at the top, so restart the wheel timer
   if (CurrentTickerEntryIndex = 0) then ElapsedTimeThisWheel := 0;
  end;
end;

//Handler for abort ticker event
procedure TMainForm.AbortPlaylistButtonClick(Sender: TObject);
begin
  if (RunningTicker) then
  begin
    //Abort
    AbortCurrentEvent;
  end;
end;

//Procedure for aborting ticker
procedure TMainForm.AbortCurrentEvent;
var
  SavePos: SmallInt;
begin
  //Clear flags & handles
  LastSegmentHeading := '';
  RunningTicker := FALSE;
  LastGameIDDisplayed := '0';

  //Make sure packet timeout timer is disabled
  EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;

  //Enable reset to top button
  ResetTickerButton.Enabled := TRUE;

  //Set labels
  TickerStatusLabel.Color := clRed;
  TickerStatusLabel.Caption := 'TICKER HALTED';
  LastCommandLabel.Caption := 'Abort Current Event';

  //Set the abort flag & reset zipper entry index
  TickerAbortFlag := TRUE;

  //Set controls to abort current event locally
  EngineInterface.TickerOut;

  //Search to see if there's a new playlist scheduled immediately
  //ticker mode
  SearchForNextScheduledPlaylist(FALSE);
end;

procedure TMainForm.TimeOfDayTimerTimer(Sender: TObject);
var
  Hour, Min, Sec, mSec: Word;
  TimeStr: String;
begin
  //Increment sponsor logo dwell counter if ticker is running
  if (RunningTicker = TRUE) then
  begin
    Inc(SponsorLogoDwellCounter);
    //Update the elapsed time counter
    Inc(ElapsedTimeThisWheel);
    Hour := ElapsedTimeThisWheel DIV (60*60);
    Min :=  ElapsedTimeThisWheel DIV 60 - (Hour*60);
    Sec :=  ElapsedTimeThisWheel - ((Hour*60*60) + (Min*60));
    TimeStr := Format('%2.2d:%2.2d:%2.2d', [Hour, Min, Sec]);

  end;
  //Update the time of day clock
  DecodeTime(Now, Hour, Min, Sec, mSec);
  if ((Sec MOD 30) = 0) then
    //EngineInterface.UpdateTimeOfDayClocks;     removed clock anthony

  //Update clock labels
  Label2.Caption := DateToStr(Now);
  Label3.Caption := TimeToStr(Now);
end;

//Handler to flash the error panel red if an error occurs
procedure TMainForm.ErrorFlashTimerTimer(Sender: TObject);
begin
  //If error condition flag is set, start flashing status box red
  If (Error_Condition = True) then
  begin
     if (Panel3.Color = clBtnFace) then Panel3.Color := clRed
     else Panel3.Color := clBtnFace;
  end;
end;

//Handler for check to new scheduled playlist
procedure TMainForm.PlaylistRefreshTimerTimer(Sender: TObject);
begin
  //Not for looping ticker mode
  SearchForNextScheduledPlaylist(FALSE);
end;
//Spawn thread to check for new schedule
procedure TMainForm.SearchForNextScheduledPlaylist(LoopingTicker: Boolean);
var
  SaveTickerEntryIndex: SmallInt;
  SaveRow: SmallInt;
begin
  //Modified to allow thread to run if ticker is running so it picks up alerts
  //if (RunningThread = FALSE) AND (RunningTicker = FALSE) then
  if (RunningThread = FALSE) then
  begin
    //Check for schedule monitoring enable
    if (ScheduleMonitoringEnabled) then
    begin
      //Spawn thread to check ticker playlist schedule
      CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
      CheckTickerScheduleThread.Priority := tpLower;
      CheckTickerScheduleThread.FreeOnTerminate := TRUE;
      //Execute the thread
      try
        CheckTickerScheduleThread.Resume;
      except
        CheckTickerScheduleThread.Free;
        if (ErrorLoggingEnabled = True) then
          EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
      end;
    end
    //Manual mode - only update if ticker not running
    else begin
      //Spawn thread - will check for Breaking News only if ticker is not running
      CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
      CheckTickerScheduleThread.Priority := tpLower;
      CheckTickerScheduleThread.FreeOnTerminate := TRUE;
      //Execute the thread
      try
        CheckTickerScheduleThread.Resume;
      except
        CheckTickerScheduleThread.Free;
        if (ErrorLoggingEnabled = True) then
          EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
      end;
      //Reload playlist if ticker not running
      if (RunningTicker = FALSE) then
      begin
        //Save current index so it can be restored
        SaveTickerEntryIndex := CurrentTickerEntryIndex;
        SaveRow := TickerPlaylistGrid.CurrentDataRow;
        //Load the manual collection
        LoadPlaylistCollection(1, 0, CurrentTickerPlaylistID);
        RefreshPlaylistGrid(1);
        if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) then
        begin
          CurrentTickerEntryIndex := SaveTickerEntryIndex;
          if (SaveRow <= Ticker_Collection.Count) then TickerPlaylistGrid.CurrentDataRow := SaveRow;
        end
        else begin
          CurrentTickerEntryIndex := 0;
          if (Ticker_Collection.Count > 0) then
            TickerPlaylistGrid.CurrentDataRow := 1;
        end;
      end;
    end;
  end;
end;
//GPI reset timer
procedure TMainForm.GPIResetTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retrigger
  GPIResetTimer.Enabled := FALSE;
  //Re-enable GPIs
  GPIsOKToFire := TRUE;
  //Turn off LEDs
  ApdStatusLight3.Lit := FALSE;
  ApdStatusLight4.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure sets up a timer to trigger 1 second after program startup.
// When triggered, database queries and the input com port are enabled.
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.GPIInitTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retrigger
  GPIInitTimer.Enabled := FALSE;
  //Enable GPIs to fire
  GPIsOKToFire := TRUE;
end;

//Handler for reset ticker to top
procedure TMainForm.ResetTickerButtonClick(Sender: TObject);
begin
  ResetTickerToTop;
end;
procedure TMainForm.ResetTickerToTop;
begin
  //Set the zipper story to start with
  CurrentTickerEntryIndex := 0;
  CurrentBreakingNewsEntryIndex := 0;
  //Set data row indicator in zipper grid
  MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
  MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
  MainForm.BreakingNewsPlaylistGrid.CurrentDataRow := CurrentBreakingNewsEntryIndex+1;
  MainForm.BreakingNewsPlaylistGrid.SetTopLeft(1, MainForm.BreakingNewsPlaylistGrid.CurrentDataRow);
end;

//Handler to enable error logging
procedure TMainForm.EnableErrorChecking1Click(Sender: TObject);
begin
  ErrorLoggingEnabled := True;
  EnableErrorChecking1.Checked := True;
  DisableErrorChecking1.Checked := False;
  StaticText27.Visible := False;
end;

//Handler to disable error logging
procedure TMainForm.DisableErrorChecking1Click(Sender: TObject);
begin
  ErrorLoggingEnabled := False;
  EnableErrorChecking1.Checked := False;
  DisableErrorChecking1.Checked := True;
  StaticText27.Visible := True;
  Panel3.Color := clLime;
end;

//Handler to reset error
procedure TMainForm.ResetError1Click(Sender: TObject);
begin
  //Reset error flag and status panel color
  Error_Condition := False;

  //Reset captions for labels on status panel
  Label5.Caption := 'OK';
  Panel3.Color := clLime;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database connection problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseConnectError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    Label15.Caption := 'ERROR';

    //WriteToErrorLog procedure defined in Engine Interface unit
    EngineInterface.WriteToErrorLog (ExceptionString);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database engine problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseEngineError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    Label15.Caption := 'ERROR';

    //WriteToErrorLog procedure defined in Engine Interface unit
    EngineInterface.WriteToErrorLog (ExceptionString);
  end;
end;

procedure TMainForm.SetGraphicsEngineConnectionPreferences1Click(
  Sender: TObject);
var
  Modal: TEnginePrefsDlg;
  Control: Word;
begin
  Modal := TEnginePrefsDlg.Create (Application);
  //Pre-load with values read from prefs file; if no prefs file is
  //found, the default values are loaded
  Modal.Edit2.Text := EngineParameters.IPAddress;
  Modal.Edit3.Text := EngineParameters.Port;
  //Note: Service information setup dynamically during dialog creation V3.0
  try
    Control := Modal.ShowModal;
  finally
    //If confirmed, set new values and store prefs
    if (Control = mrOK) then
    begin
      EngineParameters.IPAddress := Trim(Modal.Edit2.Text);
      EngineParameters.Port := Trim(Modal.Edit3.Text);
      //Store the preferences
      StorePrefs;
    end;
    Modal.Free;
  end;
end;

//Handler to reconnect to graphics engine
procedure TMainForm.ReconnecttoGraphicsEngine1Click(Sender: TObject);
begin
  //Connect to the graphics engine if it's enabled
  try
      EngineInterface.EnginePort.Active := FALSE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      //WriteToErrorLog
      EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
    end;
  end;
end;

//Handler for selecting playlist for manual load
procedure TMainForm.BitBtn4Click(Sender: TObject);
var
  i: SmallInt;
  Modal: TPlaylistSelectDlg;
  Control: Word;
begin
  //Display dialog for playlist selection
  Modal := TPlaylistSelectDlg.Create (Application);
  try
    //Refresh playlists table
    //dmMain.tblTicker_Groups.Active := FALSE;
    //dmMain.tblTicker_Groups.Active := TRUE;
    dmMain.PlaylistQuery.Close;
    dmMain.PlaylistQuery.SQL.Clear;
    dmMain.PlaylistQuery.SQL.Add('SELECT * FROM Ticker_Groups');
    dmMain.PlaylistQuery.Open;
    //Set flag to prevent thread from updating playlist
    RunningThread := TRUE;
    Control := Modal.ShowModal;
  finally
    //If confirmed, set new values and store prefs
    if (Control = mrOK) then
    begin
      //Load the collection; set flag to indicate it's the first time through
      //LoadPlaylistCollection(1, 0, dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat);
      LoadPlaylistCollection(1, 0, dmMain.PlaylistQuery.FieldByName('Playlist_ID').AsFloat);

      //Reset ticker to top
      ResetTickerToTop;

      //Refresh the zipper grid and status labels
      //Label5Caption := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
      RefreshPlaylistGrid(1);
      //Set the current playlist ID - allows for refresh of collection of more than one iteration
      //CurrentTickerPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
      CurrentTickerPlaylistID := dmMain.PlaylistQuery.FieldByName('Playlist_ID').AsFloat;

      //Update data fields
      //ScheduledPlaylistInfo[1].Playlist_Description := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
      ScheduledPlaylistInfo[1].Playlist_Description := dmMain.PlaylistQuery.FieldByName('Playlist_Description').AsString;
      //ScheduledPlaylistInfo[1].Playlist_ID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
      ScheduledPlaylistInfo[1].Playlist_ID := dmMain.PlaylistQuery.FieldByName('Playlist_ID').AsFloat;
      //ScheduledPlaylistInfo[1].Station_ID := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger;
      ScheduledPlaylistInfo[1].Station_ID := dmMain.PlaylistQuery.FieldByName('Station_ID').AsInteger;
      //ScheduledPlaylistInfo[1].Ticker_Mode := dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger;
      ScheduledPlaylistInfo[1].Ticker_Mode := dmMain.PlaylistQuery.FieldByName('Ticker_Mode').AsInteger;

      MainForm.TickerPlaylistName.Caption := ScheduledPlaylistInfo[1].Playlist_Description;
      MainForm.TickerStartTimeLabel.Caption := 'N/A';
      MainForm.TickerEndTimeLabel.Caption := 'N/A';

      //Set the ticker mode and refresh the controls
      //TickerDisplayMode := dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger;
      TickerDisplayMode := dmMain.PlaylistQuery.FieldByName('Ticker_Mode').AsInteger;
      Case TickerDisplayMode of
           //1-line, 1-time
        1: begin
             MainForm.OneTimeLoopModeLabel.Visible := TRUE;
             MainForm.TickerMode.Caption := '1-LINE MODE';
           end;
           //1-line, loop
        2: begin
             MainForm.OneTimeLoopModeLabel.Visible := FALSE;
             MainForm.TickerMode.Caption := '1-LINE MODE';
           end;
           //2-line, 1-time
        3: begin
             MainForm.OneTimeLoopModeLabel.Visible := TRUE;
             MainForm.TickerMode.Caption := '2-LINE MODE';
           end;
           //2-line, loop
        4: begin
             MainForm.OneTimeLoopModeLabel.Visible := FALSE;
             MainForm.TickerMode.Caption := '2-LINE MODE';
           end;
      end;

      //Version 3.2.0 10/26/11 Clear the sponsor logo information
      //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
      CurrentSponsorInfo.CurrentSponsorLogoName := '';
      CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
      CurrentSponsorInfo.CurrentSponsorTemplate := 0;
    end;
    dmMain.PlaylistQuery.Close;
    Modal.Free;
    //Set flag to prevent thread from updating playlist
    RunningThread := FALSE;
  end;
end;

procedure TMainForm.ScheduleMonitoringEnabledCheckBoxClick(Sender: TObject);
begin
  //Set state of schedule monitoring
  if (ScheduleMonitoringEnabledCheckBox.Checked = TRUE) then
  begin
    ScheduleMonitoringEnabled := TRUE;
    BitBtn4.Visible := FALSE;
    Label6.Visible := FALSE;
    //Enable timer to check for scheduled playlist
    //PlaylistRefreshTimer.Enabled := TRUE;
  end
  //In manual mode
  else begin
    //Clear flag; disble controls; show label
    ScheduleMonitoringEnabled := FALSE;
    BitBtn4.Visible := TRUE;
    Label6.Visible := TRUE;
    //Disable timer to check for scheduled playlist
    //PlaylistRefreshTimer.Enabled := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES TO LOAD & STORE PREFS HERE BEACUSAE OF WEIRD DEBUGGER ISSUES -
// BREAKPOINTS NOT IN CORRECT PLACE
////////////////////////////////////////////////////////////////////////////////
//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'CSN_XPression_Playout.ini');
    //Read the settings
    ForceUpperCase := PrefsINI.ReadBool('General Settings', 'Force Upper Case', FALSE);
    GPIsEnabled := PrefsINI.ReadBool('General Settings', 'GPIs Enabled', FALSE);
    GPICOMPort := PrefsINI.ReadInteger('General Settings', 'GPI COM Port', 1);
    GPIBaudRate:= PrefsINI.ReadInteger('General Settings', 'GPI Baud Rate', 19200);
    ScheduleMonitoringEnabled := PrefsINI.ReadBool('General Settings', 'Schedule Monitoring Enabled', TRUE);
    //Get game start time offset - expressed in hours, so divide by 24 to get value
    GameStartTimeOffset := (PrefsINI.ReadInteger('General Settings', 'Game Start Time Offset from ET (hours)', 0))/24;
    TimeZoneSuffix := PrefsINI.ReadString('General Settings', 'Time Zone Suffix', 'ET');
    StationID := PrefsINI.ReadInteger('General Settings', 'Station ID Number', 1);
    StationDescription := PrefsINI.ReadString('General Settings', 'Station Description', 'SF BAY AREA');
    EnableTemplateFieldFormatting := PrefsINI.ReadBool('General Settings', 'Enable Template Field Formatting', TRUE);
    EnablePersistentSponsorLogo := PrefsINI.ReadBool('General Settings', 'Enable Persistent Sponsor Logo', TRUE);
    GPILockout := PrefsINI.ReadInteger('General Settings', 'GPI Lockout', 7000);

    //For fantasy stats - used to trigger expansion into multiple templates
    FantasyExpansionPageCount := PrefsINI.ReadInteger('General Settings', 'Fantasy Expansion Page Count (must be even)', 10);
    //For leader stats - used to trigger expansion into multiple templates
    LeadersExpansionPageCount := PrefsINI.ReadInteger('General Settings', 'Leaders Expansion Page Count (must be even)', 10);

    ShowTimeAndDay := PrefsINI.ReadBool('General Settings', 'Show Time and Day for Schedule', FALSE);

    UseManualRankings_NCAAB := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAB)', FALSE);
    UseManualRankings_NCAAF := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAF)', FALSE);
    UseManualRankings_NCAAW := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAW)', FALSE);
    UseSeedingForNCAABGames := PrefsINI.ReadBool('General Settings', 'Use Seeding for NCAAB Games', FALSE);
    UseBCSRankings_NCAAF := PrefsINI.ReadBool('General Settings', 'Use BCS Rankings (NCAAF)', TRUE);
    TickerBedInDelay := PrefsINI.ReadInteger('General Settings', 'Ticker Bed In Delay (mS)', 1500);
    LineupInDelay := PrefsINI.ReadInteger('General Settings', 'Lineup In Delay (mS)', 4000);
    LineupOutDelay := PrefsINI.ReadInteger('General Settings', 'Lineup Out Delay (mS)', 2800);
    LineupToFirstPageDelay := PrefsINI.ReadInteger('General Settings', 'Lineup to First Page Delay (mS)', 1000);

    FantasyStartTemplate := PrefsINI.ReadInteger('General Settings', 'Fantasy Start Template', 200);
    FantasyEndTemplate := PrefsINI.ReadInteger('General Settings', 'Fantasy End Template', 219);
    LeadersStartTemplate := PrefsINI.ReadInteger('General Settings', 'Leaders Start Template', 300);
    LeadersEndTemplate := PrefsINI.ReadInteger('General Settings', 'Leaders End Template', 336);
    RunAnimationOnTickerOut := PrefsINI.ReadBool('General Settings', 'Run Animation on Ticker Out', TRUE);

    Score_Alert_Time_Window := PrefsINI.ReadInteger('General Settings', 'Score Alert Time Window (Minutes)', 10);
    TickerInAnimationDelay := PrefsINI.ReadInteger('General Settings', 'Ticker In Animation Delay (mS)', 10);

    EnableTeamRecords_MLB := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (MLB)', TRUE);
    EnableTeamRecords_NBA := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NBA)', TRUE);
    EnableTeamRecords_NFL := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NFL)', TRUE);
    EnableTeamRecords_NHL := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NHL)', TRUE);
    EnableTeamRecords_NCAAB := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NCAAB)', TRUE);
    EnableTeamRecords_NCAAF := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NCAAF)', TRUE);
    EnableTeamRecords_NCAAW := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (NCAAW)', TRUE);
    EnableTeamRecords_MLS := PrefsINI.ReadBool('Format Settings', 'EnableTeamRecords (MLS)', TRUE);

    LOG_QUEUE_COMMANDS := PrefsINI.ReadBool('Debug Settings', 'Log Queue Commands', FALSE);
    LOG_OUTGOING_COMMANDS := PrefsINI.ReadBool('Debug Settings', 'Log Outgoing Commands', TRUE);
    LOG_DELAYS := PrefsINI.ReadBool('Debug Settings', 'Log Calculated Delays', FALSE);
    LOG_ENGINE_RESPONSES := PrefsINI.ReadBool('Debug Settings', 'Log Engine Responses', TRUE);
    LOG_TIMER_EVENTS := PrefsINI.ReadBool('Debug Settings', 'Log Timer Events', TRUE);

    //Set labels
    StationIDNumLabel.Caption := IntToStr(StationID);
    StationIDLabel.Caption := StationDescription;
    DBConnectionString := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    DBConnectionString2 := PrefsINI.ReadString('Database Connections', 'Sportbase Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=Sportbase;Data Source=(local)');
    EngineParameters.IPAddress := PrefsINI.ReadString('Ticker Engine Connection', 'IP Address',
      '127.0.0.1');
    EngineParameters.Port := PrefsINI.ReadString('Ticker Engine Connection', 'Port', '7795');
    SceneName := PrefsINI.ReadString('Ticker Engine Connection', 'Scene Name', 'Scene1');
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store usee preferences
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'CSN_XPression_Playout.ini');
    //Save the settings
    PrefsINI.WriteBool('General Settings', 'Force Upper Case', ForceUpperCase);
    PrefsINI.WriteBool('General Settings', 'GPIs Enabled', GPIsEnabled);
    PrefsINI.WriteInteger('General Settings', 'GPI COM Port', GPICOMPort);
    PrefsINI.WriteInteger('General Settings', 'GPI Baud Rate', GPIBaudRate);
    PrefsINI.WriteBool('General Settings', 'Schedule Monitoring Enabled', ScheduleMonitoringEnabled);
    PrefsINI.WriteInteger('General Settings', 'Game Start Time Offset from ET (hours)', Trunc(GameStartTimeOffset*24));
    PrefsINI.WriteString('General Settings', 'Time Zone Suffix', TimeZoneSuffix);
    PrefsINI.WriteInteger('General Settings', 'Station ID Number', StationID);
    PrefsINI.WriteString('General Settings', 'Station Description', StationDescription);
    PrefsINI.WriteBool('General Settings', 'Enable Template Field Formatting', EnableTemplateFieldFormatting);
    PrefsINI.WriteBool('General Settings', 'Enable Persistent Sponsor Logo', EnablePersistentSponsorLogo);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);

    //For fantasy stats - used to trigger expansion into multiple templates
    PrefsINI.WriteInteger('General Settings', 'Fantasy Expansion Page Count (must be even)', FantasyExpansionPageCount);
    //For leader stats - used to trigger expansion into multiple templates
    PrefsINI.WriteInteger('General Settings', 'Leaders Expansion Page Count (must be even)', LeadersExpansionPageCount);

    PrefsINI.WriteBool('General Settings', 'Show Time and Day for Schedule', ShowTimeAndDay);

    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAB)', UseManualRankings_NCAAB);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAF)', UseManualRankings_NCAAF);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAW)', UseManualRankings_NCAAW);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);
    PrefsINI.WriteBool('General Settings', 'Use BCS Rankings (NCAAF)', UseBCSRankings_NCAAF);
    PrefsINI.WriteInteger('General Settings', 'Ticker Bed In Delay (mS)', TickerBedInDelay);
    PrefsINI.WriteInteger('General Settings', 'Lineup In Delay (mS)', LineupInDelay);
    PrefsINI.WriteInteger('General Settings', 'Lineup Out Delay (mS)', LineupOutDelay);
    PrefsINI.WriteInteger('General Settings', 'Lineup to First Page Delay (mS)', LineupToFirstPageDelay);

    PrefsINI.WriteInteger('General Settings', 'Fantasy Start Template', FantasyStartTemplate);
    PrefsINI.WriteInteger('General Settings', 'Fantasy End Template', FantasyEndTemplate);
    PrefsINI.WriteInteger('General Settings', 'Leaders Start Template', LeadersStartTemplate);
    PrefsINI.WriteInteger('General Settings', 'Leaders End Template', LeadersEndTemplate);
    PrefsINI.WriteBool('General Settings', 'Run Animation on Ticker Out', RunAnimationOnTickerOut);

    PrefsINI.WriteInteger('General Settings', 'Score Alert Time Window (Minutes)', Score_Alert_Time_Window);
    PrefsINI.WriteInteger('General Settings', 'Ticker In Animation Delay (mS)', TickerInAnimationDelay);

    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (MLB)', EnableTeamRecords_MLB);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NBA)', EnableTeamRecords_NBA);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NFL)', EnableTeamRecords_NFL);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NHL)', EnableTeamRecords_NHL);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NCAAB)', EnableTeamRecords_NCAAB);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NCAAF)', EnableTeamRecords_NCAAF);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (NCAAW)', EnableTeamRecords_NCAAW);
    PrefsINI.WriteBool('Format Settings', 'EnableTeamRecords (MLS)', EnableTeamRecords_MLS);

    PrefsINI.WriteBool('Debug Settings', 'Log Queue Commands', LOG_QUEUE_COMMANDS);
    PrefsINI.WriteBool('Debug Settings', 'Log Outgoing Commands', LOG_OUTGOING_COMMANDS);
    PrefsINI.WriteBool('Debug Settings', 'Log Calculated Delays', LOG_DELAYS);
    PrefsINI.WriteBool('Debug Settings', 'Log Engine Responses', LOG_ENGINE_RESPONSES);
    PrefsINI.WriteBool('Debug Settings', 'Log Timer Events', LOG_TIMER_EVENTS);

    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String', DBConnectionString);
    PrefsINI.WriteString('Database Connections', 'Sportbase Database Connection String', DBConnectionString2);
    PrefsINI.WriteString('Ticker Engine Connection', 'IP Address', EngineParameters.IPAddress);
    PrefsINI.WriteString('Ticker Engine Connection', 'Port', EngineParameters.Port);
    PrefsINI.WriteString('Ticker Engine Connection', 'Scene Name', SceneName);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Handler for GPI packet
procedure TMainForm.ApdDataPacket1StringPacket(Sender: TObject; Data: String);
begin
  //Only proceed if GPIs enabled
  if (GPIsOKToFire = TRUE) AND (GPIsEnabled = TRUE) AND (Length(Data) >= 3) then
  begin
    //Check for GPI #1 - Trigger Current Ticker
    if (Data[3] = '1') then
    begin
      //Call procedure to trigger next event
      TriggerCurrentTicker;
      //Set indicator
      ApdStatusLight3.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Interval := GPILockout;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to start/resume ticker');
    end
    else ApdStatusLight3.Lit := FALSE;
    //Check for GPI #2 - Abort Ticker
    if (Data[3] = '2') then
    begin
      //Call procedure to key bug in
      AbortCurrentEvent;
      //Set indicator
      ApdStatusLight4.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Interval := GPILockout;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to stop ticker');
    end
    else ApdStatusLight4.Lit := FALSE;
    //Check for GPI #3 - Reset Ticker to Top and Trigger
    if (Data[3] = '3') then
    begin
      //Reset ticker to top
      ResetTickerToTop;
      //Call procedure to trigger next event
      TriggerCurrentTicker;
      //Set indicator
      ApdStatusLight3.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to reset & start ticker');
    end
    else ApdStatusLight3.Lit := TRUE;
  end
  //Not enabled; make sure all lights not lit
  else begin
    ApdStatusLight3.Lit := FALSE;
    ApdStatusLight4.Lit := FALSE;
  end;
end;

//Handler to enable command logging
procedure TMainForm.EnableCommandLogging1Click(Sender: TObject);
begin
  CommandLoggingEnabled := TRUE;
end;

//Handler to disable command logging
procedure TMainForm.DisableCommandLogging1Click(Sender: TObject);
begin
  CommandLoggingEnabled := TRUE;
end;

//Handler to use NCAAB Ranks
procedure TMainForm.UseRankingsforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := TRUE;
  UseSeedforNCAAB.Checked := FALSE;
  UseSeedingForNCAABGames := FALSE;
  StorePrefs;
end;

//Handler to use NCAAB Seeds
procedure TMainForm.UseSeedforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := FALSE;
  UseSeedforNCAAB.Checked := TRUE;
  UseSeedingForNCAABGames := TRUE;
  StorePrefs;
end;

//Handler for clicking grid - allows operator to set first entry to be displayed when triggering ticker
procedure TMainForm.TickerPlaylistGridClick(Sender: TObject);
var
  Control: Word;
begin
  //Don't allow if ticker not running
  if (RunningTicker = FALSE) then
  begin
    //Prompt operator for confirmation
    Control := MessageDlg('Do you want to set the current entry in the playlist as the starting ' +
                 'entry for the next time the ticker is started?', mtConfirmation, [mbyes, mbNo], 0);
    //If confirned, set current entry index for ticker
    if (Control = mrYes) then
    begin
      CurrentTickerEntryIndex := MainForm.TickerPlaylistGrid.CurrentDataRow-1;
      MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
    end;
  end;
end;

//Show/hide command out log window
procedure TMainForm.S1Click(Sender: TObject);
begin
  CommandOutList.Visible := TRUE;
  ClearCommandListBtn.Visible := TRUE;
end;
procedure TMainForm.H1Click(Sender: TObject);
begin
  CommandOutList.Visible := FALSE;
  ClearCommandListBtn.Visible := FALSE;
end;

//Handler to clear all graphics
procedure TMainForm.C1Click(Sender: TObject);
var
  CmdStr: String;
begin
  //Build & send command strings
  //Re-open scene
  //Add command to queue
  CmdStr := 'B\OP\' + SceneName + '\\';
  //Add command to queue
  EngineInterface.AddCommandToQueue(CmdStr, 50);
end;

//handler to clear command list
procedure TMainForm.ClearCommandListBtnClick(Sender: TObject);
begin
  CommandOutList.Lines.Clear;
end;

end.

