object PlaylistSelectDlg: TPlaylistSelectDlg
  Left = 666
  Top = 403
  BorderStyle = bsDialog
  Caption = 'Playlist Selection for Load'
  ClientHeight = 333
  ClientWidth = 625
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn2: TBitBtn
    Left = 214
    Top = 292
    Width = 198
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkClose
  end
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 593
    Height = 273
    BevelWidth = 2
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 134
      Height = 16
      Caption = 'Available Playlists:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object AvailablePlaylistGrid: TtsDBGrid
      Left = 16
      Top = 35
      Width = 560
      Height = 182
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      ColMoving = False
      Cols = 3
      ColSelectMode = csNone
      DatasetType = dstStandard
      DataSource = dmMain.dsPlaylistQuery
      DefaultRowHeight = 20
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridMode = gmListBox
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 18
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      RowSelectMode = rsSingle
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      OnDblClick = AvailablePlaylistGridDblClick
      OnRowChanged = AvailablePlaylistGridRowChanged
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'Playlist_Description'
          Col.FieldName = 'Playlist_Description'
          Col.Heading = 'Playlist Description'
          Col.Width = 250
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'Entry_Operator'
          Col.FieldName = 'Entry_Operator'
          Col.Heading = 'Operator'
          Col.Width = 120
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'Entry_Date'
          Col.FieldName = 'Entry_Date'
          Col.Heading = 'Entry Date'
          Col.Width = 154
          Col.AssignedValues = '?'
        end>
    end
    object BitBtn1: TBitBtn
      Left = 74
      Top = 227
      Width = 198
      Height = 33
      Caption = '&Load New Playlist'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
        07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
        0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
        33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object BitBtn3: TBitBtn
      Left = 320
      Top = 228
      Width = 198
      Height = 33
      Caption = 'Append to Current List'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 6
      ParentFont = False
      TabOrder = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555000000
        000055555F77777777775555000FFFFFFFF0555F777F5FFFF55755000F0F0000
        FFF05F777F7F77775557000F0F0FFFFFFFF0777F7F7F5FFFFFF70F0F0F0F0000
        00F07F7F7F7F777777570F0F0F0FFFFFFFF07F7F7F7F5FFFFFF70F0F0F0F0000
        00F07F7F7F7F777777570F0F0F0FFFFFFFF07F7F7F7F5FFF55570F0F0F0F000F
        FFF07F7F7F7F77755FF70F0F0F0FFFFF00007F7F7F7F5FF577770F0F0F0F00FF
        0F057F7F7F7F77557F750F0F0F0FFFFF00557F7F7F7FFFFF77550F0F0F000000
        05557F7F7F77777775550F0F0000000555557F7F7777777555550F0000000555
        55557F7777777555555500000005555555557777777555555555}
      NumGlyphs = 2
    end
  end
end
