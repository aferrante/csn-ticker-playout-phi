unit ZipperEntryEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, AdStatLt, ExtCtrls, {ad3SpellBase, ad3Spell,} Grids_ts,
  TSGrid, OoMisc, ComCtrls, ad3SpellBase, ad3Spell, Globals;

type
  TZipperEntryEditorDlg = class(TForm)
    Panel11: TPanel;
    Label1: TLabel;
    SpellCheckIndicator: TApdStatusLight;
    Label35: TLabel;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn1: TBitBtn;
    RecordGrid: TtsGrid;
    TemplateName: TLabel;
    Panel4: TPanel;
    Label38: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EntryEnable: TCheckBox;
    EntryNote: TEdit;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    HiddenEdit: TEdit;
    AddictSpell31: TAddictSpell3;
    procedure BitBtn2Click(Sender: TObject);
    procedure RecordGridCellChanged(Sender: TObject; OldCol, NewCol,
      OldRow, NewRow: Integer);
    procedure RecordGridCellEdit(Sender: TObject; DataCol,
      DataRow: Integer; ByUser: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ZipperEntryEditorDlg: TZipperEntryEditorDlg;

implementation

uses Main;

{$R *.DFM}

//Perform spell check on entry
procedure TZipperEntryEditorDlg.BitBtn2Click(Sender: TObject);
begin
  //Set spellchecker data dictionary
  AddictSpell31.ConfigDictionaryDir.Add (SpellCheckerDictionaryDir);
  //Launch the spell checker
  HiddenEdit.Text := RecordGrid.Cell[3, RecordGrid.CurrentDataRow];
  AddictSpell31.CheckWinControl(HiddenEdit, ctAll);
  //Light the indicator
  SpellCheckIndicator.Lit := TRUE;
end;

//Handlers to turn off spell check indicator
procedure TZipperEntryEditorDlg.RecordGridCellChanged(Sender: TObject;
  OldCol, NewCol, OldRow, NewRow: Integer);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;
procedure TZipperEntryEditorDlg.RecordGridCellEdit(Sender: TObject;
  DataCol, DataRow: Integer; ByUser: Boolean);
begin
  //Extinguish spell check light
  SpellCheckIndicator.Lit := FALSE;
end;

end.
