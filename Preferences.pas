unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, StBase, {StShBase, StBrowsr,} Spin, Mask;

type
  TPrefs = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Panel4: TPanel;
    RadioGroup4: TRadioGroup;
    Panel3: TPanel;
    RadioGroup1: TRadioGroup;
    Panel7: TPanel;
    StaticText3: TStaticText;
    Edit1: TEdit;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    Panel1: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    StationIDDesc: TEdit;
    Label3: TLabel;
    StationIDNum: TSpinEdit;
    Panel6: TPanel;
    Label5: TLabel;
    EnableTeamRecords_Checkbox_MLB: TCheckBox;
    EnableTeamRecords_Checkbox_NBA: TCheckBox;
    EnableTeamRecords_Checkbox_NFL: TCheckBox;
    EnableTeamRecords_Checkbox_NHL: TCheckBox;
    EnableTeamRecords_Checkbox_NCAAB: TCheckBox;
    EnableTeamRecords_Checkbox_NCAAF: TCheckBox;
    EnableTeamRecords_Checkbox_NCAAW: TCheckBox;
    EnableTeamRecords_Checkbox_MLS: TCheckBox;
    Panel8: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Prefs: TPrefs;

implementation

{$R *.DFM}

end.
