object Prefs: TPrefs
  Left = 378
  Top = 99
  BorderStyle = bsDialog
  Caption = 'User Preferences'
  ClientHeight = 588
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 267
    Top = 542
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 363
    Top = 542
    Width = 81
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 16
    Top = 8
    Width = 449
    Height = 145
    TabOrder = 2
    object Label2: TLabel
      Left = 16
      Top = 8
      Width = 253
      Height = 16
      Caption = 'SQL Server ADO Connection Strings:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 88
      Width = 112
      Height = 16
      Caption = 'Stats Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 16
      Top = 32
      Width = 158
      Height = 16
      Caption = 'Main Ticker Database:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit2: TEdit
      Left = 16
      Top = 54
      Width = 409
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Edit3: TEdit
      Left = 16
      Top = 110
      Width = 409
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel4: TPanel
    Left = 14
    Top = 269
    Width = 221
    Height = 84
    TabOrder = 3
    object RadioGroup4: TRadioGroup
      Left = 19
      Top = 6
      Width = 182
      Height = 71
      Caption = 'Schedule Monitoring'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 15
    Top = 159
    Width = 449
    Height = 106
    TabOrder = 4
    object RadioGroup1: TRadioGroup
      Left = 15
      Top = 10
      Width = 106
      Height = 84
      Caption = 'GPI Enable'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'Disable'
        'Enable')
      ParentFont = False
      TabOrder = 0
    end
    object RadioGroup2: TRadioGroup
      Left = 131
      Top = 10
      Width = 162
      Height = 84
      Caption = 'GPI COM Port'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        'COM1'
        'COM2'
        'COM3'
        'COM4')
      ParentFont = False
      TabOrder = 1
    end
    object RadioGroup3: TRadioGroup
      Left = 303
      Top = 10
      Width = 130
      Height = 84
      Caption = 'GPI Baud Rate'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemIndex = 0
      Items.Strings = (
        '9600'
        '19200')
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel7: TPanel
    Left = 14
    Top = 358
    Width = 450
    Height = 71
    TabOrder = 5
    object StaticText3: TStaticText
      Left = 15
      Top = 10
      Width = 299
      Height = 20
      Caption = 'Sponsor Logo As-Run Files Directory Path:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Edit1: TEdit
      Left = 15
      Top = 34
      Width = 418
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 240
    Top = 269
    Width = 223
    Height = 84
    TabOrder = 6
  end
  object Panel5: TPanel
    Left = 16
    Top = 436
    Width = 449
    Height = 93
    TabOrder = 7
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 152
      Height = 16
      Caption = 'Station ID Information:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 184
      Top = 32
      Width = 136
      Height = 16
      Caption = 'Station Description:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 48
      Top = 32
      Width = 72
      Height = 16
      Caption = 'Station ID:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object StationIDDesc: TEdit
      Left = 184
      Top = 56
      Width = 249
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object StationIDNum: TSpinEdit
      Left = 58
      Top = 56
      Width = 49
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 9
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
    end
  end
  object Panel6: TPanel
    Left = 472
    Top = 8
    Width = 223
    Height = 177
    TabOrder = 8
    object Label5: TLabel
      Left = 8
      Top = 8
      Width = 206
      Height = 16
      Caption = 'Enable Team Record Display'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EnableTeamRecords_Checkbox_MLB: TCheckBox
      Left = 24
      Top = 40
      Width = 73
      Height = 17
      Caption = 'MLB'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 0
    end
    object EnableTeamRecords_Checkbox_NBA: TCheckBox
      Left = 24
      Top = 72
      Width = 73
      Height = 17
      Caption = 'NBA'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 1
    end
    object EnableTeamRecords_Checkbox_NFL: TCheckBox
      Left = 24
      Top = 104
      Width = 73
      Height = 17
      Caption = 'NFL'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 2
    end
    object EnableTeamRecords_Checkbox_NHL: TCheckBox
      Left = 24
      Top = 136
      Width = 73
      Height = 17
      Caption = 'NHL'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 3
    end
    object EnableTeamRecords_Checkbox_NCAAB: TCheckBox
      Left = 120
      Top = 40
      Width = 73
      Height = 17
      Caption = 'NCAAB'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 4
    end
    object EnableTeamRecords_Checkbox_NCAAF: TCheckBox
      Left = 120
      Top = 72
      Width = 73
      Height = 17
      Caption = 'NCAAF'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 5
    end
    object EnableTeamRecords_Checkbox_NCAAW: TCheckBox
      Left = 120
      Top = 104
      Width = 73
      Height = 17
      Caption = 'NCAAW'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 6
    end
    object EnableTeamRecords_Checkbox_MLS: TCheckBox
      Left = 120
      Top = 136
      Width = 73
      Height = 17
      Caption = 'MLS'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 7
    end
  end
  object Panel8: TPanel
    Left = 472
    Top = 188
    Width = 223
    Height = 341
    TabOrder = 9
  end
end
