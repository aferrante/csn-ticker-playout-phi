// Rev: 03/29/15  M. Dilworth  Video Design Software Inc.
// Modified for driving Ross XPression
unit EngineIntf;

// Template sponsor types
// 1 = Sponsor with tagline
// 2 = Full-page sponsor
// 3 = Lower-right sponsor
// 4 = Clear lower-right sponsor

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    ACKReceivedLEDTimer: TTimer;
    EnginePort: TClientSocket;
    CommandDwellTimer: TTimer;
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure FormActivate(Sender: TObject);
    procedure ACKReceivedLEDTimerTimer(Sender: TObject);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure CommandDwellTimerTimer(Sender: TObject);
  private
    { Private declarations }
    function ProcessStyleChips(CmdStr: String): String;
  public
    { Public declarations }
    procedure StartTickerData;
    procedure SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
    function GetMixedCase(InStr: String): String;
    function FormatTimeString (InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function GetTeamMnemonic (LeagueCode: String; STTeamCode: String): String;
    function GetTeamBasename (LeagueCode: String; STTeamCode: String): String;
    function LoadAllTemplateInfo(TemplateID: SmallInt; TemplateActionType: SmallInt): TemplateDefsRec;
    procedure ReloadTempTemplateCommands(TemplateID: SmallInt; TemplateActionType: SmallInt);
    procedure ReloadTemplateFields(TemplateID: SmallInt);
    procedure UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
    function GetTeamDisplayInfo2(League: String; TeamMnemonic: String): TeamDisplayInfoRec;
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function CheckForActiveSponsorLogo: Boolean;
    procedure WriteToCommandLog (CommandString: String);
    function LastEntryIsSponsor: Boolean;
    function TemplateIsFantasy(TemplateID: SmallInt): Boolean;
    function TemplateIsLeaders(TemplateID: SmallInt): Boolean;

    //Added for XPression
    procedure SendCommand(CmdStr: String);
    procedure RunStartupCommands;
    procedure Delay(Delay: LongInt);
    procedure TickerOut;
    function GetSceneDirectorState(SceneDirectorName: String): SmallInt;
    procedure SetSceneDirectorState(SceneDirectorName: String; State: SmallInt; ResetAll: Boolean);
    function GetActionControllerState(ActionControllerID: SmallInt): SmallInt;
    procedure SetActionControllerState(ActionControllerID: SmallInt; State: SmallInt);
    function ParseCommandData(CommandIn: String): CommandRec;
    procedure AddCommandToQueue(CmdStr: String; DwellTime: SmallInt);
    function GetSceneDirectorNameForAction (ActionType, CommandType: SmallInt): String;
    function GetSceneNameFromID(SceneDirectorID: SmallInt): String;
    function GetActionControllerNameFromID(ActionID, ABState: SmallInt): String;
    procedure SendPlayCommandStrings(ActionType, CommandType, SceneDirectorID, ActionControllerID, Dwell: SmallInt; ActionIsCrawl: Boolean);
    procedure UpdateTimeOfDayClocks;
    function GetTimeDurationForQueue: SmallInt;
    procedure SetTemplateFieldLastValueCheckString(FieldID: SmallInt; LastValueCheckString: String);
    function GetTemplateFieldLastValueCheckString(FieldID: SmallInt): String;
    procedure DeleteActionControllersFromTempTemplateCommandCollection;
    procedure UpdateScoreChipStatus (CurrentGameData: GameRec);
  end;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;
  //Boolean to select whether or not the data packet is used to trigger the next graphic

implementation

uses Main, //Main form
     DataModule,
     GameDataFunctions; //Data module

{$R *.DFM}
//Imit
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to delay a specified number of 100mS increments
procedure TEngineInterface.Delay(Delay: LongInt);
var
  i: SmallInt;
begin
  if (Delay >= 100) then
  begin
    for i := 1 to (Delay DIV 100) do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;
  end;
end;

//Function to send a simple command
procedure TEngineInterface.SendCommand(CmdStr: String);
begin
  if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then
  begin
    //Send command
    try
      EnginePort.Socket.SendText(CmdStr + #13#10);
      //Log command if enabled
      if (MainForm.CommandOutList.Visible) and (LOG_OUTGOING_COMMANDS) then
      begin
        if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
        MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': CommandSent: '  + CmdStr);
      end;
      //Write to command log
      if (CommandLoggingEnabled) then WriteToCommandLog('Command Sent: ' + CmdStr);
    except
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying send startup command to gateway');
    end;
  end;
end;

//Function to run startup commands
procedure TEngineInterface.RunStartupCommands;
var
  i: SmallInt;
  SponsorLogoRecPtr: ^SponsorLogoRec;
  SponsorLogoPath: String;
begin
  //Load in startup command information
  with dmMain.UtilityQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Startup_Commands');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Send command
        SendCommand(FieldByName('Command').AsString);
        //Dwell
        Sleep (FieldByName('Dwell').AsInteger);
        //Go to next record
        Next;
      until (EOF);
    end;
  end;
end;

//Function to parse queued command to get scene ID, dwell and command
function TEngineInterface.ParseCommandData(CommandIn: String): CommandRec;
var
  Tokens: Array[1..2] of String;
  i, Cursor, TokenIndex: SmallInt;
  OutRec: CommandRec;
begin
  try
    //Parse the string for the scene number and command string
    if (Length(CommandIn) > 3) then
    begin
      //Parse for tokens
      for i := 1 to 2 do Tokens[i] := '';
      Cursor := 1;
      TokenIndex := 1;
      repeat
        if (CommandIn[Cursor] = '|') then
        begin
          Inc(Cursor);
          Inc(TokenIndex);
        end;
        Tokens[TokenIndex] := Tokens[TokenIndex] + CommandIn[Cursor];
        Inc(Cursor);
      until (Cursor > Length(CommandIn));
    end;
    //Set output record
    OutRec.Dwell := StrToIntDef(Tokens[1], 3000);
    OutRec.Command := Tokens[2];
    ParseCommandData := OutRec;
  except
    MainForm.StatusBar.SimpleText := 'ERROR OCCURRED WHILE TRYING TO PARSE COMMAND DATA FILE';
  end;
end;

//Function to get total delay for all commands in queue
function TEngineInterface.GetTimeDurationForQueue: SmallInt;
var
  i: SmallInt;
  CommandInRec: CommandRec;
  TotalDelay: SmallInt;
begin
  TotalDelay := 0;
  //Process all commands to get total delay
  if (QueuedCommandCollection.Count > 0) then
  begin
    for i := 0 to QueuedCommandCollection.Count-1 do
    begin
      //Parse the command
      CommandInRec := ParseCommandData(QueuedCommandCollection[i]);
      TotalDelay := TotalDelay + CommandInRec.Dwell;
      if (MainForm.CommandOutList.Visible) and (LOG_DELAYS) then
      begin
        if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
        MainForm.CommandOutList.Lines.Add(CommandInRec.Command + ': ' + IntToStr(CommandInRec.Dwell));
      end;
    end;
  end;
  if (MainForm.CommandOutList.Visible) and (LOG_DELAYS) then
    MainForm.CommandOutList.Lines.Add('Total Duration: ' + IntToStr(TotalDelay));
  GetTimeDurationForQueue := TotalDelay;
end;

//Handler for command queue dwell timer
procedure TEngineInterface.CommandDwellTimerTimer(Sender: TObject);
var
  CommandInRec: CommandRec;
begin
  //Disable to prevent re-trigger
  CommandDwellTimer.Enabled := FALSE;

  //Process the next command if there is one
  if (QueuedCommandCollection.Count > 0) then
  begin
    //Parse the command
    CommandInRec := ParseCommandData(QueuedCommandCollection[0]);

    //Set flag if it was the lineup so that we hold off a ticker out
    if (Pos('LINEUP', CommandInRec.Command) > 0) then
      LastCommandSentWasLineup := TRUE
    else
      LastCommandSentWasLineup := FALSE;

    //If it's just a delay, don't send the command - just delay
    if (CommandInRec.Command <> '<DELAY>') then
      SendCommand(CommandInRec.Command);

    //Delete the command from the top of stack
    QueuedCommandCollection.Delete(0);

    //Setup & enable timer
    CommandDwellTimer.Interval := CommandInRec.Dwell;
    CommandDwellTimer.Enabled := TRUE;
  end;
end;

//Function to get the current state of the scene director
function TEngineInterface.GetSceneDirectorState(SceneDirectorName: String): SmallInt;
var
  i, OutVal: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  OutVal := 0;
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorName = SceneDirectorName) then
        OutVal := SceneDirectorRecPtr^.SceneDirectorState;
    end;
  end;
  GetSceneDirectorState := OutVal;
end;

//Function to update the state of the scene director in the scene director collection
procedure TEngineInterface.SetSceneDirectorState(SceneDirectorName: String; State: SmallInt; ResetAll: Boolean);
var
  i: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorName = SceneDirectorName) then
        SceneDirectorRecPtr^.SceneDirectorState := State;
      if (ResetAll) and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'SPONSOR') and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'CLOCK') and
         (SceneDirectorRecPtr^.SceneDirectorName <> 'LINEUP') then
        SceneDirectorRecPtr^.SceneDirectorState := SCENE_DIRECTOR_OUT;
    end;
  end;
end;

//Function to get the current state of the action controller
function TEngineInterface.GetActionControllerState(ActionControllerID: SmallInt): SmallInt;
var
  i, OutVal: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
begin
  OutVal := 0;
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionControllerID) then
        OutVal := ActionControllerRecPtr^.ActionControllerState;
    end;
  end;
  GetActionControllerState := OutVal;
end;

//Procedure to get the current check string for the specified action controller
function TEngineInterface.GetTemplateFieldLastValueCheckString(FieldID: SmallInt): String;
var
  i: SmallInt;
  OutStr: String;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
begin
  OutStr := '';
  if (Temporary_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Temporary_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Temporary_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Field_ID = FieldID) then
        OutStr := TemplateFieldsRecPtr^.LastValueCheckString;
    end;
  end;
  GetTemplateFieldLastValueCheckString := OutStr;
end;

//Set the state of the action controller
procedure TEngineInterface.SetActionControllerState(ActionControllerID: SmallInt; State: SmallInt);
var
  i: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
begin
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionControllerID) then
        ActionControllerRecPtr^.ActionControllerState := State;
    end;
  end;
end;

//Procedure to set the check string for the specified action controller - used to determine if we switch A <-> B
procedure TEngineInterface.SetTemplateFieldLastValueCheckString(FieldID: SmallInt; LastValueCheckString: String);
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
begin
  if (Temporary_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Temporary_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Temporary_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Field_ID = FieldID) then
        TemplateFieldsRecPtr^.LastValueCheckString := LastValueCheckString;
    end;
  end;
end;

//Function to update the state of the action controller in the action controller collection
function TEngineInterface.GetSceneNameFromID(SceneDirectorID: SmallInt): String;
var
  i: SmallInt;
  SceneDirectorRecPtr: ^SceneDirectorRec;
  OutStr: String;
begin
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      if (SceneDirectorRecPtr^.SceneDirectorID = SceneDirectorID) then
        OutStr := SceneDirectorRecPtr^.SceneDirectorName;
    end;
  end;
  GetSceneNameFromID := OutStr;
end;

//Function to get the action controller name for the specified ID and state
function TEngineInterface.GetActionControllerNameFromID(ActionID, ABState: SmallInt): String;
var
  i: SmallInt;
  ActionControllerRecPtr: ^ActionControllerRec;
  OutStr: String;
begin
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      if (ActionControllerRecPtr^.ActionControllerID = ActionID) then
      begin
        if (ABState = ACTION_CONTROLLER_IN_A) then
          OutStr := ActionControllerRecPtr^.ActionControllerName_A
        else if (ABState = ACTION_CONTROLLER_IN_B) then
          OutStr := ActionControllerRecPtr^.ActionControllerName_B;
      end;
    end;
  end;
  GetActionControllerNameFromID := OutStr;
end;

//Procedure to delete an action controller from the temporary command collection to prevent it from running on playout
// bookmark3
procedure TEngineInterface.DeleteActionControllersFromTempTemplateCommandCollection;
var
  i,j: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  DeleteDirector: Boolean;
  RunningGameDirector: Boolean;
begin
  if (TempTemplateCommand_Collection.Count > 0) then
  begin
    i := 0;
    RunningGameDirector := FALSE;

    repeat
      DeleteDirector := TRUE;

      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) then
      begin
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            if (TempTemplateCommandRecPtr^.ActionControllerID = TemplateFieldsRecPtr^.Action_Controller_ID) and
               (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) and
               (TemplateFieldsRecPtr^.ValueHasChanged) then
              DeleteDirector := FALSE;
            //Reset flag if a game was deleted and it's the stat field
            if (RunningGameDirector) and
               ((TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_STATS_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_STATS_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_MATCHUP_IN) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_TEAM2) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_SCORE2) or
                 //(TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_SLUG) or
                 (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_R_MATCHUP_START_TIME_IN) or
                 (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_START_TIME_IN)
                 ) then
            DeleteDirector := FALSE;

            //Don't delete if flagged to always play
            if (TempTemplateCommandRecPtr^.AlwaysFireActionController) then
              DeleteDirector := FALSE;
          end;
        end;
      end
      else DeleteDirector := FALSE;

      if (DeleteDirector) then
      begin
        TempTemplateCommand_Collection.AtDelete(i);
        //Set flag if game deleted
      end
      else begin
        if (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_DATA) or
        (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_TEAM2) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_SCORE2) or
                (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_STATS_DATA) or
           (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_NCAA_GAME_DATA) or
           //(TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_GAME_SLUG) or
           (TempTemplateCommandRecPtr^.ActionControllerID = ANIMATION_CONTROLLER_ID_MATCHUP_IN) then
          RunningGameDirector := TRUE;
        inc(i);
      end;
    until (i = TempTemplateCommand_Collection.Count);
    TempTemplateCommand_Collection.Pack;
  end;
end;

//Function to get a constructed command string for playing a scene director or action controller
procedure TEngineInterface.SendPlayCommandStrings(ActionType, CommandType, SceneDirectorID, ActionControllerID, Dwell: SmallInt; ActionIsCrawl: Boolean);
var
  i, j: SmallInt;
  CommandTypeRecPtr: ^CommandTypeRec;
  ActionControllerRecPtr: ^ActionControllerRec;
  CmdStr: String;
  SceneName: String;
begin
  Case ActionType of
    //Template in initial
    // bookmark2
    ACTION_TEMPLATE_IN_INITIAL:
    begin
      Case CommandType of
        COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
          SceneName := GetSceneNameFromID(SceneDirectorID);
          CmdStr := 'B\PL\' + Scenename  + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
          //Set updated stats
          if (GetSceneDirectorState(SceneName) = SCENE_DIRECTOR_OUT) then
          begin
            SetSceneDirectorState(SceneName, SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
            //Need to set state of all "A" side action controllers associated with this scene so the next update uses the "B" side
            if (ActionController_Collection.Count > 0) then
            begin
              for i := 0 to ActionController_Collection.Count-1 do
              begin
                ActionControllerRecPtr := ActionController_Collection.At(i);
                //Set the current state of all non-crawl scene controllers to the A-state
                if (ActionControllerRecPtr^.SceneID = SceneDirectorID) and not (ActionControllerRecPtr^.ActionIsCrawl) then
                begin
                  SetActionControllerState(ActionControllerRecPtr^.ActionControllerID, ACTION_CONTROLLER_IN_A);
                end;
              end;
            end;
          end;
        end;
        COMMAND_TYPE_FIRE_ACTION_CONTROLLER: begin
          begin
            if (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_OUT) or
               (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_IN_B) then
            begin
              CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_A) + '�1\\';
              AddCommandToQueue(CmdStr, Dwell);
              SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_IN_A);
            end
            else if (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_IN_A) then
            begin
              CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_B) + '�1\\';
              AddCommandToQueue(CmdStr, Dwell);
              SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_IN_B);
            end;
          end;
        end;
      end;
    end;

    //Template update
    ACTION_TEMPLATE_UPDATE:
    begin
      Case CommandType of
        COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
          SceneName := GetSceneNameFromID(SceneDirectorID);
          CmdStr := 'B\PL\' + Scenename  + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
          //Set updated stats
          if (GetSceneDirectorState(SceneName) = SCENE_DIRECTOR_OUT) then
            SetSceneDirectorState(SceneName, SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
          else if (GetSceneDirectorState(SceneName) = SCENE_DIRECTOR_IN_B) then
            SetSceneDirectorState(SceneName, SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
          else if (GetSceneDirectorState(SceneName) = SCENE_DIRECTOR_IN_A) then
            SetSceneDirectorState(SceneName, SCENE_DIRECTOR_IN_B, DONT_RESET_ALL);
        end;
        COMMAND_TYPE_FIRE_ACTION_CONTROLLER: begin
          begin
            if (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_OUT) or
               (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_IN_B) then
            begin
              CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_A) + '�1\\';
              AddCommandToQueue(CmdStr, Dwell);
              SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_IN_A);
            end
            else if (GetActionControllerState(ActionControllerID) = ACTION_CONTROLLER_IN_A) then
            begin
              CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_B) + '�1\\';
              AddCommandToQueue(CmdStr, Dwell);
              SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_IN_B);
            end;
          end;
        end;
      end;
    end;

    //Template out
    ACTION_TEMPLATE_OUT: begin
      Case CommandType of
        COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
          //Take out any active winner chips first
          for j := 1 to 6 do
          begin
            if (ScoreChipStatus[j].CurrentChipStatus = TRUE) then
            begin
              AddCommandToQueue ('B\PL\' + ScoreChipStatus[j].ChipDirectorName + '�1\\', 50);
              //Set updated status
              ScoreChipStatus[j].CurrentChipStatus := FALSE;
              ScoreChipStatus[j].NewChipStatus := FALSE;
            end;
          end;

          CmdStr := 'B\PL\' +  GetSceneNameFromID(SceneDirectorID) + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
          SetSceneDirectorState(GetSceneNameFromID(SceneDirectorID), SCENE_DIRECTOR_OUT, DONT_RESET_ALL);
          //Need to set state of all "A" side action controllers associated with this scene so the next update uses the "B" side
          if (ActionController_Collection.Count > 0) then
          begin
            for i := 0 to ActionController_Collection.Count-1 do
            begin
              ActionControllerRecPtr := ActionController_Collection.At(i);
              if (ActionControllerRecPtr^.SceneID = SceneDirectorID) then
              begin
                SetActionControllerState(ActionControllerRecPtr^.ActionControllerID, ACTION_CONTROLLER_OUT);
              end;
            end;
          end;
        end;
      end;
    end;

    //Template reset
    ACTION_TEMPLATE_RESET: begin
      Case CommandType of
        COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
          CmdStr := 'B\PL\' +  GetSceneNameFromID(SceneDirectorID) + '�1\\';
          AddCommandToQueue(CmdStr, Dwell);
        end;
        COMMAND_TYPE_FIRE_ACTION_CONTROLLER: begin
          if (ActionIsCrawl) then
            CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_A) + '�0\\'
          else
            CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_A) + '�2\\';
          AddCommandToQueue(CmdStr, Dwell);
          CmdStr := 'B\PL\' +  GetActionControllerNameFromID(ActionControllerID, ACTION_CONTROLLER_IN_B) + '�0\\';
          AddCommandToQueue(CmdStr, Dwell);

          SetActionControllerState(ActionControllerID, ACTION_CONTROLLER_OUT);
        end;
      end;
    end;
  end;
end;

//Update the time of day clocks
procedure TEngineInterface.UpdateTimeOfDayClocks;
var
  CmdStr: String;
begin
  CmdStr := 'B\UP\tod_clock_txt�' +  FormatDateTime('h:nn am/pm', Now) + ' ' + TimeZoneSuffix + '\\';
  AddCommandToQueue(CmdStr, 50);
  CmdStr := 'B\UP\breaking_clock_txt�' +  FormatDateTime('h:nn am/pm', Now) + ' ' + TimeZoneSuffix + '\\';
  AddCommandToQueue(CmdStr, 50);
end;

//Procedure to update the global game status array
procedure TEngineInterface.UpdateScoreChipStatus (CurrentGameData: GameRec);
var
  i: SmallInt;
begin
  for i := 1 to 6 do
  begin
    //Init
    ScoreChipStatus[i].NewChipStatus := FALSE;
    //Set new status
    if (CurrentGameData.State = FINAL) then
    begin
      if (CurrentGameData.VScore > CurrentGameData.HScore) then
      begin
        if (CurrentGameData.VScore < 10) then
          ScoreChipStatus[1].NewChipStatus := TRUE
        else if (CurrentGameData.VScore >= 10) and (CurrentGameData.VScore < 100) then
          ScoreChipStatus[2].NewChipStatus := TRUE
        else
          ScoreChipStatus[3].NewChipStatus := TRUE;
      end
      else if (CurrentGameData.HScore > CurrentGameData.VScore) then
      begin
        if (CurrentGameData.HScore < 10) then
          ScoreChipStatus[4].NewChipStatus := TRUE
        else if (CurrentGameData.HScore >= 10) and (CurrentGameData.HScore < 100) then
          ScoreChipStatus[5].NewChipStatus := TRUE
        else
          ScoreChipStatus[6].NewChipStatus := TRUE;
      end;
    end;
  end;  
end;

//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll]);
      //Type 2 = Use substitution font & character
      //else if (StyleChipRecPtr^.StyleChip_Type = 2) then
      //  OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
      //            '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
      //            Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

//Function to take the team mnmonic and return the team name
function TEngineInterface.GetTeamDisplayInfo2(LEague: String; TeamMnemonic: String): TeamDisplayInfoRec;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to team record type
  OutRec: TeamDisplayInfoRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(TeamMnemonic)) AND
//         (Trim(TeamRecPtr^.League) = Trim(League))then
      begin
//        OutRec.DisplayMnemonic := TeamRecPtr^.DisplayName1;
//        OutRec.Displayname := TeamRecPtr^.DisplayName2;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Team_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetTeamDisplayInfo2 := OutRec;
end;

//Function to take the league code and Sportsticker team code, and return the team menmonic
function TEngineInterface.GetTeamMnemonic(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = LeagueCode) then
//      begin
//        OutStr := Trim(TeamRecPtr^.TeamMnemonic);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    Until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamMnemonic := OutStr;
end;

//Function to take the league code and Sportsticker team code, and return the team base name
function TEngineInterface.GetTeamBaseName(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = Trim(LeagueCode)) then
//      begin
//        OutStr := Trim(TeamRecPtr^.BaseName);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamBaseName := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to take 4-digit clock time string from Datamart and format as "mm:ss"
function TEngineInterface.FormatTimeString (InStr: String): String;
var
  OutStr: String;
begin
  OutStr := '';
  InStr := Trim(InStr);
  Case Length(InStr) of
    0: Begin
         OutStr := '';
       end;
    1: Begin
         OutStr := ':0' + InStr[1];
       end;
    2: Begin
         OutStr := ':' + InStr[1] + InStr[2];
       end;
    3: Begin
         OutStr := InStr[1] + ':' + InStr[2] + InStr[3];
       end;
    4: Begin
         OutStr := InStr[1] + InStr[2] + ':' + InStr[3] + InStr[4];
       end;
  end;
  FormatTimeString := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadAllTemplateInfo(TemplateID: SmallInt; TemplateActionType: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
  TemplateCommandRecPtr, NewTemplateCommandRecPtr: ^TemplateCommandRec;
begin
  //Clear the existing collections
  if (TemplateActionType = ACTION_TEMPLATE_IN_INITIAL) then
  begin
    Temporary_Fields_Collection.Clear;
    Temporary_Fields_Collection.Pack;
  end;
  TempTemplateCommand_Collection.Clear;
  TempTemplateCommand_Collection.Pack;

  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.SceneDirectorID := TemplateRecPtr^.SceneDirectorID;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;

  //Now, get the fields for the template
  if (TemplateActionType = ACTION_TEMPLATE_IN_INITIAL) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
      begin
        GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
        With NewTemplateFieldsRecPtr^ do
        begin
          Template_ID := TemplateFieldsRecPtr^.Template_ID;
          Field_ID := TemplateFieldsRecPtr^.Field_ID;
          Field_Type := TemplateFieldsRecPtr^.Field_Type;
          Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
          Field_Label := TemplateFieldsRecPtr^.Field_Label;
          Action_Controller_ID := TemplateFieldsRecPtr^.Action_Controller_ID;
          Scene_Field_Name_A := TemplateFieldsRecPtr^.Scene_Field_Name_A;
          Scene_Field_Name_B := TemplateFieldsRecPtr^.Scene_Field_Name_B;
          Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
          Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
          Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
          Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
          LastValueCheckString := '';
          ValueHasChanged := FALSE;
        end;
        if (Temporary_Fields_Collection.Count <= 100) then
        begin
          //Add to collection
          Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
          Temporary_Fields_Collection.Pack;
        end;
      end;
    end;
  end;

  //Load the temporary template commands collection
  for i := 0 to TemplateCommand_Collection.Count-1 do
  begin
    TemplateCommandRecPtr := TemplateCommand_Collection.At(i);
    if (TemplateCommandRecPtr^.TemplateID = TemplateID) and (TemplateCommandRecPtr^.ActionType = TemplateActionType) then
    begin
      GetMem (NewTemplateCommandRecPtr, SizeOf(TemplateCommandRec));
      With NewTemplateCommandRecPtr^ do
      begin
        TemplateID := TemplateCommandRecPtr^.TemplateID;
        ActionID := TemplateCommandRecPtr^.ActionID;
        CommandID := TemplateCommandRecPtr^.CommandID;
        ActionType := TemplateCommandRecPtr^.ActionType;
        ActionDescription := TemplateCommandRecPtr^.ActionDescription;
        CommandDescription := TemplateCommandRecPtr^.CommandDescription;
        CommandType := TemplateCommandRecPtr^.CommandType;
        SceneDirectorID := TemplateCommandRecPtr^.SceneDirectorID;
        SceneDirectorName := TemplateCommandRecPtr^.SceneDirectorName;
        ActionControllerID := TemplateCommandRecPtr^.ActionControllerID;
        AlwaysFireActionController := TemplateCommandRecPtr^.AlwaysFireActionController;
        ActionController_A := TemplateCommandRecPtr^.ActionController_A;
        ActionController_B := TemplateCommandRecPtr^.ActionController_B;
        ActionIsCrawl := TemplateCommandRecPtr^.ActionIsCrawl;
        Delay := TemplateCommandRecPtr^.Delay;
      end;
      if (TempTemplateCommand_Collection.Count <= 100) then
      begin
        //Add to collection
        TempTemplateCommand_Collection.Insert(NewTemplateCommandRecPtr);
        TempTemplateCommand_Collection.Pack;
      end;
    end;
  end;

  LoadAllTemplateInfo := OutRec;
end;

//Procedure to just reload the template fields collection; leave flags and check state intact
procedure TEngineInterface.ReloadTemplateFields(TemplateID: SmallInt);
var
  i,j: SmallInt;
  TemplateFieldsRecPtr, TempTemplateFieldsRecPtr: ^TemplateFieldsRec;
begin
  if (Template_Fields_Collection.Count > 0) and (Temporary_Fields_Collection.Count > 0) then
  begin
    //Now, get the fields for the template
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
      begin
        //Iterate through temporarary fields collection and update all matches
        for j := 0 to Temporary_Fields_Collection.Count-1 do
        begin
          TempTemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
          if (TemplateFieldsRecPtr^.Template_ID = TemplateID) and
             (TemplateFieldsRecPtr^.Field_ID = TempTemplateFieldsRecPtr^.Field_ID) then
          begin
            With TempTemplateFieldsRecPtr^ do
            begin
              Template_ID := TemplateFieldsRecPtr^.Template_ID;
              Field_ID := TemplateFieldsRecPtr^.Field_ID;
              Field_Type := TemplateFieldsRecPtr^.Field_Type;
              Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
              Field_Label := TemplateFieldsRecPtr^.Field_Label;
              Action_Controller_ID := TemplateFieldsRecPtr^.Action_Controller_ID;
              Scene_Field_Name_A := TemplateFieldsRecPtr^.Scene_Field_Name_A;
              Scene_Field_Name_B := TemplateFieldsRecPtr^.Scene_Field_Name_B;
              Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
              Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
              Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
              Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
            end;
          end;
        end;
      end;
    end;
  end;
end;

//Procedure to reload the temporary template commands collection only
procedure TEngineInterface.ReLoadTempTemplateCommands(TemplateID: SmallInt; TemplateActionType: SmallInt);
var
  i: SmallInt;
  FoundMatch: Boolean;
  TemplateCommandRecPtr, NewTemplateCommandRecPtr: ^TemplateCommandRec;
begin
  //Clear the existing collection
  TempTemplateCommand_Collection.Clear;
  TempTemplateCommand_Collection.Pack;

  //Load the temporary template commands collection
  for i := 0 to TemplateCommand_Collection.Count-1 do
  begin
    TemplateCommandRecPtr := TemplateCommand_Collection.At(i);
    if (TemplateCommandRecPtr^.TemplateID = TemplateID) and (TemplateCommandRecPtr^.ActionType = TemplateActionType) then
    begin
      GetMem (NewTemplateCommandRecPtr, SizeOf(TemplateCommandRec));
      With NewTemplateCommandRecPtr^ do
      begin
        TemplateID := TemplateCommandRecPtr^.TemplateID;
        ActionID := TemplateCommandRecPtr^.ActionID;
        CommandID := TemplateCommandRecPtr^.CommandID;
        ActionType := TemplateCommandRecPtr^.ActionType;
        ActionDescription := TemplateCommandRecPtr^.ActionDescription;
        CommandDescription := TemplateCommandRecPtr^.CommandDescription;
        CommandType := TemplateCommandRecPtr^.CommandType;
        SceneDirectorID := TemplateCommandRecPtr^.SceneDirectorID;
        SceneDirectorName := TemplateCommandRecPtr^.SceneDirectorName;
        ActionControllerID := TemplateCommandRecPtr^.ActionControllerID;
        ActionController_A := TemplateCommandRecPtr^.ActionController_A;
        ActionController_B := TemplateCommandRecPtr^.ActionController_B;
        ActionIsCrawl := TemplateCommandRecPtr^.ActionIsCrawl;
        Delay := TemplateCommandRecPtr^.Delay;
      end;
      if (TempTemplateCommand_Collection.Count <= 100) then
      begin
        //Add to collection
        TempTemplateCommand_Collection.Insert(NewTemplateCommandRecPtr);
        TempTemplateCommand_Collection.Pack;
      end;
    end;
  end;
end;

//Utility function to strip off first character in string
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end
  else OutStr := InStr;
  StripPrefix := OutStr;
end;

//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR
       (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    //Do subsitutions for Fantasy and Leaders
    //if (TickerRecPtrCurrent^.Template_ID >= FantasyStartTemplate) and
       //(TickerRecPtrCurrent^.Template_ID <= FantasyEndTemplate) then
      //CurrentEntryLeague := 'FANTASY'
    //else if (TickerRecPtrCurrent^.Template_ID >= LeadersStartTemplate) and
       //(TickerRecPtrCurrent^.Template_ID <= LeadersEndTemplate) then
      //CurrentEntryLeague := 'LEADERS';
      // bookmark4
    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'IL') OR (NextEntryLeague = 'ML') then
        NextEntryLeague := 'MLB';

      //Do subsitutions for Fantasy and Leaders
      //if (TickerRecPtrNext^.Template_ID >= FantasyStartTemplate) and
         //(TickerRecPtrNext^.Template_ID <= FantasyEndTemplate) then
        //NextEntryLeague := 'FANTASY'
      //else if (TickerRecPtrNext^.Template_ID >= LeadersStartTemplate) and
         //(TickerRecPtrNext^.Template_ID <= LeadersEndTemplate) then
        //NextEntryLeague := 'LEADERS';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 6) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
      OutRec.TextFields[5] :=  OutRec.TextFields[2];
      OutRec.TextFields[6] :=  OutRec.TextFields[3];
    end
    else if (HeaderIndex = 4) then
    begin
      OutRec.TextFields[5] :=  OutRec.TextFields[1];
      OutRec.TextFields[6] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 5) then
    begin
      OutRec.TextFields[6] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 6 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 6 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger current graphic
procedure TEngineInterface.TickerOut;
var
  i: SmallInt;
  CmdStr: String;
  TemplateCommandRecPtr: ^TemplateCommandRec;
  ActionControllerRecPtr: ^ActionControllerRec;
  SceneDirectorRecPtr: ^SceneDirectorRec;
begin
  //Clear out the queue so the commands take effect immediately
  EngineInterface.CommandDwellTimer.Enabled := FALSE;
  QueuedCommandCollection.Clear;

  if (RunAnimationOnTickerOut) then
  begin
    AddCommandToQueue('<DELAY>', 1000);
    //Send command to fade content out
    CmdStr := 'B\PL\TICKER�1\\';
    //Add command to queue
    AddCommandToQueue(CmdStr, 3000);
  end
  else begin
    AddCommandToQueue('<DELAY>', 1000);
    //Send command to fade content out
    CmdStr := 'B\PL\ALL_OUT�1\\';
    //Add command to queue
    AddCommandToQueue(CmdStr, 100);
  end;
  {
  if (LastCommandSentWasLineup) then
  begin
    CmdStr := '<DELAY>';
    AddCommandToQueue(CmdStr, 1000);
  end;

  //Get & fire commands to clear template fields
  LoadTempTemplateFields(LastTemplateID, ACTION_TEMPLATE_OUT);

  //Fire actions to take director out
  if (TempTemplateCommand_Collection.Count > 0) then
  begin
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      SendPlayCommandStrings(TemplateCommandRecPtr^.ActionType, TemplateCommandRecPtr^.CommandType,
        TemplateCommandRecPtr^.SceneDirectorID, TemplateCommandRecPtr^.ActionControllerID, TemplateCommandRecPtr^.Delay, TemplateCommandRecPtr^.ActionIsCrawl);
    end;
  end;
  CommandDwellTimer.Enabled := TRUE;

  //Get & fire commands to clear template fields
  LoadTempTemplateFields(LastTemplateID, ACTION_TEMPLATE_RESET);

  //Fire actions to clear fields
  if (TempTemplateCommand_Collection.Count > 0) then
  begin
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      SendPlayCommandStrings(TemplateCommandRecPtr^.ActionType, TemplateCommandRecPtr^.CommandType,
        TemplateCommandRecPtr^.SceneDirectorID, TemplateCommandRecPtr^.ActionControllerID, TemplateCommandRecPtr^.Delay, TemplateCommandRecPtr^.ActionIsCrawl);
    end;
  end;

  //Take the sponsor out if it's in
  if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_IN_A) then
  begin
    CmdStr := 'B\PL\SPONSOR�1\\';
    AddCommandToQueue(CmdStr, 100);
    SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_OUT, DONT_RESET_ALL);
  end;

  //Take the clock out if it's in
  if (GetSceneDirectorState('CLOCK') = SCENE_DIRECTOR_IN_A) then
  begin
    CmdStr := 'B\PL\CLOCK�1\\';
    AddCommandToQueue(CmdStr, 1000);
    SetSceneDirectorState('CLOCK', SCENE_DIRECTOR_OUT, DONT_RESET_ALL);
  end;

  //Send command to fade content out
  CmdStr := 'B\PL\CONTENT_OFF�1\\';
  //Add command to queue
  AddCommandToQueue(CmdStr, 100);

  //Build & send command strings
  //Take out line-up
  CmdStr := 'B\PL\LINEUP�1\\';
  //Add command to queue
  AddCommandToQueue(CmdStr, 1000);

  //Take out ticker
  CmdStr := 'B\PL\TICKER�1\\';
  //Add command to queue
  AddCommandToQueue(CmdStr, 2000);
  }

  //Send command to reload scene
  CmdStr := 'B\OP\' + SceneName + '\\';
  //Add command to queue
  AddCommandToQueue(CmdStr, 50);

  //Set last template and scene director IDs - used to reset on next template
  LastTemplateID := -1;
  LastSceneDirectorID := -1;
  //Reset line-up so it always comes in when the ticker is triggered
  for i := 1 to 6 do LastLineupData.TextFields[i] := '';

  //Clear state of all scene directors
  if (SceneDirector_Collection.Count > 0) then
  begin
    for i := 0 to SceneDirector_Collection.Count-1 do
    begin
      SceneDirectorRecPtr := SceneDirector_Collection.At(i);
      SceneDirectorRecPtr^.SceneDirectorState := SCENE_DIRECTOR_OUT;
    end;
  end;

  //Clear state and check string for all action controllers
  if (ActionController_Collection.Count > 0) then
  begin
    for i := 0 to ActionController_Collection.Count-1 do
    begin
      ActionControllerRecPtr := ActionController_Collection.At(i);
      ActionControllerRecPtr^.ActionControllerState := ACTION_CONTROLLER_OUT;
      //ActionControllerRecPtr^.LastValueCheckString := '';
    end;
  end;

  //Reset the score chips
  for i := 1 to 6 do ScoreChipStatus[i].CurrentChipStatus := FALSE;

  //Clear flag
  InitialDataTemplateFired := FALSE;

  //Clear crawl flag and watchdog timer
  CrawlIsEnabled := FALSE;

  //Disable packet timeout timer
  TickerPacketTimeoutTimer.Enabled := FALSE;
end;

//Function to determine if the template is a fantasy template
function TEngineInterface.TemplateIsFantasy(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  OutVal, FoundMatch: Boolean;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  OutVal := FALSE;
  FoundMatch := FALSE;
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    Repeat
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        OutVal := TemplateDefsRecPtr^.IsFantasyTemplate;
      end;
      inc(i);
    Until (FoundMatch) or (i = Template_Defs_Collection.Count);
  end;
  TemplateIsFantasy := OutVal;
end;

//Function to determine if the template is a leaders template
function TEngineInterface.TemplateIsLeaders(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  OutVal, FoundMatch: Boolean;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  OutVal := FALSE;
  FoundMatch := FALSE;
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    Repeat
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        OutVal := TemplateDefsRecPtr^.IsLeadersTemplate;
      end;
      inc(i);
    Until (FoundMatch) or (i = Template_Defs_Collection.Count);
  end;
  TemplateIsLeaders := OutVal;
end;

//Procedure to update data for a specific record from the ticker playlist; called for each record
procedure TEngineInterface.UpdateTickerRecordDataFromDB(PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  SQLString: String;
begin
  try
    dmMain.Query3.Close;
    dmMain.Query3.SQL.Clear;
    SQLString := 'SELECT * FROM Ticker_Elements WHERE Playlist_ID = '+ FloatToStr(PlaylistID) +
      ' AND Event_GUID = ' + QuotedStr(GUIDToString(EventGUID));
    dmMain.Query3.SQL.Add (SQLString);
    dmMain.Query3.Open;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      //Don't update if fantasy or leaders template - will overwrite stat index
      if (TemplateIsFantasy(dmMain.Query3.FieldByName('Template_ID').AsInteger)) or
         (TemplateIsLeaders(dmMain.Query3.FieldByName('Template_ID').AsInteger)) then
      begin
        Exit;
      end
      else begin
        TickerRecPtr := Ticker_Collection.At(TickerRecordIndex);
        With TickerRecPtr^ do
        begin
          Enabled := dmMain.Query3.FieldByName('Enabled').AsBoolean;
          League := dmMain.Query3.FieldByName('League').AsString;
          //Load the user-defined text fields
          for i := 1 to 25 do
            UserData[i] := dmMain.Query3.FieldByName('UserData_' + IntToStr(i)).AsString;
          StartEnableDateTime := dmMain.Query3.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query3.FieldByName('EndEnableTime').AsDateTime;
          DwellTime := dmMain.Query3.FieldByName('DwellTime').AsInteger;
        end;
      end;
    end;
    dmMain.Query3.Close;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to update data for ticker playlist entry #' +
        IntToStr(TickerRecordIndex));
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if (TickerRecPtr^.Template_ID = SPONSOR_TEMPLATE_ID) AND
         (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) AND
         (TickerRecPtr^.Enabled = TRUE) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

function TEngineInterface.LastEntryIsSponsor: Boolean;
var
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check if last entry is sponsor
  TickerRecPtr := Ticker_Collection.At(Ticker_Collection.Count-1);
  if (TickerRecPtr^.Template_ID = SPONSOR_TEMPLATE_ID) then
    OutVal := TRUE;
  //Return
  LastEntryIsSponsor := OutVal;
end;

//Procedure to add a command to the queue
procedure TEngineInterface.AddCommandToQueue(CmdStr: String; DwellTime: SmallInt);
var
  StartTimer: Boolean;
begin
  //Dwell to front of command - will be parsed out on playout; prevent 0 mS timer interval
  if (DwellTime = 0) then DwellTime := 10;
  CmdStr := IntToStr(DwellTime) + '|' + CmdStr;
  //Add command to the queue
  QueuedCommandCollection.Add(CmdStr);

  //Log command if enabled
  if (MainForm.CommandOutList.Visible) and (LOG_QUEUE_COMMANDS) then
  begin
    if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
      MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Command Queued: '  + CmdStr);
  end;

  //Send or queue command
  if (QueuedCommandCollection.Count = 1) then StartTimer := TRUE
  else StartTimer := FALSE;

  //If first command in list, fire the command timer
  if (StartTimer) then
  begin
    CommandDwellTimer.Interval := 1;
    CommandDwellTimer.Enabled := TRUE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to start ticker, bugs and applicable extra line
//Sets all initial data and sends first data record
procedure TEngineInterface.StartTickerData;
var
  i,j,k,m: SmallInt;
  TempStr, CmdStr, UpdateCmdStr: String;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  FoundGame, FoundStat: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  SponsorLogoToUse: String;
  NextStatStr: String;
  FieldStr: String;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
  UseFieldDelayTimer: Boolean;
  TokenCounter: SmallInt;
  SymbolValueData: SymbolValueRec;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  RecordIsSponsor := FALSE;
  UseFieldDelayTimer := FALSE;
  TickerCommandDelayTimer.Enabled := FALSE;
  TickerPacketTimeoutTimer.Enabled := FALSE;
  MasterSegmentIndex := TICKER;

  USEDATAPACKET := FALSE;

  //Set looping mode as required
  Case TickerDisplayMode of
    //1x through for 1-lime, 2-line
    1,3: LoopTicker := FALSE;
    //Continuous loop for 1-lime, 2-line
    2,4: LoopTicker := TRUE;
  end;

  try
    //////////////////////////////////////////////////////////////////////////
    // START TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in Ticker
    if (Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentTickerEntryIndex < 0) then CurrentTickerEntryIndex := 0;
      if (CurrentBreakingNewsEntryIndex < 0) then CurrentBreakingNewsEntryIndex := 0;

      //If breaking news is enabled, start with it, regardless of the immediate mode
      if (BreakingNewsSettings.BreakingNewsEnable = TRUE) AND (BreakingNews_Collection.Count > 0) then
      begin
        MasterSegmentIndex := 2;
        //Set number of iterations completed to 1 -> Required to prevent extra iteration when starting
        //with breaking news
        BreakingNewsIterationsCompleted := 1;
      end;

      //Init FoundGame flag; used to prevent sending of commands if game not found
      OKToGo := TRUE;
      FoundGame := TRUE;
      //Set enable
      PacketEnable := TRUE;

      //Send the first record for the Ticker; check to make sure it's a valid index
      if (CurrentTickerEntryIndex <= Ticker_Collection.Count-1) then
      begin
        //Init
        j := 0;

        //Increment based on current master segment (Ticker = 1; Breaking News = 2)
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
        UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
        While ((TickerRecPtr^.Enabled = FALSE) OR NOT((TickerRecPtr^.StartEnableDateTime <= Now) AND
              (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < Ticker_Collection.Count) do
        begin
          Inc (j);
          Inc(CurrentTickerEntryIndex);
          if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
            CurrentTickerEntryIndex := 0;
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
        end;
        //Set flag if no enabled records found
        if (j >= Ticker_Collection.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR TICKER FIELDS
          //Get template info and load the termporary fields collection
          try
            Case MasterSegmentIndex of
              TICKER: TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);
              BREAKINGNEWS: begin
                  TemplateInfo := LoadAllTemplateInfo(BreakingNewsRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);
              end;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          //If in ticker mode, get game data & process to check game states
          Case MasterSegmentIndex of
            TICKER: begin
                //Get game data if template requires it
                if (TemplateInfo.UsesGameData) then
                begin
                  try
                    //Get game data if template requires it
                    if (TemplateInfo.UsesGameData) then
                    begin
                      //Use full game data
                      CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                      LastGameIDDisplayed := TickerRecPtr^.GameID;
                    end;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get game data for ticker');
                  end;
                  FoundGame := CurrentGameData.DataFound;
                end;

                //Check the game state vs. the state required by the template if not a match, increment
                //Also do check for enabled entries and check time window
                if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.StateForInProgressCheck <> TemplateInfo.RequiredGameState) AND
                  //Special case for postponed, delayed or suspended MLB games
                  (not(((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                        (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                        (TemplateInfo.RequiredGameState = INPROGRESS))) then
                Repeat
                  Inc(CurrentTickerEntryIndex);
                  if (CurrentTickerEntryIndex = Ticker_Collection.Count) then
                  begin
                    MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                    CurrentTickerEntryIndex := 0;
                  end;
                  TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                  TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);
                  if (TemplateInfo.UsesGameData) then
                  begin
                    try
                      //Use full game data
                      CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                      LastGameIDDisplayed := TickerRecPtr^.GameID;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to get game data for ticker');
                    end;
                    FoundGame := CurrentGameData.DataFound;
                  end
                  else CurrentGameData.State := -1;
                  //Update the data from the database - called for each record
                  UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                  //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
                  Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.StateForInProgressCheck = TemplateInfo.RequiredGameState) OR
                        //Special case for postponed, delayed or suspended MLB games
                        (((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                          (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                          (TemplateInfo.RequiredGameState = INPROGRESS))) AND
                        ((TickerRecPtr^.Enabled = TRUE) AND
                        (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));
                //Set flag
                //USEDATAPACKET := TRUE;

                //Set the default dwell time; set packet timeout interval
                //If lower-right sponsor template, sponsor logo dwell does not need to be specified
                if (TemplateInfo.TemplateSponsorType = 1) then
                begin
                  //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
                  CurrentSponsorInfo.CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
                  CurrentSponsorInfo.CurrentSponsorLogoDwell := TickerRecPtr^.SponsorLogo_Dwell;
                  CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
                end
                //If clear lower-right sponsor template, clear the name
                else if (TemplateInfo.TemplateSponsorType = 3) then
                begin
                  //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
                  CurrentSponsorInfo.CurrentSponsorLogoName := '';
                  CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
                  CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
                end
                //Not a sponsor, and not a crawl
                else if (TickerRecPtr^.DwellTime > 2000) then
                begin
                  //CurrentSponsorInfo.CurrentSponsorLogoName := '';
                  TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
                  TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
                end
                //Setup for crawl
                else begin
                  //CurrentSponsorInfo.CurrentSponsorLogoName := '';
                  TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
                  if (TickerRecPtr^.DwellTime = 1) then
                    TickerPacketTimeoutTimer.Interval := 90000
                  else
                    TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
                end;
              end;
          end;

          //Check to see if active sponsor logo needs to be cleared for main sponsor templates
          if (CheckForActiveSponsorLogo = FALSE) then CurrentSponsorInfo.CurrentSponsorLogoName := '';

          //Start of command processing
          //Send command to fade content out
          CmdStr := 'B\PL\CONTENT_ON�1\\';
          //Add command to queue
          AddCommandToQueue(CmdStr, 50);

          //Trigger ticker in
          CmdStr := 'B\PL\TICKER�1\\';
          //Add command to queue
          AddCommandToQueue(CmdStr, TickerInAnimationDelay);

          ////////////////////////////////////////////////////////////////////////
          //Fire alerts and line-ups as needed
          ////////////////////////////////////////////////////////////////////////
          //First, check if alert type coming in for first time
          if ((TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) or
              (TemplateInfo.Template_Type = TEMPLATE_TYPE_PROGRAM_ALERT)) then
          begin
            //Trigger sponsor out if breaking news
            if (TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) and
               (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_IN_A) then
            begin
              CmdStr := 'B\PL\SPONSOR�1\\';
              AddCommandToQueue(CmdStr, 50);
              //Update the state
              SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_OUT, DONT_RESET_ALL)
            end;

            //Reload template data for new alerts template
            TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);

            //Reset so line-up runs with next regular template
            LastLineupData.TextFields[1] := '';
          end
          //If lower-right sponsor template, bring sponsor in/out then skip to next record - will bring data in then
          else if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_IN) and (CurrentSponsorInfo.CurrentSponsorLogoName <> '') then
          begin
            //Just clear line-up and jump to next record - sponsor will playout with next template
            LastLineupData.TextFields[1] := '';

            //Exit and send next record
            JumpToNextTickerRecordTimer.Enabled := TRUE;
            Exit;
          end
          else if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_OUT) then
          begin
            LastLineupData.TextFields[1] := '';

            //Exit and send next record
            JumpToNextTickerRecordTimer.Enabled := TRUE;
            Exit;
          end
          ////////////////////////////////////////////////////////////////////////
          // Main block of code for bringing in next template
          ////////////////////////////////////////////////////////////////////////
          //Not breaking news or program alert       bookmark6
          else if (TemplateInfo.Template_Type <> 55)  and (TemplateInfo.Template_Type <> 66) then
          begin
            ////////////////////////////////////////////////////////////////////////
            //Fire line-up text if it's changed
            ////////////////////////////////////////////////////////////////////////
            LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
            //if (LineupData.TextFields[1] <> LastLineupData.TextFields[1]) then
            begin
              //Send new line-up text
              CmdStr := 'B\UP\prime_header_txt�' + LineupData.TextFields[1] +
                '\header_2_txt�' + LineupData.TextFields[2] +
                '\header_3_txt�' + LineupData.TextFields[3] +
                '\header_4_txt�' + LineupData.TextFields[4] +
                '\header_5_txt�' + LineupData.TextFields[5] +
                '\header_6_txt�' + LineupData.TextFields[6] +
                '\\';
              //Add command to queue
              AddCommandToQueue(CmdStr, 50);

              //Trigger line-up back in
              if (GetSceneDirectorState('LINEUP') = SCENE_DIRECTOR_OUT) then
              begin
                CmdStr := 'B\PL\LINEUP�1\\';
                //Add command to queue
                AddCommandToQueue(CmdStr, LineupInDelay);
                SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
              end;

              //Delay
              AddCommandToQueue('<DELAY>', 1000);

              //Get & fire commands to bring in template fields
              LoadAllTemplateInfo(TemplateInfo.Template_ID, ACTION_TEMPLATE_IN_INITIAL);

              //Set last template ID
              LastTemplateID := -1;
              LastSceneDirectorID := -1;

              //Save state of last line-up
              LastLineupData := LineupData;
            end;

            //This block checks to see if a sponsor needs to be brought back in after the ticker was taken out
            if ((GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) and
                    (CurrentSponsorInfo.CurrentSponsorLogoName <> '') and (TemplateInfo.Template_Type <> TEMPLATE_TYPE_BREAKING_NEWS)) then
            begin
              //Set sponsor logo
              CmdStr := 'B\MT\SPONSOR_IMG�' + CurrentSponsorInfo.CurrentSponsorLogoName + '\\';
              AddCommandToQueue(CmdStr, 50);

              //Delay
              AddCommandToQueue('<DELAY>', 1000);

              //Bring sponsor in if it was out
              if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) then
              begin
                CmdStr := 'B\PL\SPONSOR�1\\';
                AddCommandToQueue(CmdStr, 50);
                //Update the state
                SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
              end;
              //Don't dump out - just go onto rest of template processing
            end;
          end;

          //Bring in the time of day clock if no sponsor
          if (CurrentSponsorInfo.CurrentSponsorLogoName = '') and (GetSceneDirectorState('CLOCK') = SCENE_DIRECTOR_OUT) then
          begin
            UpdateTimeOfDayClocks;
            SetSceneDirectorState('CLOCK', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
            CmdStr := 'B\PL\CLOCK�1\\';
            AddCommandToQueue(CmdStr, 200);
          end;

          //Save state of last line-up
          LastLineupData := LineupData;

          //Start processing the data for the main template update
          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) then
          begin
            //Start building command string
            UpdateCmdStr := 'B\UP\';
            for j := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
              //Version 2.1
              //Check for single fields (not compound fields required for 2-line, new look engine)
              if (TemplateFieldsRecPtr^.Field_Type > 0) then
              begin
                //Filter out fields ID values of -1 => League designator
                //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
                begin
                  //Add region command
                  //If not a symbolic fields, send field contents directly
                  if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                  begin
                    TempStr := TemplateFieldsRecPtr^.Field_Contents;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                    if (Trim(TempStr) = '') then TempStr := ' ';
                  end
                  //It's a symbolic name, so get the field contents
                  else begin
                    try
                      Case MasterSegmentIndex of
                        TICKER: begin
                            SymbolValueData := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents,
                              CurrentTickerEntryIndex, CurrentGameData, TickerDisplayMode);
                            TempStr := SymbolValueData.SymbolValue;
                            //If data not received, skip record
                            if (SymbolValueData.OKToDisplay = FALSE) then
                            begin
                              JumpToNextTickerRecordTimer.Enabled := TRUE;
                              Exit;
                            end;
                          end;
                        BREAKINGNEWS: begin
                            TempStr := GetValueOfSymbol(BREAKINGNEWS, TemplateFieldsRecPtr^.Field_Contents,
                              CurrentBreakingNewsEntryIndex, CurrentGameData, TickerDisplayMode).SymbolValue;
                          end;
                      end;
                      //Add previx and/or suffix if applicable
                      if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                      if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                    end;
                    if (Trim(TempStr) = '') then TempStr := ' ';
                  end;
                end;
              end;
              //Add name\value pair to command string - always start with "A" side
              UpdateCmdStr :=  UpdateCmdStr + TemplateFieldsRecPtr^.Scene_Field_Name_A + '�' + TempStr;

              SetTemplateFieldLastValueCheckString(TemplateFieldsRecPtr^.Field_ID, TempStr);

              if (j < Temporary_Fields_Collection.Count-1) then
                UpdateCmdStr :=  UpdateCmdStr + '\';
            end;
            //Close the command
            UpdateCmdStr := UpdateCmdStr + '\\';

            //Write out to the as-run log if it's a sponsor logo
            if (MasterSegmentIndex = TICKER) AND (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            begin
              if (TemplateInfo.TemplateSponsorType = 1) OR (TemplateInfo.TemplateSponsorType = 2) then
                WriteToAsRunLog('Started display of sponsor logo in template: ' + CurrentSponsorInfo.CurrentSponsorLogoName)
              else if (TemplateInfo.TemplateSponsorType = 3) OR (TemplateInfo.TemplateSponsorType = 4) then
                WriteToAsRunLog('Started display of sponsor logo in lower-right region: ' + CurrentSponsorInfo.CurrentSponsorLogoName)
            end;
          end;

          //Set data row indicator in Ticker grid
          Case MasterSegmentIndex of
            TICKER: begin
                MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
                MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
              end;
            BREAKINGNEWS: begin
                MainForm.BreakingNewsPlaylistGrid.CurrentDataRow := CurrentBreakingNewsEntryIndex+1;
                MainForm.BreakingNewsPlaylistGrid.SetTopLeft(1, MainForm.BreakingNewsPlaylistGrid.CurrentDataRow);
              end;
          end;

          //Substitute style chip codes
          UpdateCmdStr := ProcessStyleChips(UpdateCmdStr);

          //Send the commands
          if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
          begin
            try
              if (TempTemplateCommand_Collection.Count > 0) then
              begin
                //Update the game status for the winner chips
                UpdateScoreChipStatus(CurrentGameData);

                for i := 0 to TempTemplateCommand_Collection.Count-1 do
                begin
                  TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                  Case TempTemplateCommandRecPtr^.CommandType of
                    COMMAND_TYPE_UPDATE_TEMPLATE_FIELDS: begin
                      //Add command to queue if not empty
                      if (UpdateCmdStr <> 'B\UP\\') then
                        AddCommandToQueue(UpdateCmdStr, TempTemplateCommandRecPtr^.Delay);

                      //Now bring in the new score chip if applicable
                      if (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS) then
                      for m := 1 to 6 do
                      begin
                        //Now, bring in the new chip
                        if (ScoreChipStatus[m].CurrentChipStatus <> ScoreChipStatus[m].NewChipStatus) and
                           (ScoreChipStatus[m].NewChipStatus = TRUE) then
                        begin
                          AddCommandToQueue ('B\PL\' + ScoreChipStatus[m].ChipDirectorName + '�1\\', 250);
                        end;
                        //Set updated status
                        ScoreChipStatus[m].CurrentChipStatus := ScoreChipStatus[m].NewChipStatus;
                        ScoreChipStatus[m].NewChipStatus := FALSE;
                      end;
                    end;
                    COMMAND_TYPE_FIRE_SCENE_DIRECTOR: begin
                      //Add command to queue
                      SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                        TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
                      //Set flag to indicate that the first template with data has fired
                      InitialDataTemplateFired := TRUE;
                    end;
                  end;
                end;

                //Check for score alert if a game final data template and fire if needed
                if (TemplateInfo.UsesGameData) and (TemplateInfo.RequiredGameState = 3) then
                begin
                  if (CheckForScoreAlertTrue(TickerRecPtr^.League, TRIM(TickerRecPtr^.GameID), CurrentGameData.State)) then
                  begin
                    //Update the game state
                    AddCommandToQueue ('B\PL\' + 'SCORE_ALERT' + '�1\\', 50);
                  end;
                end;
                UpdateGameInScoreAlertGamesCollection(TickerRecPtr^.League, TRIM(TickerRecPtr^.GameID), CurrentGameData.State);
              end;

              //Get the total time required to clear the queue before dwell and pad dwell timer
              TickerCommandDelayTimer.Interval := TickerCommandDelayTimer.Interval + GetTimeDurationForQueue;
              if (MainForm.CommandOutList.Visible) and (LOG_DELAYS) then
                MainForm.CommandOutList.Lines.Add('Total Delay: ' + IntToStr(TickerCommandDelayTimer.Interval));

              //Set last template ID - used to reset on next template
              LastTemplateID := TemplateInfo.Template_ID;
              LastSceneDirectorID := TemplateInfo.SceneDirectorID;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying send ticker command to engine');
            end;
            //Enable packet timeout timer
            TickerPacketTimeoutTimer.Enabled := FALSE;
            TickerPacketTimeoutTimer.Enabled := TRUE;

            //Set last game ID displayed
            if ((TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS)) then
              LastGameIDDisplayed := TickerRecPtr^.GameID
            else
              LastGameIDDisplayed := '0';

            //Start timer to send next record - timer should always be fired - will be disabled when crawl is required
            if (USEDATAPACKET = FALSE) then TickerCommandDelayTimer.Enabled := TRUE;
          end
          else if (FoundGame = FALSE) then
          begin
            //Enable delay timer
            if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
            //Clear flag
            InitialDataTemplateFired := FALSE;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send initial text and start ticker');
    end;
  end;
  //Set dwell label value
  MainForm.DwellValue.Caption := IntToStr(TickerCommandDelayTimer.Interval);
end;

//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  CmdStr: String;
begin
  //Get the data
  Data := Socket.ReceiveText;

  //Log the response if enabled
  if (MainForm.CommandOutList.Visible) and (LOG_ENGINE_RESPONSES) then
  begin
    if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
    MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Response from Engine: '  + Data);
  end;

  //Log response if command logging enabled
  if (CommandLoggingEnabled) then
  begin
    WriteToCommandLog('Data Packet received: ' + Data);
  end;

  //Light the indicator
  MainForm.ApdStatusLight5.Lit := TRUE;
  //Light ACK received indicator
  ACKReceivedLEDTimer.Enabled := TRUE;

  //Process crawl flags/state
  //Data not crawling
  if (Pos('0', Data) > 0) then
  begin
    CrawlIsEnabled := FALSE;
    //Crawl not required, so just flip the state of the crawl animation controller so it always stays paired up A-A, B-B
    //with the main text field animation controller and reset the animation controller so it's ready to go the next time
    //it's required
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) and
         (TempTemplateCommandRecPtr^.ActionIsCrawl = TRUE) then
      begin
        if (GetActionControllerState(TempTemplateCommandRecPtr^.ActionControllerID) = ACTION_CONTROLLER_OUT) or
           (GetActionControllerState(TempTemplateCommandRecPtr^.ActionControllerID) = ACTION_CONTROLLER_IN_B) then
        begin
          //Flip the state of the animation controller
          SetActionControllerState(TempTemplateCommandRecPtr^.ActionControllerID, ACTION_CONTROLLER_IN_A);
          //Reset the current animation controller
          CmdStr := 'B\PL\' +  GetActionControllerNameFromID(TempTemplateCommandRecPtr^.ActionControllerID, ACTION_CONTROLLER_IN_A) + '�0\\';
          AddCommandToQueue(CmdStr, 50);
        end
        else if (GetActionControllerState(TempTemplateCommandRecPtr^.ActionControllerID) = ACTION_CONTROLLER_IN_A) then
        begin
          //Flip the state of the animation controller
          SetActionControllerState(TempTemplateCommandRecPtr^.ActionControllerID, ACTION_CONTROLLER_IN_B);
          //Reset the current animation controller
          CmdStr := 'B\PL\' +  GetActionControllerNameFromID(TempTemplateCommandRecPtr^.ActionControllerID, ACTION_CONTROLLER_IN_B) + '�0\\';
          AddCommandToQueue(CmdStr, 50);
        end;
      end;
    end;
  end
  //Data needs to crawl
  else if (Pos('1', Data) > 0) then
  begin
    //Fire scene director for crawl
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.CommandType = COMMAND_TYPE_FIRE_ACTION_CONTROLLER) and
         (TempTemplateCommandRecPtr^.ActionIsCrawl = TRUE) then
      begin
        //V4.0.7 Modified to setup crawl only if there is a template command flagged for crawls attached to the template
        //Setup for crawl
        //Enable crawl flag
        CrawlIsEnabled := TRUE;

        //Disable command dwell timer
        TickerCommandDelayTimer.Enabled := FALSE;
        //Enable watchdog timer for 1 minute
        TickerPacketTimeoutTimer.Enabled := FALSE;
        TickerPacketTimeoutTimer.Interval := 60000;
        TickerPacketTimeoutTimer.Enabled := TRUE;

        //Add command to queue to crawl data
        SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
          TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
      end;
    end;
  end
  //Crawl is done
  //V4.0.7 Modified to respond to the "2" only if the crawl flag was set on receipt of a "1"
  else if (Pos('2', Data) > 0) and (CrawlIsEnabled) then
  begin
    //Clear flag
    CrawlIsEnabled := FALSE;
    //Disable watchdog timer
    TickerPacketTimeoutTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(TRUE, 0);
  end;

  //Log the response if enabled
  if (MainForm.CommandOutList.Visible) and (LOG_ENGINE_RESPONSES) then
  begin
    if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
    if (TickerCommandDelayTimer.Enabled) then
      MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Delay timer enabled')
    else
      MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Delay timer disabled');
  end;
end;

//Timer to extinguish ACK received indicator
procedure TEngineInterface.ACKReceivedLEDTimerTimer(Sender: TObject);
begin
  ACKReceivedLEDTimer.Enabled := FALSE;
  MainForm.ApdStatusLight5.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      TickerCommandDelayTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(TRUE, 0);

      //Log command if enabled
      if (MainForm.CommandOutList.Visible) and (LOG_TIMER_EVENTS) then
      begin
        if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
        MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Main Dwell Timer Event Fired');
      end;
    end;
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      JumpToNextTickerRecordTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(TRUE, 0);
    end;
  end;
end;

//Function to return the Scene Director name for a given template command
function TEngineInterface.GetSceneDirectorNameForAction (ActionType, CommandType: SmallInt): String;
var
  i: SmallInt;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  OutVal: String;
begin
  OutVal := '';
  //Substitute the field update into the command value of the update fields command in the command collection
  if (TempTemplateCommand_Collection.Count > 1) then
  begin
    for i := 0 to TempTemplateCommand_Collection.Count-1 do
    begin
      TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
      if (TempTemplateCommandRecPtr^.ActionType = ActionType) and (TempTemplateCommandRecPtr^.CommandType = CommandType) then
        OutVal := TempTemplateCommandRecPtr^.SceneDirectorName;
    end;
  end;
  GetSceneDirectorNameForAction := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k,m: SmallInt;
  TempStr, CmdStr, UpdateCmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtrNew: ^BreakingNewsRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OkToGo: Boolean;
  UseFieldDelayTimer: Boolean;
  TempTickerEntryIndex: SmallInt;
  TokenCounter: SmallInt;
  SymbolValueData: SymbolValueRec;
  TempTemplateCommandRecPtr: ^TemplateCommandRec;
  CurrentSceneDirectorName: String;
  CurrentSceneDirectorState: SmallInt;
  RanLineupForNewTopic: Boolean;
  CurrentActionState: SmallInt;
  FieldTextHasChanged: Boolean;
begin
  //Init flags
  USEDATAPACKET := FALSE;
  
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  UseFieldDelayTimer := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;
  RanLineupForNewTopic := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Init packet timeout timer to disabled
  TickerPacketTimeoutTimer.Enabled := FALSE;

  //Set looping mode as required
  Case TickerDisplayMode of
    //1x through for 1-lime, 2-line
    1,3: LoopTicker := FALSE;
    //Continuous loop for 1-lime, 2-line
    2,4: LoopTicker := TRUE;
  end;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Set temporary ticker rec pointer - used for segment mode breaking news
    if (CurrentTickerEntryIndex < Ticker_Collection.Count-1) then
    begin
      TempTickerEntryIndex := CurrentTickerEntryIndex+1;
      TickerRecPtrNew := Ticker_Collection.At(TempTickerEntryIndex);
    end
    else
      TickerRecPtrNew := Ticker_Collection.At(0);

    //Check if switch required to breaking news immediate mode; index will be incremented below
    if (MasterSegmentIndex = TICKER) AND (BreakingNewsSettings.BreakingNewsEnable) AND
       (BreakingNewsSettings.BreakingNewsMode = IMMEDIATE) AND (BreakingNews_Collection.Count > 0) AND
       (BreakingNewsIterationsCompleted < BreakingNewsSettings.Iterations) then
    begin
      MasterSegmentIndex := BREAKINGNEWS;
      CurrentBreakingNewsEntryIndex := -1;
      BreakingNewsIterationsCompleted := 1;
    end
    //Check if switch required to breaking news standard mode; index will be incremented below
    else if (MasterSegmentIndex = TICKER) AND (BreakingNewsSettings.BreakingNewsEnable) AND
       (BreakingNewsSettings.BreakingNewsMode = SEGMENT) AND (BreakingNews_Collection.Count > 0) AND
       (BreakingNewsIterationsCompleted < BreakingNewsSettings.Iterations) AND
       (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND (LoopTicker = TRUE) then
    begin
      MasterSegmentIndex := BREAKINGNEWS;
      CurrentBreakingNewsEntryIndex := -1;
      BreakingNewsIterationsCompleted := 1;
    end
    //V1.1.4 Add check for segment mode breaking news - first entry
    else if (MasterSegmentIndex = TICKER) AND (BreakingNewsSettings.BreakingNewsEnable) AND
       (BreakingNewsSettings.BreakingNewsMode = SEGMENT) AND (BreakingNews_Collection.Count > 0) AND
       (BreakingNewsIterationsCompleted < BreakingNewsSettings.Iterations) AND
       ((LastSegmentHeading <> TickerRecPtrNew^.League) OR
       //Special case for baseball since all leagues normalized to MLB
       ((LastSegmentHeading = 'MLB') AND (TickerRecPtrNew^.League <> 'AL') AND (TickerRecPtrNew^.League <> 'ML') AND
       (TickerRecPtrNew^.League <> 'NL') AND (TickerRecPtrNew^.League = 'IL') AND (TickerRecPtrNew^.League <> 'MLB'))) then
    begin
      MasterSegmentIndex := BREAKINGNEWS;
      CurrentBreakingNewsEntryIndex := -1;
      BreakingNewsIterationsCompleted := 1;
      //Set last segment heading - used for segment mode breaking news
      LastSegmentHeading := TickerRecPtrNew^.League;
    end
    //Check if already in breaking news, at end and in immediate mode, so reset to top or increment count
    else if (MasterSegmentIndex = BREAKINGNEWS) AND (BreakingNewsSettings.BreakingNewsEnable) AND
       (BreakingNewsSettings.BreakingNewsMode = IMMEDIATE) AND (BreakingNews_Collection.Count > 0) AND
       (CurrentBreakingNewsEntryIndex >= BreakingNews_Collection.Count-1) then
    begin
      //Check if more iterations to do
      //if (BreakingNewsIterationsCompleted <= BreakingNewsSettings.Iterations) then
      if (BreakingNewsIterationsCompleted < BreakingNewsSettings.Iterations) then
      begin
        //MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, 0);
        MasterSegmentIndex := BREAKINGNEWS;
        CurrentBreakingNewsEntryIndex := -1;
        //Increment the breaking news iterations
        Inc(BreakingNewsIterationsCompleted);
      end
      //Done with all iterations, so go to standard ticker mode
      else begin
        MasterSegmentIndex := TICKER;
        //CurrentTickerEntryIndex := Ticker_Collection.Count-1;
      end;
    end
    //Check if already in breaking news, but done displaying, so switch back to general ticker
    //Index will be incremented below
    else if (MasterSegmentIndex = BREAKINGNEWS) AND (BreakingNewsSettings.BreakingNewsEnable) AND
       (BreakingNewsSettings.BreakingNewsMode = SEGMENT) AND
       (CurrentBreakingNewsEntryIndex >= BreakingNews_Collection.Count-1) then
    begin
      //Check if more iterations to do in Breaking News
      //if (BreakingNewsIterationsCompleted <= BreakingNewsSettings.Iterations) then
      if (BreakingNewsIterationsCompleted < BreakingNewsSettings.Iterations) then
      begin
        //MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, 0);
        MasterSegmentIndex := BREAKINGNEWS;
        CurrentBreakingNewsEntryIndex := -1;
        //Increment the breaking news iterations
        Inc(BreakingNewsIterationsCompleted);
      end
      //Switch to Ticker mode
      else begin
        MasterSegmentIndex := TICKER;
        //CurrentTickerEntryIndex := Ticker_Collection.Count-1;
        //Reset iterations completed - required to show alerts prior to next segment
        BreakingNewsIterationsCompleted := 0;
        CurrentBreakingNewsEntryIndex := -1;
        //Set last segment heading - used for segment mode breaking news
        LastSegmentHeading := TickerRecPtrNew^.League;
      end;
    end
    //Check for breaking news now disabled
    else if (BreakingNewsSettings.BreakingNewsEnable = FALSE) then
    begin
      //Switch to Ticker Mode
      MasterSegmentIndex := TICKER;
    end;

    //Make sure we haven't dumped put
    if (TickerAbortFlag = FALSE) AND
      ((Ticker_Collection.Count > 0) OR (BreakingNews_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        Case MasterSegmentIndex of
          TICKER: begin
              if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
                 (LoopTicker = TRUE) then
              begin
                try
                  //Reload the Ticker collection in case it has been modified; clear flag to indicate
                  //it's not the first time through
                  MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                  if (Ticker_Collection.Count > 0) then CurrentTickerEntryIndex := 0;
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to reload main ticker collection');
                end;
                //Check time window and enable
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                //Make sure record is enabled and within time window
                j := 0;
                While ((TickerRecPtr^.Enabled = FALSE) OR NOT ((TickerRecPtr^.StartEnableDateTime <= Now) AND
                      (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < Ticker_Collection.Count) do
                begin
                  Inc(CurrentTickerEntryIndex);
                  //Set pointer
                  TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                  //Update the data from the database - called for each record
                  UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                  if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (LoopTicker = TRUE) then
                  begin
                    try
                      MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                      CurrentTickerEntryIndex := 0;
                      TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                      //Update the data from the database - called for each record
                      UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                      j := 0;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to reload main ticker collection');
                    end;
                  end
                  else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
                  begin
                    //Abort the ticker and exit this procedure
                    MainForm.AbortCurrentEvent;
                    MainForm.ResetTickerToTop;
                    PacketEnableTimer.Enabled := TRUE;
                    RunningTicker := FALSE;
                    //Dump out
                    Exit;
                  end;
                  Inc(j);
                end;
                //Set flag if no enabled records found
                if (j >= Ticker_Collection.Count) then
                begin
                  OKToGo := FALSE;
                  //Abort the ticker and exit this procedure
                  MainForm.AbortCurrentEvent;
                  MainForm.ResetTickerToTop;
                  PacketEnableTimer.Enabled := TRUE;
                  RunningTicker := FALSE;
                  //Dump out
                  Exit;
                end;
              end
              //Not last record, so increment Ticker playout collection object index
              else if (CurrentTickerEntryIndex < Ticker_Collection.Count-1) then
              begin
                //Make sure record is enabled and within time window
                j := 0;
                Repeat
                  Inc(CurrentTickerEntryIndex);
                  if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (LoopTicker = TRUE) then
                  begin
                    try
                      MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                      CurrentTickerEntryIndex := 0;
                      TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                      //Update the data from the database - called for each record
                      UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                      j := 0;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to reload main ticker collection');
                    end;
                  end
                  //Abort ticker if not looping
                  else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
                  begin
                    //Abort the ticker and exit this procedure
                    MainForm.AbortCurrentEvent;
                    MainForm.ResetTickerToTop;
                    PacketEnableTimer.Enabled := TRUE;
                    RunningTicker := FALSE;
                    //Dump out
                    Exit;
                  end;
                  TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                  Inc (j);
                  //Update the data from the database - called for each record
                  UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
                Until ((TickerRecPtr^.Enabled = TRUE) AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
                      (TickerRecPtr^.EndEnableDateTime > Now)) OR (j >= Ticker_Collection.Count);
                //Set flag if no enabled records found
                if (j >= Ticker_Collection.Count) then
                begin
                  OKToGo := FALSE;
                  //Abort the ticker and exit this procedure
                  MainForm.AbortCurrentEvent;
                  MainForm.ResetTickerToTop;
                  PacketEnableTimer.Enabled := TRUE;
                  RunningTicker := FALSE;
                  //Dump out
                  Exit;
                end;
              end
              //At end and not looping, so clear screen
              else if (CurrentTickerEntryIndex >= Ticker_Collection.Count-1) AND
                      (LoopTicker = FALSE) then
              begin
                MainForm.AbortCurrentEvent;
                MainForm.ResetTickerToTop;
                PacketEnableTimer.Enabled := TRUE;
                RunningTicker := FALSE;
                //Dump out
                Exit;
              end;
              //Set last segment heading - used for segment mode breaking news
              LastSegmentHeading := TickerRecPtr^.League;
            end;
          BREAKINGNEWS: begin
              if (CurrentBreakingNewsEntryIndex >= BreakingNews_Collection.Count-1) AND
                 (LoopTicker = TRUE) then
              begin
                try
                  //Reload the Ticker collection in case it has been modified; clear flag to indicate
                  //it's not the first time through
                  MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, 0);
                  if (BreakingNews_Collection.Count > 0) then CurrentBreakingNewsEntryIndex := 0;
                  //Increment the breaking news iterations
                  Inc(BreakingNewsIterationsCompleted);
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to reload Breaking News collection');
                end;
                //Check time window and enable
                BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
                //Make sure record is enabled and within time window
                j := 0;
                While ((BreakingNewsRecPtr^.Enabled = FALSE) OR NOT ((BreakingNewsRecPtr^.StartEnableDateTime <= Now) AND
                      (BreakingNewsRecPtr^.EndEnableDateTime > Now))) AND (j < BreakingNews_Collection.Count) do
                begin
                  Inc(CurrentBreakingNewsEntryIndex);
                  //Set pointer
                  BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
                  //Update the data from the database - called for each record
                  if (CurrentBreakingNewsEntryIndex = BreakingNews_Collection.Count) then
                  begin
                    try
                      MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, 0);
                      CurrentBreakingNewsEntryIndex := 0;
                      BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
                      j := 0;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to reload Breaking News collection');
                    end;
                    //Increment the breaking news iterations
                    Inc(BreakingNewsIterationsCompleted);
                  end;
                  Inc(j);
                end;
                //Set flag if no enabled records found
                //if (j >= BreakingNews_Collection.Count) then OKToGo := FALSE;
                if (j > BreakingNews_Collection.Count) then OKToGo := FALSE;
              end
              //Not last record, so increment Ticker playout collection object index
              else if (CurrentBreakingNewsEntryIndex < BreakingNews_Collection.Count-1) then
              begin
                //Make sure record is enabled and within time window
                j := 0;
                Repeat
                  Inc(CurrentBreakingNewsEntryIndex);
                  if (CurrentBreakingNewsEntryIndex = BreakingNews_Collection.Count) then
                  begin
                    try
                      MainForm.LoadPlaylistCollection(BREAKINGNEWS, 0, 0);
                      CurrentBreakingNewsEntryIndex := 0;
                      BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
                      j := 0;
                      //Increment the breaking news iterations
                      Inc(BreakingNewsIterationsCompleted);
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to reload Breaking News collection');
                    end;
                  end;
                  BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
                  Inc (j);
                Until ((BreakingNewsRecPtr^.Enabled = TRUE) AND (BreakingNewsRecPtr^.StartEnableDateTime <= Now) AND
                      (BreakingNewsRecPtr^.EndEnableDateTime > Now)) OR (j >= BreakingNews_Collection.Count);
                //Set flag if no enabled records found
                //if (j > BreakingNews_Collection.Count) then OKToGo := FALSE;
                if (j > BreakingNews_Collection.Count) then OKToGo := FALSE;
              end
              //At end and not looping, so clear screen
              else if (CurrentBreakingNewsEntryIndex >= BreakingNews_Collection.Count-1) AND
                      (LoopTicker = FALSE) then
              begin
                MainForm.AbortCurrentEvent;
                MainForm.ResetTickerToTop;
                RunningTicker := FALSE;
                PacketEnableTimer.Enabled := TRUE;
                //Increment to end
                Inc(CurrentBreakingNewsEntryIndex);
              end;
            end;
        end;
        //Clear ticker if at last collection obejct and not looping
        if (CurrentTickerEntryIndex > Ticker_Collection.Count-1) AND
           (LoopTicker = FALSE) then
        begin
          MainForm.AbortCurrentEvent;
          MainForm.ResetTickerToTop;
          RunningTicker := FALSE;
          PacketEnableTimer.Enabled := TRUE;
          //Dump out
          Exit;
        end;
        //Disable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        CurrentTickerEntryIndex := RecordIndex;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (((MasterSegmentIndex = 1) AND (CurrentTickerEntryIndex <= Ticker_Collection.Count-1)) OR
          ((MasterSegmentIndex = 2) AND (CurrentBreakingNewsEntryIndex <= BreakingNews_Collection.Count-1))) AND
          (OKToGo) then
      begin
        Case MasterSegmentIndex of
          TICKER: begin
              //Get pointer to current record
              TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
              //Update the data from the database - called for each record
              UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);
            end;
          BREAKINGNEWS: begin
              //Get pointer to current record
              BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentBreakingNewsEntryIndex);
            end;
        end;

        //Set default mode
        CmdStr := '';

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        Case MasterSegmentIndex of
          TICKER: begin
              TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);
            end;
            BREAKINGNEWS: begin
              TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);
            end;
        end;

        //If in ticker mode, get game data & process to check game states
        Case MasterSegmentIndex of
          TICKER: begin
              //Get game data if template requires it and in ticker mode
              if (TemplateInfo.UsesGameData) then
              begin
                try
                  //Use full game data
                  CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                except
                  if (ErrorLoggingEnabled = True) then
                    WriteToErrorLog('Error occurred while trying to get game data for ticker');
                end;
                FoundGame := CurrentGameData.DataFound;
              end;

              //Check the game state vs. the state required by the template if not a match, increment
              //Also do check for enabled entries and check time window
              if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.StateForInProgressCheck <> TemplateInfo.RequiredGameState) AND
                 //Special case for postponed, delayed or suspended MLB games
                 (not(((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                       (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                       (TemplateInfo.RequiredGameState = INPROGRESS))) and
                 (NEXTRECORD = TRUE) then
              Repeat
                Inc(CurrentTickerEntryIndex);
                if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = TRUE) then
                begin
                  MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                  CurrentTickerEntryIndex := 0;
                end
                else if (CurrentTickerEntryIndex = Ticker_Collection.Count) AND (Loopticker = FALSE) then
                begin
                  //Abort the ticker and exit this procedure
                  MainForm.AbortCurrentEvent;
                  MainForm.ResetTickerToTop;
                  PacketEnableTimer.Enabled := TRUE;
                  RunningTicker := FALSE;
                  //Dump out
                  Exit;
                end;
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex);
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(CurrentTickerPlaylistID, CurrentTickerEntryIndex, TickerRecPtr^.Event_GUID);

                //Just get basic template info
                TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);
                if (TemplateInfo.UsesGameData) then
                begin
                  try
                    //Use full game data
                    CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get game data for ticker');
                  end;
                  FoundGame := CurrentGameData.DataFound;
                end
                else begin
                  CurrentGameData.State := UNDEFINED;
                  FoundGame := TRUE;
                end;
              //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
              Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.StateForInProgressCheck = TemplateInfo.RequiredGameState) OR
                    //Special case for postponed, delayed or suspended MLB games
                    (((CurrentGameData.State = -1) OR (CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR
                      (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = CANCELLED)) AND
                      (TemplateInfo.RequiredGameState = INPROGRESS))) AND
                    ((TickerRecPtr^.Enabled = TRUE) AND
                    (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));
              //Start timer to send next record - timer should always be fired - will be disabled when crawl is required
              //USEDATAPACKET := TRUE;

              //Set the default dwell time - minimum = 2000mS; set packet timeout interval
              //If lower-right sponsor template, sponsor logo dwell does not need to be specified
              if (TemplateInfo.TemplateSponsorType = 1) then
              begin
                //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
                CurrentSponsorInfo.CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
                CurrentSponsorInfo.CurrentSponsorLogoDwell := TickerRecPtr^.SponsorLogo_Dwell;
                CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
              end
              //If clear lower-right sponsor template, clear the name
              else if (TemplateInfo.TemplateSponsorType = 2) then
              begin
                //Set the current sponsor logo name, template, taglines & dwell for the lower-right sponsor logo
                CurrentSponsorInfo.CurrentSponsorLogoName := '';
                CurrentSponsorInfo.CurrentSponsorLogoDwell := 0;
                CurrentSponsorInfo.CurrentSponsorTemplate := TickerRecPtr^.Template_ID;
              end
              //Not a sponsor logo - just set the dwell time
              else if (TickerRecPtr^.DwellTime > 2000) then
              begin
                TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
                TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
              end
              else begin
                TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
                if (TickerRecPtr^.DwellTime = 1) then
                  TickerPacketTimeoutTimer.Interval := 90000
                else
                  TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
              end;
            end;
          BREAKINGNEWS: begin
              //Set the default dwell time - minimum = 2000mS; set packet timeout interval
              if (BreakingNewsRecPtr^.DwellTime > 2000) then
              begin
                TickerCommandDelayTimer.Interval := BreakingNewsRecPtr^.DwellTime;
                TickerPacketTimeoutTimer.Interval := BreakingNewsRecPtr^.DwellTime + 60000;
              end
              else begin
                TickerCommandDelayTimer.Interval := BreakingNewsRecPtr^.DwellTime;
                if (BreakingNewsRecPtr^.DwellTime = 1) then
                  TickerPacketTimeoutTimer.Interval := 90000
                else
                  TickerPacketTimeoutTimer.Interval := BreakingNewsRecPtr^.DwellTime + 60000;
              end;
            end;
        end;

        ////////////////////////////////////////////////////////////////////////
        // Start command processing
        ////////////////////////////////////////////////////////////////////////
        //Bring in the time of day clock if no sponsor
        if (CurrentSponsorInfo.CurrentSponsorLogoName = '') and (GetSceneDirectorState('CLOCK') = SCENE_DIRECTOR_OUT) then
        begin
          UpdateTimeOfDayClocks;
          SetSceneDirectorState('CLOCK', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
          CmdStr := 'B\PL\CLOCK�1\\';
          AddCommandToQueue(CmdStr, 200);
        end;

        //Check to see if active sponsor logo needs to be cleared for main sponsor templates
        if (CheckForActiveSponsorLogo = FALSE) then CurrentSponsorInfo.CurrentSponsorLogoName := '';

        //If lower-right sponsor template, bring sponsor in/out then skip to next record - will bring data in then
        //Also, bring back in if it was taken out for an alert, etc.
        if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_IN) then
        begin
          //Set sponsor logo
          CmdStr := 'B\MT\SPONSOR_IMG�' + CurrentSponsorInfo.CurrentSponsorLogoName + '\\';
          AddCommandToQueue(CmdStr, 50);

          //Bring sponsor in if it was out
          if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) then
          begin
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 1500);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
          end;

          //Exit and send next record
          JumpToNextTickerRecordTimer.Enabled := TRUE;
          Exit;
        end
        //This block happens when we're returning out of breaking news and a sponsor was in
        else if ((GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) and
                (CurrentSponsorInfo.CurrentSponsorLogoName <> '') and (TemplateInfo.Template_Type <> TEMPLATE_TYPE_BREAKING_NEWS)) then
        begin
          //Set sponsor logo
          CmdStr := 'B\MT\SPONSOR_IMG�' + CurrentSponsorInfo.CurrentSponsorLogoName + '\\';
          AddCommandToQueue(CmdStr, 50);

          //Bring sponsor in if it was out
          if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_OUT) then
          begin
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 50);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL)
          end;
          //Don't dump out - just go onto rest of template processing
        end
        else if (TemplateInfo.TemplateSponsorType = TEMPLATE_SPONOSR_TYPE_OUT) then
        begin
          //Take out sponsor in if it was in
          if (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_IN_A) then
          begin
            //Take sponsor out
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 500);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_OUT, DONT_RESET_ALL)
          end;

          //Bring in the time of day clock if no sponsor
          if (GetSceneDirectorState('CLOCK') = SCENE_DIRECTOR_OUT) then
          begin
            UpdateTimeOfDayClocks;
            SetSceneDirectorState('CLOCK', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
            CmdStr := 'B\PL\CLOCK�1\\';
            AddCommandToQueue(CmdStr, 250);
          end;

          //Exit and send next record
          JumpToNextTickerRecordTimer.Enabled := TRUE;
          Exit;
        end;

        ////////////////////////////////////////////////////////////////////////
        //Fire alerts and line-ups as needed
        ////////////////////////////////////////////////////////////////////////
        //First, check if alert type coming in for first time
        if ((TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) or
            (TemplateInfo.Template_Type = TEMPLATE_TYPE_PROGRAM_ALERT)) and
           ((InitialDataTemplateFired = FALSE) or (TemplateInfo.Template_ID <> LastTemplateID)) then
        begin
          //Trigger sponsor out if breaking news
          if (TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) and
             (GetSceneDirectorState('SPONSOR') = SCENE_DIRECTOR_IN_A) then
          begin
            //CmdStr := '<DELAY>';
            //AddCommandToQueue(CmdStr, 1500);
            CmdStr := 'B\PL\SPONSOR�1\\';
            AddCommandToQueue(CmdStr, 200);
            //Update the state
            SetSceneDirectorState('SPONSOR', SCENE_DIRECTOR_OUT, DONT_RESET_ALL)
          end;

          //Take out the last template
          LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_OUT);

          //Fire actions to take director out
          if (TempTemplateCommand_Collection.Count > 0) then
          begin
            for i := 0 to TempTemplateCommand_Collection.Count-1 do
            begin
              TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
              SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
            end;
          end;

          //Trigger line-up out
          if (GetSceneDirectorState('LINEUP') = SCENE_DIRECTOR_IN_A) then
          begin
            CmdStr := 'B\PL\LINEUP�1\\';
            //Add command to queue
            //AddCommandToQueue(CmdStr, LineupOutDelay);
            AddCommandToQueue(CmdStr, 1000);
            SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_OUT, DONT_RESET_ALL);
          end;

          //Get & fire commands to clear template fields
          LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_RESET);

          //Fire actions to take director out
          if (TempTemplateCommand_Collection.Count > 0) then
          begin
            for i := 0 to TempTemplateCommand_Collection.Count-1 do
            begin
              TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
              SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
            end;
          end;

          //Reload template data for new alerts template
          TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_IN_INITIAL);

          //Reset so line-up runs with next regular template
          LastLineupData.TextFields[1] := '';
        end
        ////////////////////////////////////////////////////////////////////////
        // Main block of code for bringing in next template
        ////////////////////////////////////////////////////////////////////////
        //Not breaking news or program alert  bookmark7
        else if (TemplateInfo.Template_Type <> 55)  and (TemplateInfo.Template_Type <> 66) then
        begin
          ////////////////////////////////////////////////////////////////////////
          //Fire line-up text if it's changed
          ////////////////////////////////////////////////////////////////////////
          LineupData := GetLineuptext(CurrentTickerEntryIndex, TickerDisplayMode);
          if (LineupData.TextFields[1] <> LastLineupData.TextFields[1]) then
          begin
            //Take out the last template
            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_OUT);
            //Fire actions to take director out
            if (TempTemplateCommand_Collection.Count > 0) then
            begin
              for i := 0 to TempTemplateCommand_Collection.Count-1 do
              begin
                TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                  TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
              end;
            end;

            //Trigger line-up out if needed
            if (GetSceneDirectorState('LINEUP') = SCENE_DIRECTOR_IN_A) then
            begin
              CmdStr := 'B\PL\LINEUP�1\\';
              //Add command to queue
              AddCommandToQueue(CmdStr, LineupOutDelay);
              SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_OUT, DONT_RESET_ALL);
            end;

            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_RESET);
            //Fire actions to clear fields
            if (TempTemplateCommand_Collection.Count > 0) then
            begin
              for i := 0 to TempTemplateCommand_Collection.Count-1 do
              begin
                TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                  TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
              end;
            end;

            //Send new line-up text
            CmdStr := 'B\UP\prime_header_txt�' + LineupData.TextFields[1] +
              '\header_2_txt�' + LineupData.TextFields[2] +
              '\header_3_txt�' + LineupData.TextFields[3] +
              '\header_4_txt�' + LineupData.TextFields[4] +
              '\header_5_txt�' + LineupData.TextFields[5] +
              '\header_6_txt�' + LineupData.TextFields[6] +
              '\\';
            //Add command to queue
            AddCommandToQueue(CmdStr, 50);

            //Trigger line-up back in
            if (GetSceneDirectorState('LINEUP') = SCENE_DIRECTOR_OUT) then
            begin
              CmdStr := 'B\PL\LINEUP�1\\';
              //Add command to queue
              AddCommandToQueue(CmdStr, LineupInDelay);
              SetSceneDirectorState('LINEUP', SCENE_DIRECTOR_IN_A, DONT_RESET_ALL);
            end;

            //Delay
            AddCommandToQueue('<DELAY>', LineupToFirstPageDelay);

            //Set flag - used to determine state below
            RanLineupForNewTopic := TRUE;

            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(TemplateInfo.Template_ID, ACTION_TEMPLATE_IN_INITIAL);

            //Set last template ID
            LastTemplateID := -1;
            LastSceneDirectorID := -1;

            //Save state of last line-up
            LastLineupData := LineupData;
          end
          //Lineup hasn't changed, so just switch out templates
          else if (TemplateInfo.SceneDirectorID <> LastSceneDirectorID) then
          begin
            //Take out the last template
            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_OUT);
            //Fire actions to take director out
            if (TempTemplateCommand_Collection.Count > 0) then
            begin
              for i := 0 to TempTemplateCommand_Collection.Count-1 do
              begin
                TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                  TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
              end;
            end;

            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(LastTemplateID, ACTION_TEMPLATE_RESET);
            //Fire actions to clear fields
            if (TempTemplateCommand_Collection.Count > 0) then
            begin
              for i := 0 to TempTemplateCommand_Collection.Count-1 do
              begin
                TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
                SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                  TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
              end;
            end;

            //Get & fire commands to clear template fields
            LoadAllTemplateInfo(TemplateInfo.Template_ID, ACTION_TEMPLATE_IN_INITIAL);
          end
          //Lineup hasn't changed and it's the same scene director, so just update the fields
          else if (TemplateInfo.SceneDirectorID = LastSceneDirectorID) then
          begin
            if (TemplateInfo.Template_ID = LastTemplateID) then
            begin
              //Get & fire commands to clear template fields
              TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);
            end
            //Same scene director, different template
            else if (TickerRecPtr^.Template_ID <> LastTemplateID) then
            begin
              TemplateInfo := LoadAllTemplateInfo(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);
              ReloadTemplateFields(TickerRecPtr^.Template_ID);
            end;
          end;
        end;

        //Start processing the data for the main template update
        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          //Start building command string
          UpdateCmdStr := 'B\UP\';

          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin

            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);

            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add previx and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    Case MasterSegmentIndex of
                      TICKER: begin
                          SymbolValueData := GetValueOfSymbol(TICKER, TemplateFieldsRecPtr^.Field_Contents,
                            CurrentTickerEntryIndex, CurrentGameData, TickerDisplayMode);
                          TempStr := SymbolValueData.SymbolValue;
                          //If data not received, skip record
                          if (SymbolValueData.OKToDisplay = FALSE) then
                          begin
                            JumpToNextTickerRecordTimer.Enabled := TRUE;
                            Exit;
                          end;
                        end;
                      BREAKINGNEWS: begin
                          TempStr := GetValueOfSymbol(BREAKINGNEWS, TemplateFieldsRecPtr^.Field_Contents,
                            CurrentBreakingNewsEntryIndex, CurrentGameData, TickerDisplayMode).SymbolValue;
                        end;
                    end;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';
                end;
              end;
            end;

            //Reset director states if we ran a new line-up
            if (j = 0) then
            begin
              if (RanLineupForNewTopic) then SetSceneDirectorState(CurrentSceneDirectorName, SCENE_DIRECTOR_OUT, RESET_ALL);
            end;
                  // bookmark1
            //UpdatFe apprpriate text fields based on current state of scene director
            //Get the current state of the action associated with the field and update its change check value
            CurrentActionState := GetActionControllerState(TemplateFieldsRecPtr^.Action_Controller_ID);
            //We're on the A-side or just started the template fresh
            if (CurrentActionState = ACTION_CONTROLLER_IN_A) then
            begin
              //If text has not changed, don't add to string to update command and delete action controller from play commands
              if (TempStr <> GetTemplateFieldLastValueCheckString(TemplateFieldsRecPtr^.Field_ID)) or (LastGameIDDisplayed <> TickerRecPtr^.GameID) or
                 //(TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_GAME_SLUG) or
                 //(TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_NCAA_GAME_SLUG) or
                 //Version 4.0.10 - add special case for breaking news, program alert simple news crawl
                 (TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) or (TemplateInfo.Template_Type = TEMPLATE_TYPE_PROGRAM_ALERT) or
                 (TemplateInfo.Template_Type = TEMPLATE_TYPE_GENERAL_NEWS) or
                 //Added by Anthony
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_GAME_SLUG_CENTER) or
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_R_MATCHUP_NOTE_IN) or
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_MATCHUP_NOTE_IN) or
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_R_MATCHUP_NOTE_IN)
                  then
              begin
                UpdateCmdStr :=  UpdateCmdStr + TemplateFieldsRecPtr^.Scene_Field_Name_B + '�' + TempStr + '\';
                SetTemplateFieldLastValueCheckString(TemplateFieldsRecPtr^.Field_ID, TempStr);
                TemplateFieldsRecPtr^.ValueHasChanged := TRUE;
              end;
            end
            //We're on the B-side
            else begin
              if (TempStr <> GetTemplateFieldLastValueCheckString(TemplateFieldsRecPtr^.Field_ID)) or (LastGameIDDisplayed <> TickerRecPtr^.GameID) or
                 //(TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_GAME_SLUG) or
                 //(TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_NCAA_GAME_SLUG) or
                 //Version 4.0.10 - add special case for breaking news, program alert simple news crawl
                 (TemplateInfo.Template_Type = TEMPLATE_TYPE_BREAKING_NEWS) or (TemplateInfo.Template_Type = TEMPLATE_TYPE_PROGRAM_ALERT) or
                 (TemplateInfo.Template_Type = TEMPLATE_TYPE_GENERAL_NEWS) or
                 //Added by Anthony
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_GAME_SLUG_CENTER) or
                  (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_R_MATCHUP_NOTE_IN) or
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_MATCHUP_NOTE_IN) or
                 (TemplateFieldsRecPtr^.Action_Controller_ID = ANIMATION_CONTROLLER_ID_R_MATCHUP_NOTE_IN)
                 then
              begin
                UpdateCmdStr :=  UpdateCmdStr + TemplateFieldsRecPtr^.Scene_Field_Name_A + '�' + TempStr + '\';
                SetTemplateFieldLastValueCheckString(TemplateFieldsRecPtr^.Field_ID, TempStr);
                TemplateFieldsRecPtr^.ValueHasChanged := TRUE;
              end;
            end;
          end;
          //Close the command
          UpdateCmdStr := UpdateCmdStr + '\';

          //Delete any action controllers where the text has not changed and only if the game ID is the same
          if (LastGameIDDisplayed = TickerRecPtr^.GameID) then
            DeleteActionControllersFromTempTemplateCommandCollection;

          //Write out to the as-run log if it's a sponsor logo
          if (MasterSegmentIndex = 1) AND (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorInfo.CurrentSponsorLogoName);
        end;

        //Set data row indicator in Ticker grid
        if (NEXTRECORD) then
        begin
          try
            Case MasterSegmentIndex of
              TICKER: begin
                  MainForm.TickerPlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex+1;
                  MainForm.TickerPlaylistGrid.SetTopLeft(1, MainForm.TickerPlaylistGrid.CurrentDataRow);
                end;
              BREAKINGNEWS: begin
                  MainForm.BreakingNewsPlaylistGrid.CurrentDataRow := CurrentBreakingNewsEntryIndex+1;
                  MainForm.BreakingNewsPlaylistGrid.SetTopLeft(1, MainForm.BreakingNewsPlaylistGrid.CurrentDataRow);
                end;
             end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition grid cursor to correct record');
          end;
        end;

        //Substitute style chip codes
        try
          UpdateCmdStr := ProcessStyleChips(UpdateCmdStr);
        except
          if (ErrorLoggingEnabled = True) then
            WriteToErrorLog('Error occurred while trying to process style chips for main ticker record');
        end;

        //Send the new data
        if (SocketConnected) AND (EngineParameters.Enabled = TRUE) AND (FoundGame = TRUE) then
        begin
          //Enable packet timeout timer
          if (NEXTRECORD) then
          begin
            TickerPacketTimeoutTimer.Enabled := TRUE;
          end;

          //////////////////////////////////////////////////////////////////////
          //This is where the commands are sent to the engine
          //////////////////////////////////////////////////////////////////////
          if (TempTemplateCommand_Collection.Count > 0) then
          begin
            //Update the game status for the winner chips
            UpdateScoreChipStatus(CurrentGameData);
            for i := 0 to TempTemplateCommand_Collection.Count-1 do
            begin
              TempTemplateCommandRecPtr := TempTemplateCommand_Collection.At(i);
              Case TempTemplateCommandRecPtr^.CommandType of
                COMMAND_TYPE_UPDATE_TEMPLATE_FIELDS: begin
                  //If it's a game record, run the winner chips first
                  if ((TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS)) and
                     (LastGameIDDisplayed <> TickerRecPtr^.GameID) then
                  for m := 1 to 6 do
                  begin
                    //First, take out any chip that is in
                    if (ScoreChipStatus[m].CurrentChipStatus <> ScoreChipStatus[m].NewChipStatus) and
                       (ScoreChipStatus[m].CurrentChipStatus = TRUE) then
                    begin
                      AddCommandToQueue ('<DELAY>', 50);
                      AddCommandToQueue ('B\PL\' + ScoreChipStatus[m].ChipDirectorName + '�1\\', 150);
                    end;
                  end;
                  //Add command to queue if not empty
                  if (UpdateCmdStr <> 'B\UP\\') then
                    AddCommandToQueue(UpdateCmdStr, TempTemplateCommandRecPtr^.Delay);
                end;
                COMMAND_TYPE_FIRE_SCENE_DIRECTOR, COMMAND_TYPE_FIRE_ACTION_CONTROLLER: begin
                  //Don't fire any crawl action controllers - will be done once gateway sends back crawl status
                  if (TempTemplateCommandRecPtr^.ActionIsCrawl = FALSE) then
                  begin
                    //Add command to queue
                    SendPlayCommandStrings(TempTemplateCommandRecPtr^.ActionType, TempTemplateCommandRecPtr^.CommandType,
                      TempTemplateCommandRecPtr^.SceneDirectorID, TempTemplateCommandRecPtr^.ActionControllerID, TempTemplateCommandRecPtr^.Delay, TempTemplateCommandRecPtr^.ActionIsCrawl);
                  end;
                end;
              end;
              if (i = TempTemplateCommand_Collection.Count-1) then
              begin
                //Now, bring in the new chip
                if ((TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS)) and
                    (LastGameIDDisplayed <> TickerRecPtr^.GameID) then
                for m := 1 to 6 do
                begin
                  //Now, bring in the new chip
                  if (ScoreChipStatus[m].CurrentChipStatus <> ScoreChipStatus[m].NewChipStatus) and
                     (ScoreChipStatus[m].NewChipStatus = TRUE) then
                  begin
                    AddCommandToQueue ('<DELAY>', 50);
                    AddCommandToQueue ('B\PL\' + ScoreChipStatus[m].ChipDirectorName + '�1\\', 200);
                  end;
                  //Set updated status
                  ScoreChipStatus[m].CurrentChipStatus := ScoreChipStatus[m].NewChipStatus;
                  ScoreChipStatus[m].NewChipStatus := FALSE;
                end;
              end;
            end;

            //Reset the change flags for the temporary fields
            if (Temporary_Fields_Collection.Count > 0) then
            begin
              for j := 0 to Temporary_Fields_Collection.Count-1 do
              begin
                TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
                TemplateFieldsRecPtr^.ValueHasChanged := FALSE;
              end;
            end;

            //Check for score alert if a game final data template and fire if needed
            if (TemplateInfo.UsesGameData) and (TemplateInfo.RequiredGameState = 3) then
            begin
              if (CheckForScoreAlertTrue(TickerRecPtr^.League, TRIM(TickerRecPtr^.GameID), CurrentGameData.State)) then
              begin
                //Update the game state
                AddCommandToQueue ('B\PL\' + 'SCORE_ALERT' + '�1\\', 50);
              end;
            end;
            UpdateGameInScoreAlertGamesCollection(TickerRecPtr^.League, TRIM(TickerRecPtr^.GameID), CurrentGameData.State);

            //Reload the commands so they are all there the next time around
            ReloadTempTemplateCommands(TickerRecPtr^.Template_ID, ACTION_TEMPLATE_UPDATE);

            //Get the total time required to clear the queue before dwell and pad dwell timer
            TickerCommandDelayTimer.Interval := TickerCommandDelayTimer.Interval + GetTimeDurationForQueue;
            if (MainForm.CommandOutList.Visible) and (LOG_DELAYS) then
              MainForm.CommandOutList.Lines.Add('Total Delay: ' + IntToStr(TickerCommandDelayTimer.Interval));

            //Set last template and scene director IDs - used to reset on next template
            LastTemplateID := TemplateInfo.Template_ID;
            LastSceneDirectorID := TemplateInfo.SceneDirectorID;
            //Set flag
            InitialDataTemplateFired := TRUE;
          end;

          //Set last game ID displayed
          if ((TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_GAME_STATS) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_NCAA_GAME_STATS) or
            (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_MATCHUPS_IN) or (TemplateInfo.SceneDirectorID = SCENE_DIRECTOR_ID_R_MATCHUPS_IN)) then
            LastGameIDDisplayed := TickerRecPtr^.GameID
          else
            LastGameIDDisplayed := '0';

          //Start timer to send next record - will be disabled if data needs to crawl
          if (USEDATAPACKET = FALSE) AND (NEXTRECORD = TRUE) then TickerCommandDelayTimer.Enabled := TRUE;
        end
        //Game not found, so don't send commands & go to next command
        else if (FoundGame = FALSE) then
        begin
          //Enable delay timer
          if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
        end;
      end
      //OK to go was set to false
      else begin
        //Enable delay timer
        if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
  //Set dwell label value
  MainForm.DwellValue.Caption := IntToStr(TickerCommandDelayTimer.Interval);
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
  begin
    //Send next record
    SendTickerRecord(TRUE, 0);

    //Log command if enabled
    if (MainForm.CommandOutList.Visible) and (LOG_TIMER_EVENTS) then
    begin
      if (MainForm.CommandOutList.Lines.Count > 500) then MainForm.CommandOutList.Lines.Clear;
      MainForm.CommandOutList.Lines.Add(FormatDateTime('hh:nn:ss.zzz', Now) + ': Watchdog Timer Event Fired');
    end;
  end;
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network');
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
  //Set error code to 0 to prevent default windows error message
  ErrorCode := 0;

  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network');
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\Comcast_Error_LogFiles') = FALSE) then
   begin
     CreateDir('c:\Comcast_Error_LogFiles');
     DirectoryStr := 'c:\Comcast_Error_LogFiles\';
   end;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Comcast_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\Comcast_AsRun_LogFiles');
     DirectoryStr := 'c:\Comcast_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToCommandLog (CommandString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   CommandLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(CommandLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Comcast_Command_LogFiles') = FALSE) then
       CreateDir('c:\Comcast_Command_LogFiles');
     DirectoryStr := 'c:\Comcast_Command_LogFiles\';
   end
   else DirectoryStr := CommandLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   CommandLogStr := TimeStr + '    ' + DateStr + '    ' + CommandString +
     '[TIME = ' + IntToStr(Hour) + ':' + IntToStr(Min) + ':' + IntToStr(Sec) +
     ':' + IntToStr(MSec) + ']';
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastCommandLog' + DayStr + '.txt';
   AssignFile (CommandLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (CommandLogFile);
      try
         Writeln (CommandLogFile, CommandLogStr);      finally         CloseFile (CommandLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (CommandLogFile);      try         Readln (CommandLogFile, TestStr);      finally         CloseFile (CommandLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (CommandLogFile);         {Create a new logfile}         ReWrite (CommandLogFile);         try
            Writeln (CommandLogFile, CommandLogStr);         finally            CloseFile (CommandLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (CommandLogFile);         try
            Writeln (CommandLogFile, CommandLogStr);         finally            CloseFile (CommandLogFile);         end;      end;   end;end;

end.


